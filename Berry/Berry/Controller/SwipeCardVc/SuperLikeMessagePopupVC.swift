//
//  SuperLikeMessagePopupVC.swift
//  Berry
//
//  Created by Anil on 26/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class SuperLikeMessagePopupVC: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblEnterDescription: UILabel!
    @IBOutlet weak var lblPlaceHolderEnterHere: UILabel!
    
    //MARK: - Variables
    var message: String = ""
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblPlaceHolderEnterHere.text = message
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
    }
    
    func setupUI() {
        lblMessage.textColor = UIColor.appThemeLightGrayColor
        lblMessage.font = themeFont(size: 18, fontname: .medium)
        
        [lblMessage,lblEnterDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        
        lblPlaceHolderEnterHere.font = themeFont(size: 15, fontname: .light)
        lblPlaceHolderEnterHere.textColor = UIColor.appThemeLightGrayColor        
    }
}

//MARK: - IBAction method
extension SuperLikeMessagePopupVC {
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
