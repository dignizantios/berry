//
//  LeftSideTblCell.swift
//  Liber
//
//  Created by YASH on 05/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class LeftSideTblCell: UITableViewCell {

    //MARK: - @IBOutlet
    @IBOutlet weak var imgLeftProfile: UIImageView!
    @IBOutlet weak var lblLeftMsg: UILabel!
    @IBOutlet weak var lblLeftTime: UILabel!
    @IBOutlet weak var vwBackColor: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK: - Variables
    let gradientLayer:CAGradientLayer = CAGradientLayer()

    
    //MARK: - AwakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgLeftProfile.layer.cornerRadius = imgLeftProfile.layer.bounds.width/2
        imgLeftProfile.layer.masksToBounds = true
        
        lblLeftMsg.textColor = UIColor.white
        lblLeftMsg.font = themeFont(size: 16, fontname: .regular)
        
        lblName.textColor = UIColor(red: 54.0/255.0, green: 135.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        lblName.font = themeFont(size: 11, fontname: .regular)
        
        lblTime.textColor = UIColor(red: 54.0/255.0, green: 135.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        lblTime.font = themeFont(size: 10, fontname: .regular)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        vwBackColor.roundCorners(corners: [.bottomLeft,.bottomRight,.topRight], radius: 5.0)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
