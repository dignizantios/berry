//
//  SignUpVc.swift
//  Berry
//
//  Created by YASH on 30/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import TOCropViewController
import SwiftyJSON
import SDWebImage
import Crashlytics

//import LNICoverFlowLayout

class SignUpVc: UIViewController, CountryCodeDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var vwCollectionProfile: UIView!
    
    @IBOutlet weak var collectionVwUserProfile: UICollectionView!
    @IBOutlet weak var coverFlowLayout: LNICoverFlowLayout!
    
    @IBOutlet weak var vwSingleImage: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView! //Single image Hidden From Storyboard
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: CustomTextField!
    
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var txtDate: CustomTextField!
    @IBOutlet weak var txtMonthName: CustomTextField!
    @IBOutlet weak var txtYear: CustomTextField!
    
    @IBOutlet weak var viewMobileNumber: UIView!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    
    @IBOutlet weak var viewHobbies: UIView!
    @IBOutlet weak var lblHobby: UILabel!
    @IBOutlet weak var txtHobby: CustomTextField!
    
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnRegister: CustomButton!
    
    @IBOutlet weak var txtCountName: UILabel!
    @IBOutlet weak var txtCountHobbies: UILabel!
    
    @IBOutlet weak var viewPass: UIView!
    
    @IBOutlet weak var lblPasswordCharacter: UILabel!
    @IBOutlet weak var lblPasswordNumber: UILabel!
    @IBOutlet weak var lblPasswordNonAlphan: UILabel!
    @IBOutlet weak var lblPassReq: UILabel!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgCountryCode: UIImageView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var lblAgree: ActiveLabel!
    
    //MARK: - Variables
    var picker = UIImagePickerController()
    
    var originalItemSize = CGSize()
    var originalCollectionViewSize = CGSize()
    
    var arrayImageUser = NSMutableArray()
    var isMaleSelected:Bool = true
    var birthSelected:String = ""
    var isSignUpSocial :Bool = false
    var userId :String = ""
    var missingArray :[JSON] = [JSON]()
    var countryDialCode: String!
    var countryName: String!
    var countryCode: String!
    
    //MARK: - ViewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        let countryJSON = getCountryPhonceCode(Locale.current.regionCode ?? "")
        self.countryDialCode = countryJSON["dial_code"].stringValue
        self.countryName = countryJSON["name"].stringValue
        self.countryCode = countryJSON["code"].stringValue
        
        self.viewCountry.layer.cornerRadius = 8
        originalItemSize = coverFlowLayout.itemSize
        originalCollectionViewSize = collectionVwUserProfile.bounds.size
        
        collectionVwUserProfile.register(UINib(nibName: "collectionUserImageCell", bundle: nil), forCellWithReuseIdentifier:"collectionUserImageCell")
        
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
        setupUI()
        
        if isSignUpSocial {
            self.setupSocial()
            if arrayImageUser.count != 0 {
                vwSingleImage.isHidden = true
                vwCollectionProfile.isHidden = false
                collectionVwUserProfile.reloadData()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.viewWillAppear(_:)), name: NSNotification.Name(rawValue: "refreshView"), object: nil)
        
        let text = "By clicking the register below, I agree to be bound by Berry's Terms of Use and Privacy Policy."
        
        let customType = ActiveType.custom(pattern: "\\sTerms of Use\\b")
        let customType2 = ActiveType.custom(pattern: "\\sPrivacy Policy\\b") 
        
        self.lblAgree.enabledTypes.append(customType)
        self.lblAgree.enabledTypes.append(customType2)
        self.lblAgree.customize { label in
            label.text = text
            label.numberOfLines = 0
            label.lineSpacing = 4

            label.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                case customType:
                    atts[NSAttributedString.Key.underlineStyle] = true
                case customType2:
                    atts[NSAttributedString.Key.underlineStyle] = true
                default: ()
                }
                
                return atts
            }
            
            label.handleCustomTap(for: customType) {_ in
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WebvwVc") as!  WebvwVc
                obj.selectedController = .forTermsofService
                obj.url = "http://www.berrydates.com/terms-of-use/"
                self.navigationController?.pushViewController(obj, animated: true)
            }
            label.handleCustomTap(for: customType2) {_ in
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WebvwVc") as!  WebvwVc
                obj.selectedController = .forPrivacyPolicy
                obj.url = "http://www.berrydates.com/privacy-policy/" //"http://www.berrydates.com/privacy-policy-2/"
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Profile Setup")//getCommonString(key: "Sign_Up_key"))
        AppDelegate.shared.checkOldUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            self.collectionVwUserProfile.reloadData()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        coverFlowLayout.invalidateLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        coverFlowLayout.itemSize = CGSize(width: collectionVwUserProfile.bounds.size.width * originalItemSize.width / originalCollectionViewSize.width, height: collectionVwUserProfile.bounds.size.height * originalItemSize.height / originalCollectionViewSize.height)
        
        setInitialValues()
        
        collectionVwUserProfile.setNeedsLayout()
        collectionVwUserProfile.layoutIfNeeded()
        collectionVwUserProfile.reloadData()
    }
    
    
    //MARK: - Setup UI
    func setupUI() {
        selectedBtn(selectedBtn: btnMale, unselectedBtn: btnFemale)
        
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        [lblName,lblDateOfBirth,lblMobileNumber,lblPassword,lblGender,lblHobby, lblPassReq].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
            
        }
        
        self.viewPass.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.viewPass.layer.cornerRadius = 8

        [txtName,txtDate,txtYear,txtMonthName,txtMobileNumber,txtPassword,txtHobby].forEach { (txt) in
            txt?.setupCustomTextField()
            txt?.delegate = self
        }
        
        [lblMale,lblFemale].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        addDoneButtonOnKeyboard(textfield: txtMobileNumber)
        
        lblName.text = getCommonString(key: "Name_key")
        lblDateOfBirth.text = getCommonString(key: "Date_of_birth_key")
        lblMobileNumber.text = getCommonString(key: "Mobile_number_key")
        lblHobby.text = getCommonString(key: "Hobbies_key")
        lblPassword.text = getCommonString(key: "Password_key")
        lblGender.text = getCommonString(key: "Gender_key")
        lblMale.text = getCommonString(key: "Male_key")
        lblFemale.text = getCommonString(key: "Female_key")
        btnRegister.setTitle(getCommonString(key: "REGISTER_key"), for: .normal)
        btnRegister.setupShadowThemeButtonUI()
        picker.delegate = self
        vwCollectionProfile.isHidden = false
        vwSingleImage.isHidden = true
    }
    
    func setInitialValues() {
        coverFlowLayout.maxCoverDegree = 0
        coverFlowLayout.coverDensity = 0
        coverFlowLayout.minCoverScale = 0.90
        coverFlowLayout.minCoverOpacity = 1.0
    }
    
    func setupSocial() {
        self.btnRegister.setTitle("Save", for: .normal)
        self.viewName.isHidden = true
        self.viewDOB.isHidden = true
        self.viewMobileNumber.isHidden = true
        self.viewHobbies.isHidden = true
        self.viewPassword.isHidden = true
        self.viewGender.isHidden = true

        for key in self.missingArray {
            if key == "name" {
                self.viewName.isHidden = false
            }
            else if key == "birth" {
                self.viewDOB.isHidden = false
            }
            else if key == "mobile_no" {
                self.viewMobileNumber.isHidden = false
            }
            else if key == "hobbies" {
                self.viewHobbies.isHidden = false
            }
            else if key == "gender" {
                self.viewGender.isHidden = false
            }
        }
    }

    func validationSignUp(){
        if self.txtName.text?.toTrim() == "" {
            self.showToast(message: "Please enter name")
        }
        else if self.txtDate.text! == "" || self.txtMonthName.text! == "" || self.txtYear.text! == "" {
            self.showToast(message: "Please enter date of birth")
        }
        else if self.txtMobileNumber.text?.toTrim() == "" {
            self.showToast(message: "Please enter mobile number")
        }
        else if self.txtHobby.text?.toTrim() == "" {
            self.showToast(message: "Please enter hobbies")
        }
        else if self.txtPassword.text?.toTrim() == "" {
            self.showToast(message: "Please enter password")
        }
        /*else if self.txtPassword.text!.count < 8 {
            self.showRedAlert()
            self.showToast(message: "Please enter minimum 8 digit password")
        }*/
        else if !isValidPassword(testStr: self.txtPassword.text!) {
            self.showRedAlert()
//            self.showToast(message: "Password should be at least one number, one non-alphanumeric character and 8 character long")
        }
        else if self.arrayImageUser.count == 0 {
            self.showRedAlert(isShow: false)
            self.showToast(message: "Please select profile picture")
        }
        else if self.arrayImageUser.count < 3 {
            self.showToast(message: "Please select minimum 3 profile picture")
        }
        else if !self.btnAgree.isSelected {
            self.showToast(message: "Please agree terms of Use")
        }
        else {
            self.makeSignup()
        }
    }
    
    func showRedAlert(isShow: Bool = true) {
        self.lblPasswordNumber.textColor = isShow ? UIColor.red : UIColor.lightGray
        self.lblPasswordCharacter.textColor = isShow ? UIColor.red : UIColor.lightGray
        self.lblPasswordNonAlphan.textColor = isShow ? UIColor.red : UIColor.lightGray
    }
    
    func validationSignUpSocial(){
        if self.txtName.text?.toTrim() == "" && self.viewName.isHidden == false {
            self.showToast(message: "Please enter name")
        }
        else if !isValidName(testStr: (self.txtName.text?.toTrim())!) && self.viewName.isHidden == false {
            self.showToast(message: "Please enter valid name, name must not cantains digits & special characters.")
        }
        else if (self.txtDate.text! == "" || self.txtMonthName.text! == "" || self.txtYear.text! == "")  && self.viewDOB.isHidden == false {
            self.showToast(message: "Please enter date of birth")
        }
        else if self.txtMobileNumber.text?.toTrim() == "" && self.viewMobileNumber.isHidden == false  {
            self.showToast(message: "Please enter mobile number")
        }
        else if self.txtHobby.text?.toTrim() == ""  && self.viewHobbies.isHidden == false  {
            self.showToast(message: "Please enter hobbies")
        }
        else if self.arrayImageUser.count < 3 {
            self.showToast(message: "Please select minimum 3 profile picture")
        }
        else if !self.btnAgree.isSelected {
            self.showToast(message: "Please agree terms of Use.")
        }
        else {
            self.makeSignupSocial()
        }
    }
    
    func makeSignup() {
        
        var images = [UIImage]()
        var imagesUrl = [String]()
        
        for image in self.arrayImageUser {
            let imageNew = image as! NSDictionary
            let isImage = imageNew.value(forKey: "isImage") as! String
            if isImage == "1" {
                images.append(imageNew.value(forKey: "imageURL") as! UIImage)
            }
            else {
                imagesUrl.append(imageNew.value(forKey: "imageURL") as! String)
            }
        }
        
        let gender = self.isMaleSelected ? "male" : "female"
        let location = jsonString(from: [AppDelegate.shared.longitude,AppDelegate.shared.lattitude])
        
        var param: [String : Any]?  
        if AppDelegate.shared.longitude != 0 && AppDelegate.shared.lattitude != 0 {
            param = ["type":"guest","name":self.txtName.text!.toTrim(),"birth":self.birthSelected,"co": "\(self.countryDialCode.dropFirst())","mobile_no":self.txtMobileNumber.text!.toTrim(),"password":self.txtPassword.text!.toTrim(),"gender":gender,"hobbies":self.txtHobby.text!.toTrim(), "firebaseTokenId":getFCMToken(),"location": "\"" + location! + "\"", "det": DEVICE_TYPE]
        }
        else {
            param = ["type":"guest","name":self.txtName.text!.toTrim(),"birth":self.birthSelected,"co": "\(self.countryDialCode.dropFirst())","mobile_no":self.txtMobileNumber.text!.toTrim(),"password":self.txtPassword.text!.toTrim(),"gender":gender,"hobbies":self.txtHobby.text!.toTrim(), "firebaseTokenId":getFCMToken(), "det": DEVICE_TYPE]
        }
        
        ApiManager.shared.MakePostWithImageAPI(name: REGISTER, params: param!, images: images, imagesURL: imagesUrl, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
//                    saveJSON(j: json, key: USER_DETAILS_KEY)
                    userDetails = json
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        let userid = json.getString(key: .userid)
                        let myProfile = json.getDictionary(key: .myprofile)
                        let type = myProfile.getString(key: .type)
                        print(self.countryDialCode)
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OTPVc") as! OTPVc
                        obj.userId = userid
                        obj.countryDialCode = self.countryDialCode
                        obj.mobileNo = self.txtMobileNumber.text!
                        obj.isOpenSignUP = true
                        if type.lowercased() == "facebook".lowercased() || type.lowercased() == "google".lowercased() {
                            obj.isOpenSocial = true
                        }
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeSignup()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    
    func makeSignupSocial() {
        let gender = self.isMaleSelected ? "male" : "female"
        
        let location = jsonString(from: [AppDelegate.shared.longitude,AppDelegate.shared.lattitude])
        var param: [String : Any]?
        if AppDelegate.shared.longitude != 0 && AppDelegate.shared.lattitude != 0 {
            param =    ["userid":self.userId, "birth":self.birthSelected,"co": "\(self.countryDialCode.dropFirst())","mobile_no":self.txtMobileNumber.text! ,"gender":gender,"hobbies":self.txtHobby.text!, "location":"\"" + location! + "\""]
        }
        else {
            param =    ["userid":self.userId,"birth":self.birthSelected,"co": "\(self.countryDialCode.dropFirst())","mobile_no":self.txtMobileNumber.text! ,"gender":gender,"hobbies":self.txtHobby.text!]
        }
        
        if self.viewDOB.isHidden {
            param?.removeValue(forKey: "birth")
        }
        if self.viewGender.isHidden {
            param?.removeValue(forKey: "gender")
        }
        
        var images = [UIImage]()
        var imagesUrl = [String]()
        for image in self.arrayImageUser {
            let imageNew = image as! NSDictionary
            let isImage = imageNew.value(forKey: "isImage") as! String
            let isNew = imageNew.value(forKey: "isNew") as? String ?? ""
            
            if isImage == "1" {
                if isNew != "0" {
                    images.append(imageNew.value(forKey: "imageURL") as! UIImage)
                }
            }
            else {
                if isNew != "0" {
                    imagesUrl.append(imageNew.value(forKey: "imageURL") as! String)
                }
            }
        }
        
        ApiManager.shared.MakePostWithImageAPI(name: UPDATE_PROFILE, params: param!, images: images, imagesURL: imagesUrl, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
//                    saveJSON(j: json, key: USER_DETAILS_KEY)
                    userDetails = json
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        let userid = json.getString(key: .userid)
                        let myProfile = json.getDictionary(key: .myprofile)
                        let type = myProfile.getString(key: .type)
                        
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OTPVc") as! OTPVc
                        obj.userId = userid
                        obj.countryDialCode = self.countryDialCode
                        obj.mobileNo = self.txtMobileNumber.text!
                        if type.lowercased() == "facebook".lowercased() || type.lowercased() == "google".lowercased() {
                            obj.isOpenSocial = true
                        }
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeSignupSocial()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
}


//MARK: - IBAction

extension SignUpVc {
    
    @IBAction func btnAgreeTapped(_ sender: UIButton) {
      sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnProfileSelectionTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "PhotoPickerVc") as! PhotoPickerVc
        obj.delegateImage = self
        obj.intRemainingPhotoCount = 6 - arrayImageUser.count
        obj.selectedController = .forRegistration
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnRegisterTapped(_ sender: UIButton) {
        if isSignUpSocial {
            self.validationSignUpSocial()
        }
        else {
            self.validationSignUp()
        }
    }
    
    @IBAction func btnShowPasswordTapped(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "password_not_viewed") {
            UIView.performWithoutAnimation {
                self.txtPassword.isSecureTextEntry = false
                sender.setImage(UIImage(named: "password_viewed"), for: .normal)
            }
        }
        else {
            UIView.performWithoutAnimation {
                self.txtPassword.isSecureTextEntry = true
                sender.setImage(UIImage(named: "password_not_viewed"), for: .normal)
            }
        }
    }
    
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode  
    }
    
    @IBAction func btnMaleTapped(_ sender: UIButton) {
        self.isMaleSelected = true
        selectedBtn(selectedBtn: btnMale, unselectedBtn: btnFemale)
    }
    
    @IBAction func btnFemaleTapped(_ sender: UIButton) {
        self.isMaleSelected = false
        selectedBtn(selectedBtn: btnFemale, unselectedBtn: btnMale)
    }
    
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        self.present(obj, animated:  false, completion: nil)
    }
    
    @objc func btnAddPhotoInCollectionView(sender : UIButton) {
        if self.arrayImageUser.count >= 6 {
            self.showToast(message: "You reach maximum limit of image upload")
        }
        else {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "PhotoPickerVc") as! PhotoPickerVc
            obj.delegateImage = self
            obj.intRemainingPhotoCount = 6 - arrayImageUser.count
            obj.selectedController = .forRegistration
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    
    func selectedBtn(selectedBtn:UIButton,unselectedBtn : UIButton) {
        selectedBtn.isSelected = true
        unselectedBtn.isSelected = false
    }
}


extension SignUpVc : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if textField == txtMobileNumber {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        else {
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                if updatedText == " " {
                    return false
                }
                
                if textField == self.txtPassword {
                    if (string == " ") {
                        return false
                    }
                }
                
                if textField == self.txtName {
                    
                    if !isValidName(testStr: updatedText) {
                        return false
                    }
                    if updatedText.count > 50 {
                        return false
                    }
                    self.txtCountName.text = "\(updatedText.count)" + " / 50"
                }
                
                if textField == self.txtHobby {
                    if updatedText.count > 50 {
                        return false
                    }
                    self.txtCountHobbies.text = "\(updatedText.count)" + " / 50"
                }
            }
        }
        return true
    }    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate || textField == txtMonthName || textField == txtYear {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 1
            obj.isSetMaximumDate = true
            
            let YearAgo18 = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            obj.setMaximumDate = YearAgo18
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
}

//MARK: - Image delegate

extension SignUpVc : UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let selectedImage = info[UIImagePickerController.InfoKey.originalImage]
        
        let cropVC = TOCropViewController(image: selectedImage as! UIImage)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        cropVC.modalPresentationStyle = .overFullScreen
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropVC, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropVC, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        
        //        imgProfile.image = image
        
        vwCollectionProfile.isHidden = false
        vwSingleImage.isHidden = true
        
//        let data = image.pngData()! as NSData
//        let selectedImage:UIImage = UIImage(data: data as Data)!
        
        //        arrayImageUser.append(selectedImage)
        
        collectionVwUserProfile.reloadData()
        
        cropViewController.dismiss(animated: false, completion: nil)
        self.picker.dismiss(animated: true, completion: nil)
    }
}


extension SignUpVc : DateTimePickerDelegate {
    func setDateandTime(dateValue: Date, type: Int) {
        self.view.endEditing(true)
        
        if type == 1 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM"
            
            txtMonthName.text = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "yyyy"
            txtYear.text = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "dd"
            txtDate.text = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            self.birthSelected = dateFormatter.string(from: dateValue)
        }
    }
}


//MARK: - CollectionView delegate and DataSource

extension SignUpVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImageUser.count == 6 ? arrayImageUser.count : arrayImageUser.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionUserImageCell", for: indexPath) as! collectionUserImageCell
        
        cell.btnAddPhoto.tag = indexPath.row
        
        if arrayImageUser.count == indexPath.row {
            cell.btnAddPhoto.isHidden = false
            cell.img.image = UIImage(named: "ic_add_swipe_card")
            cell.img.contentMode = .center
            cell.img.backgroundColor = UIColor.white
        }
        else {
            cell.btnAddPhoto.isHidden = true
            let imageNew = arrayImageUser[indexPath.row] as! NSDictionary
            let isImage = imageNew.value(forKey: "isImage") as! String
            if isImage == "1" {
                cell.img.image = imageNew.value(forKey: "imageURL") as? UIImage
            }
            else {
                let url = imageNew.value(forKey: "imageURL") as? String
                //                cell.img.sd_setImage(with: URL(string: url!), completed: nil)
                cell.img.sd_setImage(with: URL(string: url!), placeholderImage: UIImage(named: "user_placeholder_big"), completed: nil)
            }
            
            cell.img.contentMode = .scaleAspectFill
            cell.img.backgroundColor = UIColor.clear
        }
        
        cell.btnAddPhoto.addTarget(self, action: #selector(btnAddPhotoInCollectionView), for: .touchUpInside)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrayImageUser.count == indexPath.row {

        }
        else {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CropMultiImagesVc") as! CropMultiImagesVc
            
            obj.delegateCropImage = self
            obj.arrSocialMediaImagesMutable = arrayImageUser
            obj.currentImageIndex = indexPath.row
            obj.selectedController = .forRegistration
            obj.userId = self.userId
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}


extension SignUpVc : selectedImageDelegate {
    func setImagesWithUrlInCollection(arrayOfImages: NSMutableArray) {
        vwSingleImage.isHidden = true
        vwCollectionProfile.isHidden = false
        for image in arrayOfImages {
            arrayImageUser.add(image)
        }
        collectionVwUserProfile.reloadData()
        
        //        for i in 0..<arrayOfImages.count
        //        {
        //            arrayImageUser.append(arrayOfImages[i])
        //        }
    }
    
    func setImagesInCollection(arrayOfImages: [UIImage]) {
        vwSingleImage.isHidden = true
        vwCollectionProfile.isHidden = false
        for image in arrayOfImages {
            let dict: NSDictionary = ["imageURL" : image,"isImage": "1"]
            self.arrayImageUser.add(dict)
        }
        collectionVwUserProfile.reloadData()
        
    }
}

extension SignUpVc : CropImagesDelegate {
    func setCropImagesWithUrlInCollection(arrayOfImages: NSMutableArray) {
        
    }
    
    func setCropImagesInCollection(arrayOfImages: [UIImage]) {
        arrayImageUser = []
        if arrayOfImages.count == 0 {
            vwSingleImage.isHidden = false
            vwCollectionProfile.isHidden = true
        }
        else {
            vwSingleImage.isHidden = true
            vwCollectionProfile.isHidden = false
        }
        collectionVwUserProfile.reloadData()
    }
}


extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
