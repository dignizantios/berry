//
//  RightSideImageTblCell.swift
//  Berry
//
//  Created by Anil on 22/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class RightSideImageTblCell: UITableViewCell {

    @IBOutlet weak var imgRightProfile: UIImageView!
    @IBOutlet weak var imgRightMessage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var height: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgRightProfile.layer.cornerRadius = imgRightProfile.layer.bounds.width/2
        imgRightProfile.layer.masksToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
