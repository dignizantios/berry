//
//  AppDelegate.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftyJSON
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
//import GiphyCoreSDK
import InMobiSDK
import GoogleMobileAds

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var window: UIWindow?

    static let shared = UIApplication.shared.delegate as! AppDelegate

    var locationManager = CLLocationManager()
    var lattitude  = Double()
    var longitude = Double()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if userDetails.getString(key: .userid) != "" {
            SocketIOHandler.shared.Connect()
            self.OutTime(type: "login")
        }
        GADMobileAds.sharedInstance().start(completionHandler: nil)
//        self.refreshToken()
        self.checkOldUser()
        
        var consentdict: [AnyHashable : Any] = [:]
        consentdict[IM_GDPR_CONSENT_AVAILABLE] = "true"
        consentdict["gdpr"] = NSNumber(value: 1)
        IMSdk.setLogLevel(IMSDKLogLevel.debug);
        var error: NSError?
        IMSdk.initWithAccountID(INMOBI_ACCOUNT_ID, consentDictionary: consentdict, andError: &error)
        if error != nil {
            print("** ERROR: Failed to initialize with error \(error?.description ?? "No Error"). **")
        }
        
        GiphyCore.configure(apiKey: "qTEYXxcFtxmqJHD2cwlBWBdm0Zr06eYH")

        GIDSignIn.sharedInstance().clientID = "334920797431-2oqarm2itos0bj7rs5hl33socne154b1.apps.googleusercontent.com" // "3919008659-2c68eok9rfs88phb0kks8a72d5korgsh.apps.googleusercontent.com"
        //old url scheem : com.googleusercontent.apps.3919008659-2c68eok9rfs88phb0kks8a72d5korgsh
        
        GMSServices.provideAPIKey(GlobalVariables.googleMapKey)
        GMSPlacesClient.provideAPIKey(GlobalVariables.googleMapKey)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        self.setUpQuickLocationUpdate()

        if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
            self.verifyInSub()
            let json = loadJSON(key: USER_DETAILS_KEY)
            print(json)
            let myProfile = json.getDictionary(key: .myprofile)
            let type = myProfile.getString(key: .type)
            if myProfile.getBool(key: .verified) == false {
                let accountStatus = myProfile.getString(key: .acountStatus).uppercased()                
                IM_AD_INSERTION_POSITION = accountStatus == "N" ? IM_AD_INSERTION_POSITION_TOTAL : 0

//                let aedate = myProfile.getString(key: .aedate)
//                if aedate != "" {
//                    let date = convertDate(str: aedate)
//                    if date > Date() {
//                        IM_AD_INSERTION_POSITION = 0
//                    }
//                    else {
//                        IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                    }
//                }
//                else {
//                    IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                }
                
                let userid = json.getString(key: .userid)
                let mobile = myProfile.getString(key: .mobile_no)
                let co = myProfile.getString(key: .co)
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OTPVc") as! OTPVc
                obj.userId = userid
                obj.mobileNo = mobile
                obj.countryDialCode = "+" + co
                obj.isOpenAppDelegate = true
                
                if type.lowercased() == "facebook".lowercased() || type.lowercased() == "google".lowercased() {
                    obj.isOpenSocial = true
                }
                
                let nav = UINavigationController(rootViewController: obj)
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
            else {
                let accountStatus = myProfile.getString(key: .acountStatus).uppercased()
                IM_AD_INSERTION_POSITION = accountStatus == "N" ? IM_AD_INSERTION_POSITION_TOTAL : 0

//                let aedate = myProfile.getString(key: .aedate)
//                if aedate != "" {
//                    let date = convertDate(str: aedate)
//                    if date > Date() {
//                        IM_AD_INSERTION_POSITION = 0
//                    }
//                    else {
//                        IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                    }
//                }
//                else {
//                    IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                }
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVc") as! MainVc
                let nav = UINavigationController(rootViewController: obj)
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
        
        //Firebase
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
//        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//            AnalyticsParameterItemID: "id-\(title!)",
//            AnalyticsParameterItemName: title!,
//            AnalyticsParameterContentType: "cont"
//        ])

        return true
    }
    
    func checkOldUser() {
        if getFCMLogoutToken() != "" {
            DispatchQueue.main.async {
                guard ReachabilityTest.isConnectedToNetwork() else {
                    return
                }
                self.serviceCallToLogout()
            }
        }
    }
    
    func serviceCallToLogout() {
        let param = ["userid": getLogoutUserId() , "det":DEVICE_TYPE,"token":getFCMLogoutToken()]
        print("param:=",param)
        ApiManager.shared.MakePostAPIWithoutVC(name: LOGOUT, params: param, isShowError: false) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
                    removeLogoutOldToken()
                }
                else {
                    
                }
            }
            else {
                self.serviceCallToLogout()
            }
        }
    }
    
    func verifyInSub() {
        let param = ["userid": userDetails.getString(key: .userid)]
        ApiManager.shared.MakePostAPIWithoutVC(name: VERIFY_IN_SUB, params: param, isShowError: false) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
//                    saveJSON(j: json, key: USER_DETAILS_KEY)                    
                    let myProfile = json.getDictionary(key: .myprofile)
                    
                    let aedate = myProfile.getString(key: .aedate)
                    if aedate != "" {
                        let date = convertDate(str: aedate)
                        print(Date())
                        print(date)
                    }
                }
            }
            else {
                self.verifyInSub()
            }
        }
    }
    
    func OutTime(type:String) {
        let param = ["userid": userDetails.getString(key: .userid), "type":type]
        ApiManager.shared.MakePostAPIWithoutVC(name: OUTTIME, params: param, isShowError: false) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    
                }
                else {
                    
                }
            }
            else {
                self.OutTime(type: type)
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("terminated")
        if userDetails.getString(key: .userid) != "" {
            self.OutTime(type: "logout")
        }
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == "fb185299605718710" {
            let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
             return handled
        }
        else {
            return (GIDSignIn.sharedInstance()?.handle(url))!
//            return GIDSignIn.sharedInstance()?.handle(url as URL?,
//                                                     sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//                                                     annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        // Add any custom logic here.
    }
    
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return (GIDSignIn.sharedInstance()?.handle(url))!
//        return GIDSignIn.sharedInstance().handle(url,
//                                                    sourceApplication: sourceApplication,
//                                                    annotation: annotation)
    }
}

extension AppDelegate {
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(JSON(userInfo))
        let dict = JSON(userInfo)
        if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
            let userid = dict.getString(key: .pushUserid)
            let type = dict.getString(key: .pushType)
            
            if userid != CHAT_USER_ID && type == "chat" {
                completionHandler(UIBackgroundFetchResult.newData)
            }
            else if type != "chat"
            {
                completionHandler(UIBackgroundFetchResult.newData)
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let dict = JSON(notification.request.content.userInfo)
        if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
            let userid = dict.getString(key: .pushUserid)
            let type = dict.getString(key: .pushType)
            
            if userid != CHAT_USER_ID && type == "chat" {
                completionHandler([.badge,.alert,.sound])
            }
            else if type != "chat"
            {
                completionHandler([.badge,.alert,.sound])
            }
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        Messaging.messaging().apnsToken = deviceToken
        print(getRealFCMToken())
        print(getFCMToken())
        if getFCMToken() == "" {
            saveFCMToken(token: getRealFCMToken())
            print(getFCMToken())
        }
        else {
            if getFCMToken() != getRealFCMToken() {
                if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
                    let param = ["userid":userDetails.getString(key: .userid), "det": DEVICE_TYPE, "oldToken":getFCMToken(), "newToken":getRealFCMToken()]
                    saveFCMToken(token: getRealFCMToken())
                    ApiManager.shared.MakePostAPIWithoutVC(name: TOKEN_REPLACE_REMOVE, params: param) { (result, error) in
                    }
                }
                saveFCMToken(token: getRealFCMToken())
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR : \(error)")
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let dict = JSON(response.notification.request.content.userInfo)
        print("dict: \(dict)")
        completionHandler()
        
        if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
            let type = dict.getString(key: .pushType)
            print(type)
            let userid = dict.getString(key: .pushUserid)
            print(userid)
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVc") as! MainVc
            obj.isPush = true
            obj.pushIndex = type == "chat" ? 2 : 3
            PUSH_TYPE = type
            PUSH_USER_ID = userid            
            let nav = UINavigationController(rootViewController: obj)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
        }
    }
    
    
    func refreshToken() {
        let instance = InstanceID.instanceID()
        instance.deleteID { (error) in
            print(error.debugDescription)
        }

        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }

        
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
//        guard InstanceID.instanceID().token() != nil else {
//            return
//        }
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().shouldEstablishDirectChannel = false
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
//        Messaging.messaging().connect() { (error) in
//            if error != nil {
//                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
//            } else {
//                print("Connected to FCM.")
//            }
//        }
    }
}

//MARK: - Location Delegate

extension AppDelegate:CLLocationManagerDelegate {
    func setUpQuickLocationUpdate() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10
        locationManager.activityType = .automotiveNavigation
        //   locationManager.pausesLocationUpdatesAutomatically = true
        // locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        //  locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        IMSdk.setLocation(locations.first!)
        if let latestLocation = locations.first {
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            //  KSToastView.ks_showToast("Location Update Successfully", duration: ToastDuration)
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude {
                userCurrentLocation = latestLocation
                lattitude = latestLocation.coordinate.latitude
                longitude = latestLocation.coordinate.longitude
                print("lattitude:- \(String(describing: userCurrentLocation?.coordinate.latitude) ), longitude:- \(String(describing: userCurrentLocation?.coordinate.longitude))")
                //self.delegate?.didUpdateLocation(lat:latestLocation.coordinate.latitude,lon:latestLocation.coordinate.longitude)
                //
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            //    manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
                openSetting()
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
                openSetting()
            } else {
                // Fallback on earlier versions
            }
            break
            //   default:
            // break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error :- \(error)")
    }
    
    //MARK:- open Setting
    
    func openSetting() {
        let alertController = UIAlertController (title: getCommonString(key: "Berry_key"), message: getCommonString(key: "Go_to_Setting_key"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: getCommonString(key: "Setting_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        //     let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .default, handler: nil)
        //    alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
