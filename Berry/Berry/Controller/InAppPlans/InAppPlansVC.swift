//
//  InAppPlansVC.swift
//  Berry
//
//  Created by iMac on 15/01/20.
//  Copyright © 2020 YASH. All rights reserved.
//

import UIKit

protocol InAppPlansDelegate: class {
    func InAppPlansFinish()
}

class InAppPlansVC: UIViewController {
    
    weak var delegate: InAppPlansDelegate?
    var price:String = ""
    @IBOutlet weak var lblPrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbarTitle(titleText: "Subscription Plans")
        self.lblPrice.text = price + "/month"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPrivacyPolicyTapped(_ sender: UIButton) {
    //        let details = self.profileDetails["settings"].dictionary
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WebvwVc") as!  WebvwVc
            obj.selectedController = .forPrivacyPolicy
            obj.url = "http://www.berrydates.com/privacy-policy/" //http://www.berrydates.com/privacy-policy-2/" //details?["privacy"]?.string ?? ""
            self.navigationController?.pushViewController(obj, animated: true)
        }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.InAppPlansFinish()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
