//
//  RightSideTblCell.swift
//  Liber
//
//  Created by YASH on 05/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class RightSideTblCell: UITableViewCell {

    //MARK: - @IBOutlet
    @IBOutlet weak var imgRightProfile: UIImageView!
    @IBOutlet weak var lblRightMsg: UILabel!
    @IBOutlet weak var vwBackColor: CustomView!
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK: - AwakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgRightProfile.layer.cornerRadius = imgRightProfile.layer.bounds.width/2
        imgRightProfile.layer.masksToBounds = true
        
        lblRightMsg.textColor = UIColor(red: 54.0/255.0, green: 135.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        lblRightMsg.font = themeFont(size: 16, fontname: .regular)
        
        lblName.text = ""
        
        lblTime.textColor = UIColor(red: 54.0/255.0, green: 135.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        lblTime.font = themeFont(size: 10, fontname: .regular)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)

    }
}
