//
//  collectionUserImageCell.swift
//  Berry
//
//  Created by YASH on 02/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class collectionUserImageCell: UICollectionViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwBack: CustomView!
    @IBOutlet weak var img: CustomImageView!
    
    @IBOutlet weak var btnAddPhoto: UIButton!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.img.layer.cornerRadius = 5
        self.img.layer.masksToBounds = true
        self.img.clipsToBounds = true
        
        self.vwBack.backgroundColor = UIColor.clear
        self.vwBack.layer.shadowColor = UIColor.black.cgColor
        self.vwBack.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.vwBack.layer.shadowOpacity = 0.3;
        self.vwBack.layer.shadowRadius = 2.0;
        self.vwBack.layer.shadowPath = UIBezierPath(roundedRect: self.vwBack.bounds, cornerRadius: 5.0).cgPath
        
    }
    
    
}
