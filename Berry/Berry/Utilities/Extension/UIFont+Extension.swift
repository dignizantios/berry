//
//  UIFont+Extension.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String {
//    case regular = "HelveticaNeue"
    case regular = "NHaasGroteskTXPro-Rg"
    case medium = "HelveticaNeue-Medium"
//    case bold = "HelveticaNeue-Bold"
    case bold = "NHaasGroteskTXPro-Bd"
    case light = "HelveticaNeue-Light"
}

extension UIFont {

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont {
    if UIScreen.main.bounds.width <= 320 {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
}
