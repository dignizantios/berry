//
//  MainVc.swift
//  Berry
//
//  Created by YASH on 30/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON

protocol ChangeIndexCarbonkitDelegate: class {
    func ChangeIndexCarbonkit(index: Int)
    func ChangeBadge(badgeValue: String)
}


class MainVc: UIViewController, ChangeIndexCarbonkitDelegate, SockerConnectionUnreadMsgDelegate, ChangeIndexMatchDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var vwMain: UIView!
    
    static var shared = MainVc()
    
    //MARK: - Variables
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var selectedIndexVC = 0
    var items = NSArray()
    
    var isPush : Bool = false
    var pushIndex : Int = 0
    var isLogin : Bool = false
    var lastBadge: String = ""
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketIOHandler.shared.delegateUnreadMsg = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.setValueOfBadgeCount), name: NSNotification.Name(rawValue: "showBadgeCount"), object: nil)
        if isLogin {
            SocketIOHandler.shared.Connect()
        }
        
        if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
            let json = loadJSON(key: USER_DETAILS_KEY)
            print(json)
            let myProfile = json.getDictionary(key: .myprofile)
            
            let accountStatus = myProfile.getString(key: .acountStatus).uppercased()
            IM_AD_INSERTION_POSITION = accountStatus == "N" ? IM_AD_INSERTION_POSITION_TOTAL : 0
            
//            let aedate = myProfile.getString(key: .aedate)
//            if aedate != "" {
//                let date = convertDate(str: aedate)
//                if date > Date() {
//                    IM_AD_INSERTION_POSITION = 0
//                }
//                else {
//                    IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                }
//            }
//            else {
//                IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//            }
        }

        setupUI()
    }
    
    @objc func setValueOfBadgeCount(notification : Notification) {
        if let obj = notification.object as? JSON {
            print(obj)
            self.ChangeBadge(badgeValue: String(obj.getInt(key: .totalCount)))
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupUI() {
        items = [UIImage(named: "ic_profile_header_tab_unselected")!,
                 UIImage(named: "ic_swipe_card_header_tab_unselected")!,
                 UIImage(named: "ic_match_card_header_tab_unselected")!,
                 UIImage(named: "ic_notidication_header_tab_unselected")!
        ]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: vwMain)
        self.style()
    }
    
    func ChangeIndexCarbonkit(index: Int) {
        isShowSetting = true
        carbonTabSwipeNavigation.setCurrentTabIndex(UInt(index), withAnimation: true)
    }
    
    func ChangeIndexMatchFinish() {
        carbonTabSwipeNavigation.setCurrentTabIndex(UInt(2), withAnimation: true)
    }
    
    func ChangeBadge(badgeValue: String) {
        self.showBadge(index: 2, badgeValue: badgeValue)
    }
    func SockerConnectionUnreadMsg(response: JSON) {
        self.ChangeBadge(badgeValue: String(response.getInt(key: .totalCount)))
    }
    
    /*
    func passValueToProfileVC(_ min:CGFloat, _ max:CGFloat,_ sliderType:SliderType) {
        
        if let viewController = carbonTabSwipeNavigation.pageViewController.viewControllers {
            print(viewController)
            if viewController.count > 0, let vc = viewController[0] as? ProfileVc {
//                self.makeUpdateProfileSettings(key: "distance", value: "\(slider.selectedMaxValue)")
//                 self.makeUpdateProfileSettings(key: "max_range", value: "\(slider.selectedMaxValue)", keyMax: "min_range", ValueMax: "\(slider.selectedMinValue)")
                let key = sliderType == SliderType.distance ? "distance" : "max_range"
                let value = "\(max)"
                let keyMax = sliderType == SliderType.age ? "min_range" : ""
                let valueMax = sliderType == SliderType.age ? "\(min)" : ""
                vc.updateRangeSeekSliderUI(sliderType, min, max)
                vc.makeUpdateProfileSettings(key: key, value: value, keyMax: keyMax, ValueMax: valueMax)
            }
        }
    }
    
    func redirectToRangeSliderUpdateVC(userData:JSON, sliderType:SliderType) {
        
        let vc = RangeSliderUpdateVC.init(nibName: "RangeSliderUpdateVC", bundle: nil)
        vc.setTheData(userData,sliderType)
        vc.handlerChagesValue = { [weak self] (min, max) in
            self?.passValueToProfileVC(min, max, sliderType)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }*/
}


extension MainVc {
    
    //MARK: - Carbon method
    func style() {
        let width = self.view.bounds.width
        
        carbonTabSwipeNavigation.toolbarHeight.constant = 60
        let tabWidth = (width / CGFloat(items.count))
        
        let indicatorcolor: UIColor = UIColor.clear
        let color: UIColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = color
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 2)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 3)
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 0.7
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        carbonTabSwipeNavigation.toolbar.layer.shadowRadius = 1.0
        
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.lightGray, font:themeFont(size: 15, fontname: .medium))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.appThemePurpleColor, font: themeFont(size: 15, fontname: .medium))
        carbonTabSwipeNavigation.toolbar.shadowImage(forToolbarPosition: .bottom)
        carbonTabSwipeNavigation.setCurrentTabIndex(1, withAnimation: true)
        carbonTabSwipeNavigation.pagesScrollView?.decelerationRate = .normal
        
        
//        carbonTabSwipeNavigation.pagesScrollView?.alwaysBounceHorizontal = false

//        carbonTabSwipeNavigation.pagesScrollView?.bounces = false

        if isPush {
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(pushIndex), withAnimation: true)
            self.isPush = false
        }        
    }
    
    func showBadge(index: Int, badgeValue: String) {
        let tabView: UIView? = carbonTabSwipeNavigation.carbonSegmentedControl!.segments![index] as? UIView
        
//        if badgeValue == "0" {
//            if let viewWithTag = self.view.viewWithTag(101) {
//                viewWithTag.removeFromSuperview()
//            }
//        }
        
        if badgeValue != "0" {
            if lastBadge == badgeValue {
                return
            }
            lastBadge = badgeValue
            if let viewWithTag = tabView!.viewWithTag(101) {
                for v in viewWithTag.subviews{
                    if v is UILabel{
                        v.removeFromSuperview()
                        viewWithTag.addSubview(addBadgeLabel(count: badgeValue))
                    }
                }
            }
            else {
                tabView?.addSubview(addBadgeValue(count: badgeValue))
            }
        }
        else {
            lastBadge = ""
            if let viewWithTag = self.view.viewWithTag(101) {
                viewWithTag.removeFromSuperview()
            }
        }
    }
    
    func addBadgeValue(count: String)-> UIView {
        let starWidth = self.widthOfString(str: count, usingFont: UIFont.systemFont(ofSize: 14)) + 10
        let newWidth = starWidth > 25 ? starWidth : 25
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: newWidth, height: 25))
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "\(count)"
        
        let viewDemo = UIView()
        viewDemo.tag = 101
        viewDemo.backgroundColor = UIColor.red
        let frame = UIScreen.main.bounds.width / 8
        viewDemo.frame = CGRect(x: frame + 5, y: 5, width: newWidth, height: 25)
        viewDemo.layer.cornerRadius = 25 / 2
        
        viewDemo.addSubview(label)
        return viewDemo
    }
    
    func addBadgeLabel(count: String)-> UILabel {
        let starWidth = self.widthOfString(str: count, usingFont: UIFont.systemFont(ofSize: 14)) + 10
        let newWidth = starWidth > 25 ? starWidth : 25
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: newWidth, height: 25))
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "\(count)"
        return label
    }
    
    func widthOfString(str: String, usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = str.size(withAttributes: fontAttributes)
        return size.width
    }
}

extension MainVc : CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        selectedIndexVC = Int(index)
        switch index {
        case 0:
            let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
            //            vc.tabIndex = selectedIndexVC
            /*vc.handlerPassUserDataAndSliderType = { [weak self] (userData, sliderType) in
                self?.redirectToRangeSliderUpdateVC(userData: userData, sliderType: sliderType)
            }*/
            return vc
        case 1 :
            let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SwipeCardVc") as! SwipeCardVc
            vc.delegateSwipe = self
            vc.delegate = self
            //            vc.tabIndex = selectedIndexVC
            return vc
        case 2 :
            let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MatchCardVc") as! MatchCardVc
            vc.delegate = self
            //            vc.tabIndex = selectedIndexVC
            return vc
        case 3 :
            let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            vc.delegate = self
            //            vc.tabIndex = selectedIndexVC
            return vc
        default:
            let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
            vc.delegate = self
            //            vc.tabIndex = selectedIndexVC
            return vc
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        selectedIndexVC = Int(index)
        SELECTED_TAB = Int(index)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshView"), object: nil)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        selectedIndexVC = Int(index)
        SELECTED_TAB = Int(index)
        //        let vc = carbonTabSwipeNavigation.viewControllers[index] as! ProfileVc
        
        //   if scrollView.contentOffset.y == self.viewDetail.bounds.height {
        //        vc.tblPoolList.isScrollEnabled = true
        //        vc.tblPoolList.contentOffset = CGPoint.zero
    }
}
