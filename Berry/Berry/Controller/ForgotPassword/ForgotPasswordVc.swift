//
//  ForgotPasswordVc.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForgotPasswordVc: UIViewController, CountryCodeDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var lblPasswordMsg: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgCountryCode: UIImageView!
    @IBOutlet weak var viewCountry: UIView!
    
    //MARK: - Variable
    var countryDialCode: String = "+1"
    var countryName: String = "United States"
    var countryCode: String = "US"
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Forgot_Password_key"))
        AppDelegate.shared.checkOldUser()
    }
    
    func setupUI() {
        self.viewCountry.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.viewCountry.layer.cornerRadius = 8
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
        
        lblPasswordMsg.text = getCommonString(key: "forgot_password_msg_key")
        lblPasswordMsg.font = themeFont(size: 16, fontname: .light)
        lblPasswordMsg.textColor = UIColor.black
        
        lblMobileNumber.textColor = UIColor.black
        lblMobileNumber.font = themeFont(size: 17, fontname: .bold)
        lblMobileNumber.text = getCommonString(key: "Mobile_number_key")
        
        txtMobileNumber.setupCustomTextField()
        txtMobileNumber.delegate = self
        
        btnNext.setTitle(getCommonString(key: "NEXT_key"), for: .normal)
        btnNext.setupShadowThemeButtonUI()
        
        addDoneButtonOnKeyboard(textfield: txtMobileNumber)
    }
    
    func validationLogin() {
        if self.txtMobileNumber.text! == "" {
            AlertView(title: "Failed", message: "Please enter mobile number")
        }
        else {
            self.makeForgotPassword()
        }
    }
    
    func makeForgotPassword() {
        let param = ["mobile_no":self.txtMobileNumber.text!, "co": self.countryDialCode.dropFirst()] as [String : Any]
        ApiManager.shared.MakePostAPI(name: VERIFY_MOBILE, params: param, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OTPVc") as! OTPVc
                    obj.isForgot = true
                    obj.mobileNo = self.txtMobileNumber.text!
                    obj.countryDialCode = self.countryDialCode
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeForgotPassword()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        self.present(obj, animated:  false, completion: nil)
    }
    
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
    }
}


//MARK: - IBAction
extension ForgotPasswordVc {
    @IBAction func btnNextTapped(_ sender: UIButton) {
        self.validationLogin()
    }
}


extension ForgotPasswordVc : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if textField == txtMobileNumber {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
