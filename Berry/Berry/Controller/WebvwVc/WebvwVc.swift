//
//  WebvwVc.swift
//  Berry
//
//  Created by YASH on 01/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import WebKit

enum checkParentControllerForWebVw {
    case forLicenses
    case forPrivacyPolicy
    case forTermsofService
}

class WebvwVc: UIViewController {
    
    
    //MARK: - Variables
    var selectedController = checkParentControllerForWebVw.forLicenses
    var url : String = ""
    var webVw = WKWebView()

    //MARL: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webVw.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.view.bounds.height)
        self.webVw.backgroundColor = .clear
        self.view.addSubview(self.webVw)
        
        webVw.uiDelegate = self
        let webRequest : NSURLRequest = NSURLRequest(url: URL(string: url)!)
        webVw.load(webRequest as URLRequest)
        
        
        let swipeRightRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(recognizer:)))
        swipeRightRecognizer.direction = .right        
        webVw.addGestureRecognizer(swipeRightRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .forLicenses {
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Licenses_key"))
        }
        else if selectedController == .forPrivacyPolicy {
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Privacy_policy_key"))
        }
        else if selectedController == .forTermsofService {
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Terms_of_service_key"))
        }
    }
    
    @objc private func handleSwipe(recognizer: UISwipeGestureRecognizer) {
        if (recognizer.direction == .right) {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension WebvwVc: WKNavigationDelegate, WKUIDelegate{
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
    }
}
