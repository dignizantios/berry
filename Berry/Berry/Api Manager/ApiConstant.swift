//
//  ApiConstant.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseMessaging

//MARK: - Main URLS
//let SERVER_URL = "http://berry.oengines.com:4001/"
let SERVER_URL = "http://ec2-13-59-127-40.us-east-2.compute.amazonaws.com:4001/" //http://ec2-3-14-146-40.us-east-2.compute.amazonaws.com:4001/"
let BASE_URL = SERVER_URL + "v1/"


//MARK: - URLS Names
let LOGIN = "login"
let REGISTER = "register"
let GENERATE_OTP = "generateOtp"
let VERIFY_OTP = "verifyOtp"
let VERIFY_RESET_OTP = "verifyResetOtp"
let UPDATE_PROFILE = "updateProfile"
let FORGOT_PASSWORD = "forgetPassword"
let PROFILE = "profile"
let VERIFY_MOBILE = "verifyMobile"
let CHANGE_PASSWORD = "changePassword"
let DELETE_ACCOUNT = "deleteAccount"
let DELETE_PROIFLE = "deleteProfile"
let DISCOVER_MY_DISTANCE = "discoverMyDistance"
let ACTION_DESCOVERED = "actionDiscovered"
let GET_NOTIFICATIONS = "getNotification"
let SEND_REPORT = "sendReport"
let SEND_FEEDBACK = "sendFeedback"
let LOGOUT = "logoutAccount"
let UNMATCH = "unmatch"
let TOKEN_REPLACE_REMOVE = "TokenReplaceRemove"
let VERIFY_IN_SUB = "verifyInSub"
let PREMIUM_ACCOUNT = "PremiumAccount"
let OUTTIME = "outTime"
let REPORT_CRASH_DATE = "InsertLogModel"


//SandBox: “https://sandbox.itunes.apple.com/verifyReceipt”
//iTunes Store : “https://buy.itunes.apple.com/verifyReceipt”
enum IN_APP_PURCHASE:String {
    case SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt"
    case ITUNES = "https://buy.itunes.apple.com/verifyReceipt"
}

var userDetails: JSON = loadJSON(key: USER_DETAILS_KEY)

func getRealFCMToken() -> String {
    return Messaging.messaging().fcmToken ?? ""
}


func removeLogoutOldToken() {
    userDefault.removeObject(forKey: "LogoutUserId")
    userDefault.removeObject(forKey: "FCMLogoutToken")
    userDefault.synchronize()
}

func getLogoutUserId() -> String {
    if isKeyPresentInUserDefaults(key: "LogoutUserId") {
        return userDefault.value(forKey: "LogoutUserId") as? String ?? ""
    }
    return ""
}

func saveLogoutUserId(UserId: String) {
    userDefault.setValue(UserId, forKey: "LogoutUserId")
    userDefault.synchronize()
}

func getFCMLogoutToken() -> String {
    if isKeyPresentInUserDefaults(key: "FCMLogoutToken") {
        return userDefault.value(forKey: "FCMLogoutToken") as? String ?? ""
    }
    return ""
}

func saveFCMLogoutToken(token: String) {
    userDefault.setValue(token, forKey: "FCMLogoutToken")
    userDefault.synchronize()
}

func getFCMToken() -> String {
//    return Messaging.messaging().fcmToken ?? ""
    if isKeyPresentInUserDefaults(key: "FCMToken") {
        return userDefault.value(forKey: "FCMToken") as? String ?? ""
    }
    return ""
}

func saveFCMToken(token: String) {
    userDefault.setValue(token, forKey: "FCMToken")
    userDefault.synchronize()
}

//MARK: - API Enums
enum APIKeys: String {
    case data = "data"
    case msg = "msg"
    case error = "error"
    case userid = "userid"
    case oppid = "oppid"
    case username = "username"
    case type = "type"
    case name = "name"
    case myprofile = "myprofile"
    case profile = "profile"
    case birth = "birth"
    case about = "about"
    case gender = "gender"
    case hobbies = "hobbies"
    case job = "job"
    case insta = "insta"
    case insta_links = "insta_links"
    case show_male = "show_male"
    case show_female = "show_female"
    case distance = "distance"
    case min_range = "min_range"
    case max_range = "max_range"
    case myfeed = "myfeed"
    case missing = "missing"
    case location_name = "location_name"
    case years = "years"
    case verified = "verified"
    case mobile_no = "mobile_no"
    case createdat = "createdat"
    case title = "title"
    case en = "en"
    case selected = "selected"
    case status = "status"
    case question = "question"
    case bothUserInfo = "bothUserInfo"
    case ans = "ans"
    case roomId = "roomId"
    case date = "date"
    case opp = "opp"
    case _id = "_id"
    case superlike = "superlike"
    case message = "message"
    case totalCount = "totalCount"
    case read = "read"
    case superLike = "superLike"
    case pushType = "gcm.notification.type"
    case pushUserid = "gcm.notification.userid"
    case match = "match"
    case profileDetails = "profileDetails"
    case isAds = "isAds"
    case isVideo = "isVideo"
    case screenshots = "screenshots"
    case url = "url"
    case co = "co"
    case pagination = "pagination"
    case next_url = "next_url"
    case carousel_media = "carousel_media"
    case acountStatus = "acountStatus"
    case transaction_id = "transaction_id"
    case aedate = "aedate"
    case aadate = "aadate"
}

extension JSON {
    func getString(key: APIKeys) -> String {
        return self[key.rawValue].string ?? ""
    }
    
    func getInt(key: APIKeys) -> Int {
        return self[key.rawValue].int ?? 0
    }
    
    func getBool(key: APIKeys) -> Bool {
        return self[key.rawValue].bool ?? false
    }
    
    func getDouble(key: APIKeys) -> Double {
        return self[key.rawValue].double ?? 0.0
    }
    
    func getFloat(key: APIKeys) -> Float {
        return self[key.rawValue].float ?? 0.0
    }
    
    func getDictionary(key: APIKeys) -> JSON {
        return JSON(self[key.rawValue].dictionary ?? [String: JSON]())
    }
    
    func getArray(key: APIKeys) -> [JSON] {
        return self[key.rawValue].array ?? [JSON]()
    }
    
    mutating func setValue(key: APIKeys, value: String) {
        self[key.rawValue].string = value
    }
    
    mutating func setIntValue(key: APIKeys, value: Int) {
        self[key.rawValue].int = value
    }
    
    mutating func setBoolValue(key: APIKeys, value: Bool) {
        self[key.rawValue].bool = value
    }
    
    mutating func setDictionaryValue(key: APIKeys, value: [String : JSON]) {
        self[key.rawValue].dictionaryObject = value
    }
}

//MARK: - Extension
extension NSDictionary {
    func getString(key: APIKeys) -> String {
        return self.value(forKey: key.rawValue) as? String ?? ""
    }
    
    func getInt(key: APIKeys) -> Int {
        return self.value(forKey: key.rawValue) as? Int ?? 0
    }
    
    func getBool(key: APIKeys) -> Bool {
        return self.value(forKey: key.rawValue) as? Bool ?? false
    }
    
    func getDouble(key: APIKeys) -> Double {
        return self.value(forKey: key.rawValue) as? Double ?? 0.0
    }
    
    func getFloat(key: APIKeys) -> Float {
        return self.value(forKey: key.rawValue) as? Float ?? 0.0
    }
    
    func getDictionary(key: APIKeys) -> NSDictionary {
        return self.value(forKey: key.rawValue) as? NSDictionary ?? NSDictionary()
    }
    
    func getArray(key: APIKeys) -> NSArray {
        return self.value(forKey: key.rawValue) as? NSArray ?? NSArray()
    }
    
    func getMutableArray(key: APIKeys) -> NSMutableArray {
        return self.value(forKey: key.rawValue) as? NSMutableArray ?? NSMutableArray()
    }
    
    func setValue(key: APIKeys, value: String) {
        self.setValue(value, forKey: key.rawValue)
    }
}

