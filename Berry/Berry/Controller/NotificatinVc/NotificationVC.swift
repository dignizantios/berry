//
//  NotificationVC.swift
//  Berry
//
//  Created by YASH on 31/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import KRPullLoader
import SDWebImage

class NotificationVC: UIViewController, KRPullLoadViewDelegate, UnMatchUserDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var lblNotFound: UILabel!
    
    //MARK: - Variables
    let refreshView = KRPullLoadView()
    let loadMOreView = KRPullLoadView()
    var index: Int = 1
    var totalCount: Int = 0
    var jsonResponse : [JSON] = [JSON]()
    weak var delegate: ChangeIndexCarbonkitDelegate?
    var isLoaded:Bool = false

    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        tblNotification.estimatedRowHeight = 80.0
        tblNotification.rowHeight = UITableView.automaticDimension
        tblNotification.register(UINib(nibName: "Notificationcell", bundle: nil), forCellReuseIdentifier: "Notificationcell")
        tblNotification.tableFooterView = UIView()
        
        refreshView.delegate = self
        loadMOreView.delegate = self
        tblNotification.addPullLoadableView(refreshView, type: .refresh)
        tblNotification.addPullLoadableView(loadMOreView, type: .loadMore)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshView(notification:)), name: NSNotification.Name(rawValue: "refreshView"), object: nil)
        
        lblNotFound.isHidden  = true
        self.lblNotFound.underline()

        self.index = 1
        DispatchQueue.main.async {
            self.getNotifications(progress: !self.isLoaded)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        self.lblNotFound.isUserInteractionEnabled = true
        self.lblNotFound.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.index = 1
//        self.getNotifications()
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        self.delegate?.ChangeIndexCarbonkit(index: 1)
    }
    
    @objc func refreshView(notification : Notification) {
        if SELECTED_TAB == 3 {
            self.index = 1
            DispatchQueue.main.async {
                self.getNotifications(progress: !self.isLoaded)
            }
        }
    }
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    print(self.totalCount)
                    print(self.jsonResponse.count)
                    if self.totalCount > self.jsonResponse.count {
                        DispatchQueue.main.async {
                            self.getNotifications(page: self.index, progress: false)
                        }
                    }
                    completionHandler()
                }
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case .pulling(_, _):
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                self.getNotifications(page: 1, progress: false)
                completionHandler()
            }
        }
    }
    
    func getNotifications(page: Int = 1, progress: Bool = true) {
        let param = ["userid":userDetails.getString(key: .userid),"page":page] as [String : Any]
        print(param)
        ApiManager.shared.MakePostAPI(name: GET_NOTIFICATIONS, params: param, progress: progress, vc: self) { (result, error) in
            self.isLoaded = true
            if result != nil {
                let json = JSON(result!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    let data = json.getArray(key: .data)
                    self.totalCount = json.getInt(key: .totalCount)
                    if page == 1 {
                        self.index += 1
                        self.jsonResponse = data
                    }
                    else {
                        self.index += 1
                        for notification in data {
                            self.jsonResponse.append(notification)
                        }
                    }
                    self.lblNotFound.text = "No notifications yet. Get swiping!"//"There is no more notification found at this time."
                    self.lblNotFound.underline()
                    self.lblNotFound.isHidden = self.jsonResponse.count == 0 ? false : true
                    self.tblNotification.reloadData()
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                    self.lblNotFound.text = msg
                    self.lblNotFound.underline()
                }
            }
            else {
                self.getNotifications(page: page, progress: progress)
//                AlertView(title: "Failed", message: error!)
//                self.lblNotFound.text = error!
            }
            self.lblNotFound.isHidden = self.jsonResponse.count == 0 ? false : true
        }
    }
}

extension NotificationVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notificationcell") as!  Notificationcell
        let details = self.jsonResponse[indexPath.row]
        cell.img.tag = indexPath.row
        cell.img.sd_setImage(with: URL(string: details.getString(key: .profile)), placeholderImage: UIImage(named: "user_placeholder_big"), completed: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openProfile(sender:)))
        cell.img.isUserInteractionEnabled = true
        cell.img.addGestureRecognizer(tap)
        
//        if details.getString(key: .type) == "match" {
            let name = details.getString(key: .username).capitalized
            let title = details.getString(key: .title)
            let text = name + " " + title// + " " + "with you."
            cell.lblDescription.attributedText = text.setColor(str: name)
            cell.lblDescription.isUserInteractionEnabled = true
            cell.lblDescription.onClick = {
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MatchCardProfileVc") as! MatchCardProfileVc
                obj.oppUserId = self.jsonResponse[indexPath.row].getString(key: .oppid)
                self.navigationController?.pushViewController(obj, animated: true)
            }
//        }
//        else {
        //  cell.lblDescription.text = "New"
//        }
        cell.lblTime.text = timeAgoSinceDate(str: UTCToLocal(date: details.getString(key: .createdat), messageData: details), numericDates: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func openProfile(sender: UITapGestureRecognizer)
    {
        let tag = sender.view!.tag
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MatchCardProfileVc") as! MatchCardProfileVc
        obj.oppUserId = self.jsonResponse[tag].getString(key: .oppid)
        obj.delegate = self
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func UnMatchUserDidFinish(userId: String) {
        self.index = 1
        self.getNotifications(progress: false)
    }
}

extension String {
    func setColor(str: String) -> NSAttributedString {
        let range = (self as NSString).range(of: str)
        let attributedString = NSMutableAttributedString(string:self)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 58/255, green: 26/255, blue: 90/255, alpha: 1) , range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: range)
        return attributedString
    }
}
