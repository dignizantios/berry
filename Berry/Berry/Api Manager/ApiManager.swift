//
//  ApiManager.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import Reachability
import SystemConfiguration

struct AlamofireAppManager {
    static let shared: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        configuration.allowsCellularAccess = true
//        configuration.httpMaximumConnectionsPerHost = 1
        configuration.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData

        if #available(iOS 11.0, *) {
            configuration.waitsForConnectivity = true
        } else {}
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
}

class ApiManager: NSObject {
    
    static var shared = ApiManager()
    
    func showProgress(vc: UIViewController) {
        DispatchQueue.main.async {
            let LoaderString:String = "Loading..."
            let LoaderType:Int = 32
            let LoaderSize = CGSize(width: 30, height: 30)
            
            vc.startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        }
    }
    
    func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
    //MARK: - Post
    func MakePostAPI(name:String, params:[String:Any], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void) {       
       
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            vc.view.endEditing(true)
        }
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            AlertView(title: "Failed", message: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-Type": "application/json"]
        print(headers)
        let url = BASE_URL + name
        print(url)
        print(params)
        AlamofireAppManager.shared.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            
            if progress {
                DispatchQueue.main.async {
                    vc.stopAnimating()
                }
            }

            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    let dict = response.result.value as? NSDictionary
                    let error = dict?.value(forKey: "error") as? String
                    if error?.capitalized == "B" || error?.capitalized == "C" {
                        self.redirectToLoginScreen()
                        if name == LOGIN || name == REGISTER {
                            let msg = JSON(dict!).getString(key: .msg)
                            AlertView(title: "Failed", message: msg)
                        }
                        return
                    }
                    
                    let msg = dict?.value(forKey: "msg") as? String
                    if (msg == "user not found!" || msg == "Invalid phone number")  && name != VERIFY_MOBILE {
                        self.redirectToLoginScreen()
                        return
                    }
                    
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
//                let status = response.response?.statusCode
//                if status == nil {
//                    AlertView(title: "Failed", message: "Server not working")
//                    return
//                }
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    func MakePostAPIWithoutVC(name:String, params:[String:Any], isShowError:Bool = true, completionHandler: @escaping (NSDictionary?, String?)-> Void) {
        guard ReachabilityTest.isConnectedToNetwork() else {
            if isShowError {
                AlertView(title: "Failed", message: "No internet connection available")
            }
            return
        }
        
        let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-Type": "application/json"]
        
        let url = BASE_URL + name
        print(url)
        print(params)
        AlamofireAppManager.shared.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            print(response)

            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    let dict = response.result.value as? NSDictionary
                    let msg = dict?.value(forKey: "msg") as? String
                    if (msg == "user not found!" || msg == "Invalid phone number") && name != VERIFY_MOBILE {
                        self.redirectToLoginScreen()
                        return
                    }
                    
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
//                let status = response.response?.statusCode
//                if status == nil {
//                    AlertView(title: "Failed", message: "Server not working")
//                    return
//                }
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
   
    func redirectToLoginScreen() {
        deletJSON(key: USER_DETAILS_KEY)
        Defaults.removeObject(forKey: "facebookID")
        Defaults.removeObject(forKey: "authoToken")
        Defaults.removeObject(forKey: "InstagramID")
        Defaults.synchronize()
        
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RootNav") as! UINavigationController
        AppDelegate.shared.window?.rootViewController = vc
    }
    
    //MARK: - Get
    func MakeGetAPI(name:String, params:[String:Any], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void) {
        vc.view.endEditing(true)
        guard ReachabilityTest.isConnectedToNetwork() else {
            AlertView(title: "Failed", message: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-Type": "application/json"]
        print(headers)
        let url = BASE_URL + name
        print(url)
        print(params)
        AlamofireAppManager.shared.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            
            if progress {
                DispatchQueue.main.async {
                    vc.stopAnimating()
                }
            }

            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    let dict = response.result.value as? NSDictionary
                    let error = dict?.value(forKey: "error") as? String
                    if error?.capitalized == "B" || error?.capitalized == "C" {
                        self.redirectToLoginScreen()
                        if name == LOGIN || name == REGISTER {
                            let msg = JSON(dict!).getString(key: .msg)
                            AlertView(title: "Failed", message: msg)
                        }
                        return
                    }
                    let msg = dict?.value(forKey: "msg") as? String
                    if (msg == "user not found!" || msg == "Invalid phone number") && name != VERIFY_MOBILE {
                        self.redirectToLoginScreen()
                        return
                    }
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
//                let status = response.response?.statusCode
//                if status == nil {
//                    AlertView(title: "Failed", message: "Server not working")
//                    return
//                }
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    
    //MARK: - Put
    func MakePutAPI(name:String, params:[String:Any], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void) {
        vc.view.endEditing(true)
        guard ReachabilityTest.isConnectedToNetwork() else {
            AlertView(title: "Failed", message: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        let url = BASE_URL + name
        
        AlamofireAppManager.shared.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
//            print(response)
            
            if progress {
                DispatchQueue.main.async {
                    vc.stopAnimating()
                }
            }

            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    let dict = response.result.value as? NSDictionary
                    let error = dict?.value(forKey: "error") as? String
                    if error?.capitalized == "B" || error?.capitalized == "C" {
                        self.redirectToLoginScreen()
                        if name == LOGIN || name == REGISTER {
                            let msg = JSON(dict!).getString(key: .msg)
                            AlertView(title: "Failed", message: msg)
                        }
                    }
                    
                    let msg = dict?.value(forKey: "msg") as? String
                    if (msg == "user not found!" || msg == "Invalid phone number") && name != VERIFY_MOBILE {
                        self.redirectToLoginScreen()
                        return
                    }
                    
                    completionHandler(response.result.value as? NSDictionary, nil)
                }
                else {
                    completionHandler(nil, SERVER_VALIDATION)
                }
            case .failure( _):
//                let status = response.response?.statusCode
//                if status == nil {
//                    AlertView(title: "Failed", message: "Server not working")
//                    return
//                }
                completionHandler(nil, SERVER_VALIDATION)
            }
        }.resume()
    }
    
    //MARK: - Post with Image upload
    func MakePostWithImageAPI(name:String, params:[String:Any], images:[UIImage], imagesURL:[String], imagesInstaURL:[String] = [], progress: Bool = true, vc: UIViewController, completionHandler: @escaping (NSDictionary?, String?)-> Void)
    {
        vc.view.endEditing(true)
        guard ReachabilityTest.isConnectedToNetwork() else {
            AlertView(title: "Failed", message: "No internet connection available")
            return
        }
        
        if progress {
            showProgress(vc: vc)
        }
        
        let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"multipart/form-data"]
        
        let url = BASE_URL + name
        print(url)
        print(params)
        AlamofireAppManager.shared.upload(multipartFormData: { (multipartFormData) in
            
            for image in images {
                multipartFormData.append(image.jpeg(.medium)!, withName: "profile",fileName: "\(randomString(length: 5)).jpg", mimeType: "image/jpg")
            }
            
            if imagesURL.count != 0 {
                let value = jsonString(from: imagesURL)
                multipartFormData.append(value!.data(using: String.Encoding.utf8)!, withName: "links")
            }            
            
            if imagesInstaURL.count != 0 {
                let value = jsonString(from: imagesInstaURL)
                multipartFormData.append(value!.data(using: String.Encoding.utf8)!, withName: "insta_links")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            
            print(multipartFormData)
        }, to: url, method:.post,
           headers:headers, encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    if progress {
                        DispatchQueue.main.async {
                            vc.stopAnimating()
                        }
                    }
//                    print(response)
                    
                    if(response.error == nil) {
                        let dict = response.result.value as? NSDictionary
                        let error = dict?.value(forKey: "error") as? String
                        if error?.capitalized == "B" || error?.capitalized == "C" {
                            self.redirectToLoginScreen()
                            if name == LOGIN || name == REGISTER {
                                let msg = JSON(dict!).getString(key: .msg)
                                AlertView(title: "Failed", message: msg)
                            }
                            return
                        }
                        
                        let msg = dict?.value(forKey: "msg") as? String
                        if (msg == "user not found!" || msg == "Invalid phone number") && name != VERIFY_MOBILE {
                            self.redirectToLoginScreen()
                            return
                        }
                        
                        completionHandler(response.result.value as? NSDictionary, nil)
                    }
                    else {
                        completionHandler(nil, SERVER_VALIDATION)
                    }
                })
            case .failure( _):               
                completionHandler(nil, SERVER_VALIDATION)
            }
        })
    }
}
