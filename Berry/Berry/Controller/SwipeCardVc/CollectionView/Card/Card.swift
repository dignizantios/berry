//
//  Card.swift
//  testingTinderSwipe
//
//  Created by om on 10/13/18.
//  Copyright © 2018 Nicky. All rights reserved.
//

import UIKit

class Card: UIView {

    @IBOutlet var collectionViewGallery : UICollectionView!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var vwInsta : UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        vwInsta.layer.cornerRadius = vwInsta.frame.size.height/2
        vwInsta.layer.masksToBounds = true
    }
}
