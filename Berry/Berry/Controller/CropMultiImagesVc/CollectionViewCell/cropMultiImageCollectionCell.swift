//
//  cropMultiImageCollectionCell.swift
//  Berry
//
//  Created by YASH on 19/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class cropMultiImageCollectionCell: UICollectionViewCell,UIScrollViewDelegate {

    @IBOutlet weak var imgSelectedPhotos: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        
        scrollView.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
        tapGesture.numberOfTapsRequired = 2
        
        scrollView.addGestureRecognizer(tapGesture)
        // Initialization code
    }

    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgSelectedPhotos
    }
    
    @objc func handleDoubleTap(gestureRecognizer: UIGestureRecognizer) {
        if(self.scrollView.zoomScale > self.scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        }
        else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }    
}
