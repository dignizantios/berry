//
//  LeftSideImageTblCell.swift
//  Berry
//
//  Created by Anil on 22/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class LeftSideImageTblCell: UITableViewCell {

    @IBOutlet weak var imgLeftProfile: UIImageView!
    @IBOutlet weak var imgLeftMessage: UIImageView!
    @IBOutlet weak var vwBackColor: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var height: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgLeftProfile.layer.cornerRadius = imgLeftProfile.layer.bounds.width/2
        imgLeftProfile.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
