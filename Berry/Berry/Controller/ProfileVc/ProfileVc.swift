//
//  ProfileVc.swift
//  Berry
//
//  Created by YASH on 30/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import RangeSeekSlider
import SwiftyJSON
import InstagramLogin
import UserNotifications
import Firebase
import StoreKit

//import LNICoverFlowLayout
//protocol SliderDelegate:class {
//    func updateSliderValues(_ sliderType:SliderType,_ minValue:CGFloat,_ maxValue:CGFloat)
//}

class ProfileVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var coverFlowLayout: LNICoverFlowLayout!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    
    @IBOutlet weak var viewCancelSubscription: UIView!
    @IBOutlet weak var heightCacelSubscription: NSLayoutConstraint!
    
    //Profile Outlet
    @IBOutlet weak var viewChangePassword: UIView!
    @IBOutlet weak var vwMainProfile: UIView!
    @IBOutlet weak var lblDateofBirth: UILabel!
    @IBOutlet weak var txtDate: CustomTextField!
    @IBOutlet weak var txtMonthName: CustomTextField!
    @IBOutlet weak var txtYear: CustomTextField!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var txtvwAbout: UITextView!
    @IBOutlet weak var vwShowMore: UIView!
    @IBOutlet weak var btnShowMore: CustomButton!
    @IBOutlet weak var vwJobTitle: UIView!
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var txtJobTitle: CustomTextField!
    @IBOutlet weak var vwFavoutireSocialActivity: UIView!
    @IBOutlet weak var lblFavouriteSocialActivity: UILabel!
    @IBOutlet weak var txtSocialActivity: CustomTextField!
    @IBOutlet weak var vwSocialMedia: UIView!
    @IBOutlet weak var lblSocialMedia: UILabel!
    @IBOutlet weak var switchSocialMedia: UISwitch!
    @IBOutlet weak var vwGender: UIView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var vwSepratore: UIView!
    @IBOutlet weak var vwApplyBtn: UIView!
    @IBOutlet weak var btnApply: CustomButton!
    
    //settings outlet
    
    @IBOutlet weak var vwMainSetting: UIView!
    @IBOutlet weak var lblDiscoverSettings: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var lblShowme: UILabel!
    @IBOutlet weak var lblMen: UILabel!
    @IBOutlet weak var switchMen: UISwitch!
    @IBOutlet weak var lblWomen: UILabel!
    @IBOutlet weak var switchWomen: UISwitch!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDistanceValue: UILabel!
    @IBOutlet weak var rangeDistance: RangeSeekSlider!
    @IBOutlet weak var lblAgeRange: UILabel!
    @IBOutlet weak var lblRangeValue: UILabel!
    @IBOutlet weak var rangeAgeRange: RangeSeekSlider!
    @IBOutlet weak var lblSharemyFeed: UILabel!
    @IBOutlet weak var switchShareMyFeed: UISwitch!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    @IBOutlet weak var btnLicense: UIButton!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnTermsofService: UIButton!
    @IBOutlet weak var btnLogout: CustomButton!
    @IBOutlet weak var txtCountHobbies: UILabel!
    @IBOutlet weak var txtCountOccupation: UILabel!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!

    @IBOutlet weak var instaCollectionView: UICollectionView!
    @IBOutlet weak var instaCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblPremium: UILabel!
    @IBOutlet weak var btnPremium: UIButton!
    
    //MARK: - Variable
    var originalItemSize = CGSize.zero
    var originalCollectionViewSize = CGSize.zero
    var arrayImageUser = NSMutableArray()
    var profileDetails : JSON = JSON()
    var isMaleSelected:Bool = true
    var birthSelected:String = ""
    var arrayOfURLimagesForInsta = [String]()
    var arrayOfURLimagesForInstaJSON = [JSON]()
    static var shared = ProfileVc()
    weak var delegate: ChangeIndexCarbonkitDelegate?

    var isProfileChnaged:Bool = false
    var isDOBChanged:Bool = false
    var isAboutChanged:Bool = false
    var isHobbiesChanged:Bool = false
    var isOccupationChanged:Bool = false
    var isgenderChanged:Bool = false
    var isInstagramChanged:Bool = false
    var cellPortraitSize = CGSize()

    var productIDs: Array<String?> = []
    var productsArray: Array<SKProduct?> = []
    
    let defaults = UserDefaults.standard
    var isLoaded:Bool = false
    var showInstaLines:Int = 50
    var showInstaColumn:Int = 4
    var isPhotoViewed:Bool = false
    var isLocationViewed:Bool = false

    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.bounces = false
        originalItemSize = coverFlowLayout.itemSize
        originalCollectionViewSize = collectionImages.bounds.size
        
        collectionImages.register(UINib(nibName: "collectionUserImageCell", bundle: nil), forCellWithReuseIdentifier:"collectionUserImageCell")
        instaCollectionView.register(UINib(nibName: "YMSPhotoCell", bundle: nil), forCellWithReuseIdentifier:"YMSPhotoCell")

        setupDefaultEditProfile()
        
        setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setValueOfLocation), name: NSNotification.Name(rawValue: "addAddressInProfile"), object: nil)
        
        productIDs.append("BerryProductRenew")
        SKPaymentQueue.default().add(self)

//        SKPaymentQueue.default().restoreCompletedTransactions()
        requestProductInfo()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isPhotoViewed && !isLocationViewed {
            self.getProfileDetails(isLoading: !self.isLoaded)
        }
        self.isPhotoViewed = false
        self.isLocationViewed = false
        
        if isShowSetting {
            isShowSetting = false
            self.openSetting()
        }
        
//        if !self.profileDetails.isEmpty {
//            let details = self.profileDetails.getDictionary(key: .myprofile)
//
//            let accountStatus = details.getString(key: .acountStatus).uppercased()
//            if accountStatus == "N" {
//                IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                self.lblPremium.text = "Get Premium account"
//                self.btnPremium.isUserInteractionEnabled = true
//            }
//            else {
//                let aedate = details.getString(key: .aedate)
//                if aedate != "" {
//                    let time = self.convertFormate(str: aedate)
//                    self.lblPremium.text = "Premium member \nExpire on: \(time)"
//                    IM_AD_INSERTION_POSITION = 0
//                    self.btnPremium.isUserInteractionEnabled = false
//                }
//                else {
//                    IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                    self.lblPremium.text = "Get Premium account"
//                    self.btnPremium.isUserInteractionEnabled = true
//                }
//            }

//            let aedate = details.getString(key: .aedate)
//            if aedate != "" {
//                let date = self.convertDate(str: aedate)
//                if date > Date() {
//                    let time = self.convertFormate(str: aedate)
//                    self.lblPremium.text = "Premium member \nExpire on: \(time)"
//                    IM_AD_INSERTION_POSITION = 0//accountStatus == "N" ? 10 : 0
//                    self.btnPremium.isUserInteractionEnabled = false
//                }
//                else {
//                    IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                    self.lblPremium.text = "Get Premium account"
//                    self.btnPremium.isUserInteractionEnabled = true
//                }
//            }
//            else {
//                IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                self.lblPremium.text = "Get Premium account"
//                self.btnPremium.isUserInteractionEnabled = true
//            }
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            self.collectionImages.reloadData()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // We should invalidate layout in case we are switching orientation.
        // If we won't do that we will receive warning from collection view's flow layout that cell size isn't correct.
        coverFlowLayout.invalidateLayout()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Now we should calculate new item size depending on new collection view size.
        coverFlowLayout.itemSize = CGSize(
            width: collectionImages.bounds.size.width * originalItemSize.width / originalCollectionViewSize.width,
            height: collectionImages.bounds.size.height * originalItemSize.height / originalCollectionViewSize.height
        )
        
        setInitialValues()
        
        // Forcely tell collection view to reload current data.
        collectionImages.setNeedsLayout()
        collectionImages.layoutIfNeeded()
        collectionImages.reloadData()
    }
    
    func showDiscoverSetting() {
        selectedBtn(selectedBtn: btnSetting, unselectedBtn: btnEdit)
        vwMainProfile.isHidden = true
        vwMainSetting.isHidden = false
    }
    
    func showCancelSubscriptions(isShow:Bool) {
        self.viewCancelSubscription.isHidden = !isShow
        self.heightCacelSubscription.constant = isShow ? 25 : 0
    }
    
    func setupUI() {
        [lblName,lblDateofBirth,lblAbout,lblJobTitle,lblFavouriteSocialActivity,lblSocialMedia,lblGender,lblDiscoverSettings,lblShowme,lblDistance,lblAgeRange,lblSharemyFeed].forEach { (lbl) in
            
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        [txtDate,txtYear,txtMonthName,txtJobTitle,txtSocialActivity].forEach { (txt) in
            txt?.setupCustomTextField()
            txt?.delegate = self
        }
        
        txtvwAbout.backgroundColor = UIColor.appThemetxtBackgroundColor
        txtvwAbout.font = themeFont(size: 16, fontname: .regular)
        txtvwAbout.contentInset = UIEdgeInsets(top: 2, left: 10, bottom: 2, right: 10)
        txtvwAbout.layer.cornerRadius = 8
        txtvwAbout.layer.masksToBounds = true
        txtvwAbout.delegate = self
        
        selectedGender(selectedBtn: btnMale, unselectedBtn: btnFemale)
        
        lblDateofBirth.text = getCommonString(key: "Date_of_birth_key")
        lblAbout.text = getCommonString(key: "About_key")
        
        //        lblJobTitle.text = getCommonString(key: "Job_title_key")
        //        lblFavouriteSocialActivity.text = getCommonString(key: "Favourite_social_activity_key")
        
        lblJobTitle.text = getCommonString(key: "Occupation_key")
        lblFavouriteSocialActivity.text = getCommonString(key: "Hobbies_key")

        
        //        lblSocialMedia.text = getCommonString(key: "Social_media_key")
        lblSocialMedia.text = getCommonString(key: "Connect_Instagram_key")
        lblGender.text = getCommonString(key: "Gender_key")
        
        btnShowMore.setTitle(getCommonString(key: "Show_more_key"), for: .normal)
        btnMale.setTitle(getCommonString(key: "Male_key"), for: .normal)
        btnFemale.setTitle(getCommonString(key: "Female_key"), for: .normal)
        
        btnApply.setTitle(getCommonString(key: "APPLY_key"), for: .normal)
        btnApply.setTitleColor(UIColor.appThemePurpleColor, for: .normal)
        btnApply.backgroundColor = UIColor.white
        btnApply.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        btnApply.shadowOffset = CGSize(width: 1, height: 1)
        btnApply.shadowOpacity = 0.2
        btnApply.shadowColor = UIColor.black
        
        //Setting side
        
        lblLocation.font = themeFont(size: 16, fontname: .light)
        lblLocation.textColor = UIColor.appThemeLightGrayColor
        
        [lblMen,lblWomen].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 15, fontname: .light)
        }
        
        [lblPremium].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 15, fontname: .bold)
        }
        
        [lblDistanceValue,lblRangeValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .light)
            lbl?.textColor = UIColor.appThemePurpleColor
        }
        
        [btnChangePassword,btnLicense,btnPrivacyPolicy,btnTermsofService,btnDeleteAccount].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeDarkGrayColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .bold)
        }
        
        lblDiscoverSettings.text = getCommonString(key: "Discover_settings_key")
        lblShowme.text = getCommonString(key: "Show_me_key")
        lblLocation.text = getCommonString(key: "Location_key")
        lblMen.text = getCommonString(key: "Men_key")
        lblWomen.text = getCommonString(key: "Women_key")
        lblDistance.text = getCommonString(key: "Distance_key")
        lblAgeRange.text = getCommonString(key: "Age_range_key")
        lblSharemyFeed.text = getCommonString(key: "Share_my_feed_key")
        
        btnChangePassword.setTitle(getCommonString(key: "Change_password_key"), for: .normal)
        btnDeleteAccount.setTitle(getCommonString(key: "Delete_Account_key"), for: .normal)
        btnLicense.setTitle(getCommonString(key: "Licenses_key"), for: .normal)
        btnPrivacyPolicy.setTitle(getCommonString(key: "Privacy_policy_key"), for: .normal)
        btnTermsofService.setTitle(getCommonString(key: "Terms_of_service_key"), for: .normal)
        btnLogout.setTitle(getCommonString(key: "LOG_OUT_key"), for: .normal)
        
        btnLocation.setTitle(getCommonString(key: "My_Current_location_key"), for: .normal)
        btnLocation.setTitleColor(UIColor(red: 51.0/255.0, green: 72.0/255.0, blue: 173.0/255.0, alpha: 1.0), for: .normal)
        btnLocation.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        
        btnLogout.setupShadowThemeButtonUI()
        btnLogout.backgroundColor = UIColor.appThemeRedColor
        btnLogout.setTitleColor(UIColor.white, for: .normal)
        
        rangeDistance.delegate = self
        rangeAgeRange.delegate = self
        
        
        rangeAgeRange.handleDiameter = 30
        rangeAgeRange.selectedHandleDiameterMultiplier = 1
        rangeAgeRange.isUserInteractionEnabled = false
        
        rangeDistance.handleDiameter = 30
        rangeDistance.selectedHandleDiameterMultiplier = 1        
        rangeDistance.isUserInteractionEnabled = false
        
        setRangeForDistance(range: rangeDistance)
        setRangeForAgeRange(range: rangeAgeRange)
    }
    
    func setInitialValues() {
        coverFlowLayout.maxCoverDegree = 0
        coverFlowLayout.coverDensity = 0
        coverFlowLayout.minCoverScale = 0.90
        coverFlowLayout.minCoverOpacity = 1.0
    }
    
    func getProfileDetails(isLoading:Bool = true) {
        let param = ["userid": userDetails.getString(key: .userid)]
        ApiManager.shared.MakeGetAPI(name: PROFILE, params: param as [String : Any], progress: isLoading, vc: self, completionHandler: { (result, error) in
            self.isLoaded = true
            if result != nil {
                let json = JSON(result!)
                print(json)
                self.profileDetails = json
                let error = json.getString(key: .error)
                if error != YES {
                    saveJSON(j: json, key: USER_DETAILS_KEY)
                    userDetails = json
                    self.profileDetails = json
                    self.showDetails(isFirstTime: true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.getProfileDetails()
//                AlertView(title: "Failed", message: error!)
            }
        })        
    }
    
    func showDetails(isFirstTime:Bool = false) {
        self.arrayImageUser.removeAllObjects()
        let details = self.profileDetails.getDictionary(key: .myprofile)
        for image in details.getArray(key: .profile) {
            let dict: NSDictionary = ["imageURL" : image.string!, "isImage": "0", "isNew": "0"]
            self.arrayImageUser.add(dict)
        }
        
        self.collectionImages.reloadData()
        self.viewChangePassword.isHidden = details.getString(key: .type) == "guest" ? false : true
        if details.getString(key: .birth) != "" {
            self.birthSelected = details.getString(key: .birth)
            self.setDateOfdate(txtDate: details.getString(key: .birth))
        }
        
        self.lblName.text = details.getString(key: .name).capitalized
        let accountStatus = details.getString(key: .acountStatus).uppercased()
        if accountStatus == "N" {
            IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
            self.lblPremium.text = "Upgrade to Premium!"
            self.btnPremium.isUserInteractionEnabled = true
            self.showCancelSubscriptions(isShow: false)
            if isFirstTime {
//                SKPaymentQueue.default().restoreCompletedTransactions()
            }
        }
        else {
            let aedate = details.getString(key: .aedate)
            if aedate != "" {
                let time = self.convertFormate(str: aedate)
                
                self.lblPremium.text = "Premium member \nExpire on: \(time)"
                IM_AD_INSERTION_POSITION = 0
                self.btnPremium.isUserInteractionEnabled = false
                self.showCancelSubscriptions(isShow: true)
            }
            else {
                IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
                self.lblPremium.text = "Upgrade to Premium"
                self.btnPremium.isUserInteractionEnabled = true
                self.showCancelSubscriptions(isShow: false)
            }
        }
        
//        var textAccount = ""
//        let aedate = details.getString(key: .aedate)
//        if aedate != "" {
//            let date = self.convertDate(str: aedate)
//            if date > Date() {
//                let time = self.convertFormate(str: aedate)
//                textAccount = "Premium member \nExpire on: \(time)"
//                self.lblPremium.text = accountStatus == "N" ? "Get Premium account" : textAccount
//                IM_AD_INSERTION_POSITION = 0//accountStatus == "N" ? 10 : 0
//                self.btnPremium.isUserInteractionEnabled = false
//            }
//            else {
//                IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                self.lblPremium.text = "Get Premium account"
//                self.btnPremium.isUserInteractionEnabled = true
//            }
//        }
//        else {
//            IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//            self.lblPremium.text = "Get Premium account"
//            self.btnPremium.isUserInteractionEnabled = true
//        }

        self.txtvwAbout.text = details.getString(key: .about)
        if details.getString(key: .gender) == "male" {
            self.isMaleSelected = true
            selectedGender(selectedBtn: btnMale, unselectedBtn: btnFemale)
        }
        else{
            self.isMaleSelected = false
            selectedGender(selectedBtn: btnFemale, unselectedBtn: btnMale)
        }
        self.txtSocialActivity.text = details.getString(key: .hobbies)
        self.txtCountHobbies.text = "\(txtSocialActivity.text!.count)" + " / 50"

        self.txtJobTitle.text = details.getString(key: .job)
        self.txtCountOccupation.text = "\(txtJobTitle.text!.count)" + " / 50"

        self.switchSocialMedia.isOn = details.getInt(key: .insta) == 0 ? false : true
        
        self.arrayOfURLimagesForInsta.removeAll()
        self.arrayOfURLimagesForInstaJSON.removeAll()
    
        if details.getArray(key: .insta_links).count != 0 && self.switchSocialMedia.isOn {
            let links = details.getArray(key: .insta_links)
            for img in links {
                self.arrayOfURLimagesForInsta.append(img.string!)
                self.arrayOfURLimagesForInstaJSON.append(img)
            }
            self.instaCollectionView.reloadData {
                self.instaCollectionView.layoutIfNeeded()
                self.instaCollectionHeight.constant = self.instaCollectionView.contentSize.height
            }
        }
        //settings
        let location = details.getString(key: .location_name) == "" ? getCommonString(key: "My_Current_location_key") : details.getString(key: .location_name)
        btnLocation.setTitle(location, for: .normal)
        self.switchMen.isOn = details.getInt(key: .show_male) == 0 ? false : true
        self.switchWomen.isOn = details.getInt(key: .show_female) == 0 ? false : true
        
        self.rangeDistance.selectedMaxValue = CGFloat(details.getInt(key: .distance))
        self.lblDistanceValue.text = "\(Int(self.rangeDistance.selectedMaxValue)) mi."
        self.rangeAgeRange.selectedMinValue = CGFloat(details.getInt(key: .min_range))
        self.rangeAgeRange.selectedMaxValue = CGFloat(details.getInt(key: .max_range))
        lblRangeValue.text = "\(Int(self.rangeAgeRange.selectedMinValue)) - \(Int(self.rangeAgeRange.selectedMaxValue)) years"
        
        self.switchShareMyFeed.isOn = details.getInt(key: .myfeed) == 0 ? false : true
        self.showMore()
    }
    
    func setDateOfdate(txtDate: String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: txtDate)!
        
        dateFormatter.dateFormat = "MMMM"
        
        txtMonthName.text = dateFormatter.string(from: date)
        
        dateFormatter.dateFormat = "yyyy"
        txtYear.text = dateFormatter.string(from: date)
        
        dateFormatter.dateFormat = "dd"
        self.txtDate.text = dateFormatter.string(from: date)
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.birthSelected = dateFormatter.string(from: date)
    }
    
    func updateRangeSeekSliderUI(_ sliderType:SliderType,_ minValue:CGFloat,_ maxValue:CGFloat ) {
        if sliderType == SliderType.distance {
            self.lblDistanceValue.text = "\(Int(maxValue)) mi."
            self.rangeDistance.selectedMaxValue = maxValue
            self.rangeDistance.setNeedsLayout()
            /*DispatchQueue.main.async {
                
                self.rangeDistance.selectedMaxValue = maxValue
                print("self.rangeDistance.selectedMaxValue:=",self.rangeDistance.selectedMaxValue)
                
            }*/
            
        } else {
            self.rangeAgeRange.selectedMinValue = minValue
            self.rangeAgeRange.selectedMaxValue = maxValue
            self.rangeAgeRange.setNeedsLayout()
            lblRangeValue.text = "\(Int(minValue)) - \(Int(maxValue)) years"
        }
    }
    func redirectToRangeSliderUpdateVC(userData:JSON, sliderType:SliderType) {
        
        let vc = RangeSliderUpdateVC.init(nibName: "RangeSliderUpdateVC", bundle: nil)
        vc.setTheData(userData,sliderType)
//        vc.delegate = self
        vc.handlerChagesValue = { [weak self] (min, max) in
            if let obj = self {
                let key = sliderType == SliderType.distance ? "distance" : "max_range"
                let value = "\(max)"
                let keyMax = sliderType == SliderType.age ? "min_range" : ""
                let valueMax = sliderType == SliderType.age ? "\(min)" : ""
                obj.updateRangeSeekSliderUI(sliderType, min, max)
                let details = self!.profileDetails.getDictionary(key: .myprofile)

                if key == "distance" {
                    
                    let max = Int(details.getInt(key: .distance))
                    let DistanceValue = Int(self!.rangeDistance.selectedMaxValue)
                    if max != DistanceValue {
                        obj.makeUpdateProfileSettings(key: key, value: value, keyMax: keyMax, ValueMax: valueMax)
                    }
                }
                else {
                    
                    let selectedMinValue = Int(details.getInt(key: .min_range))
                    let selectedMaxValue = Int(details.getInt(key: .max_range))
                    
                    let min = Int(self!.rangeAgeRange.selectedMinValue)
                    let max = Int(self!.rangeAgeRange.selectedMaxValue)
                    if selectedMinValue != min {
                        obj.makeUpdateProfileSettings(key: key, value: value, keyMax: keyMax, ValueMax: valueMax)
                    }
                    else if selectedMaxValue != max {
                        obj.makeUpdateProfileSettings(key: key, value: value, keyMax: keyMax, ValueMax: valueMax)
                    }
                }
            }
//            self?.passValueToProfileVC(min, max, sliderType)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
//        self.present(vc, animated: true, completion: nil)
    }
    /*
    func deleteAccount() {
        let param = ["userid": userDetails.getString(key: .userid)]
        ApiManager.shared.MakePostAPI(name: DELETE_ACCOUNT, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                self.profileDetails = json
                let error = json.getString(key: .error)
                if error != YES {
                    deletJSON(key: USER_DETAILS_KEY)    
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Success", message: msg)
                    self.redirectToLoginScreen()
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                AlertView(title: "Failed", message: error!)
            }
        }
    }*/
    
    func validationUpdateProfile(){
        if self.txtDate.text! == "" || self.txtMonthName.text! == "" || self.txtYear.text! == "" {
            self.showToast(message: "Please select date of birth")
        }
        else if self.txtSocialActivity.text!.toTrim() == "" {
            self.showToast(message: "Please enter hobbies")
        }
        else if self.arrayImageUser.count == 0 {
            self.showToast(message: "Please select profile picture")
        }
        else if self.arrayImageUser.count < 3 {
            self.showToast(message: "Please select minimum 3 profile picture")
        }
        else if (!self.isDOBChanged && !self.isAboutChanged && !self.isHobbiesChanged && !self.isOccupationChanged && !self.isgenderChanged && !self.isInstagramChanged) && !self.isProfileChnaged {
            self.showToast(message: "Please enter data to update")
        }
        else {
            self.makeUpdateProfile()
        }
    }
    
    func makeUpdateProfile() {
        var images = [UIImage]()
        var imagesUrl = [String]()
        for image in self.arrayImageUser {
            let imageNew = image as! NSDictionary
            let isImage = imageNew.value(forKey: "isImage") as! String
            let isNew = imageNew.value(forKey: "isNew") as? String ?? ""
            if isImage == "1" {
                if isNew != "0" {
                    images.append(imageNew.value(forKey: "imageURL") as! UIImage)
                }
            }
            else {
                if isNew != "0" {
                    imagesUrl.append(imageNew.value(forKey: "imageURL") as! String)
                }
            }
        }

        let gender = self.isMaleSelected ? "male" : "female"
        let insta = self.switchSocialMedia.isOn ? "1" : "0"
        
        var param: [String : Any] = [String : Any]()
//        param = ["type":(userDetails.getDictionary(key: .myprofile)).getString(key: .type),"userid":userDetails.getString(key: .userid),"birth":self.birthSelected,"about":self.txtvwAbout.text!.toTrim(),"hobbies":self.txtSocialActivity.text!.toTrim(),"job":self.txtJobTitle.text!.toTrim(),"insta":insta, "gender":gender]
        
        if images.count == 0 && imagesUrl.count == 0 {
            param = ["userid":userDetails.getString(key: .userid),"birth":self.birthSelected,"about":self.txtvwAbout.text!.toTrim(),"hobbies":self.txtSocialActivity.text!.toTrim(),"job":self.txtJobTitle.text!.toTrim(),"insta":insta, "gender":gender]
            
            if !self.isDOBChanged {
                param.removeValue(forKey: "birth")
            }
            
            if !self.isAboutChanged {
                param.removeValue(forKey: "about")
            }
            
            if !self.isHobbiesChanged {
                param.removeValue(forKey: "hobbies")
            }
            
            if !self.isOccupationChanged {
                param.removeValue(forKey: "job")
            }
            
            if !self.isgenderChanged {
                param.removeValue(forKey: "gender")
            }
            
            if !self.isInstagramChanged {
                param.removeValue(forKey: "insta")
            }
        }
        else {
            param = ["userid":userDetails.getString(key: .userid)]
        }
        let instaLinks: [String] = self.switchSocialMedia.isOn ? self.arrayOfURLimagesForInsta : []
        print(self.arrayOfURLimagesForInsta)

        print(instaLinks)
//        if !self.isInstagramChanged {
//            instaLinks = []
//        }
        print(param)
        ApiManager.shared.MakePostWithImageAPI(name: UPDATE_PROFILE, params: param, images: images, imagesURL: imagesUrl, imagesInstaURL: instaLinks,vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    if self.isProfileChnaged {
                        self.isProfileChnaged = false
                    }
                    else {
                        self.isDOBChanged = false
                        self.isAboutChanged = false
                        self.isHobbiesChanged = false
                        self.isOccupationChanged = false
                        self.isgenderChanged = false
                        self.isInstagramChanged = false
                    }
                    saveJSON(j: json, key: USER_DETAILS_KEY)
                    userDetails = json
                    self.profileDetails = json
                    self.showDetails()
                    self.scrollView.scrollToTop()
//                    self.showToast(message: "Proifle Updated")
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeUpdateProfile()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func makeUpdateProfileSettings(key:String, value: String, keyMax: String = "", ValueMax: String = "") {
        var param: [String : Any] = [String : Any]()
        if keyMax == "" {
            param = ["userid":userDetails.getString(key: .userid),key: value]
        }
        else {
            param = ["userid":userDetails.getString(key: .userid), key: value, keyMax: ValueMax]
        }
        
        ApiManager.shared.MakePostAPI(name: UPDATE_PROFILE, params: param, progress: false, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
                    self.profileDetails = json
                    saveJSON(j: json, key: USER_DETAILS_KEY)
                    userDetails = json
//                    self.showToast(message: "Proifle Updated")
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeUpdateProfileSettings(key: key, value: value, keyMax: keyMax, ValueMax: ValueMax)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func makeUpdateProfileSettingsLocation(name:String, location: String) {
        let param: [String : Any] = ["userid":userDetails.getString(key: .userid),"location_name": name, "location": "\"" + location + "\""]
        
        ApiManager.shared.MakePostWithImageAPI(name: UPDATE_PROFILE, params: param, images: [], imagesURL: [], vc: self, completionHandler: { (response, error) in
            if response != nil {
                let json = JSON(response!)
                
                let error = json.getString(key: .error)
                if error != YES {
                    saveJSON(j: json, key: USER_DETAILS_KEY)
                    userDetails = json
//                    self.showToast(message: "Proifle Updated")
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeUpdateProfileSettingsLocation(name: name, location: location)
//                AlertView(title: "Failed", message: error!)
            }
        })
    }
}

//MARK: - IBAction
extension ProfileVc {
    
    
    //For Edit Profile
    func setupDefaultEditProfile() {
        selectedBtn(selectedBtn: btnEdit, unselectedBtn: btnSetting)
        vwMainProfile.isHidden = false
//        vwShowMore.isHidden = false
//        [vwJobTitle,vwFavoutireSocialActivity,vwSocialMedia,vwGender,vwSepratore,vwApplyBtn].forEach { (vw) in
//            vw?.isHidden = true
//        }
        vwMainSetting.isHidden = true
        self.showMore()
    }
    
    @IBAction func btnCancelSubscriptionsTapped(_ sender: UIButton) {
        guard let url = URL(string: "https://apps.apple.com/account/subscriptions") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnEditProfileTapped(_ sender: UIButton) {
        setupDefaultEditProfile()
    }
    
    @IBAction func btnSettingTapped(_ sender: UIButton) {
        self.openSetting()
    }
    
    func openSetting() {
        selectedBtn(selectedBtn: btnSetting, unselectedBtn: btnEdit)
        vwMainProfile.isHidden = true
        vwMainSetting.isHidden = false
    }
    
    @IBAction func btnShowMoreTapped(_ sender: UIButton) {
        self.showMore()
    }
    
    func showMore() {
        vwShowMore.isHidden = true
        [vwJobTitle,vwFavoutireSocialActivity,vwSocialMedia,vwGender,vwSepratore,vwApplyBtn].forEach { (vw) in
            vw?.isHidden = false
        }
    }
    
    @IBAction func btnSwitchSocialMediaTapped(_ sender: UISwitch) {
        if sender.isOn {
            self.arrayOfURLimagesForInsta.removeAll()
            self.arrayOfURLimagesForInstaJSON.removeAll()
            connectInsta()
        }
        else {
            self.arrayOfURLimagesForInsta.removeAll()
            self.arrayOfURLimagesForInstaJSON.removeAll()
            self.instaCollectionView.reloadData()
            self.instaCollectionHeight.constant = 0
        }
        
//        let details = self.profileDetails.getDictionary(key: .myprofile)
//        let insta = details.getInt(key: .insta) == 0 ? false : true
        self.isInstagramChanged = true//insta != sender.isOn
    }
    
    @IBAction func btnMaleTapped(_ sender: UIButton) {
        self.isMaleSelected = true
        selectedGender(selectedBtn: btnMale, unselectedBtn: btnFemale)
        let details = self.profileDetails.getDictionary(key: .myprofile)
        let gender = details.getString(key: .gender)
        self.isgenderChanged = gender.toTrim() != "male"
    }
    
    @IBAction func btnFemaleTapped(_ sender: UIButton) {
        self.isMaleSelected = false
        selectedGender(selectedBtn: btnFemale, unselectedBtn: btnMale)
        
        let details = self.profileDetails.getDictionary(key: .myprofile)
        let gender = details.getString(key: .gender)
        self.isgenderChanged = gender.toTrim() != "female".toTrim()
    }
    
    @IBAction func btnApplyTapped(_ sender: Any) {
        self.validationUpdateProfile()
    }
    
    func selectedBtn(selectedBtn:UIButton,unselectedBtn : UIButton) {
        selectedBtn.isSelected = true
        unselectedBtn.isSelected = false
    }
    
    func selectedGender(selectedBtn:UIButton,unselectedBtn : UIButton) {
        selectedBtn.layer.borderWidth = 1
        selectedBtn.layer.borderColor = UIColor.appThemePurpleColor.cgColor
        selectedBtn.backgroundColor = .white
        selectedBtn.setTitleColor(UIColor.appThemePurpleColor, for: .normal)
        selectedBtn.layer.cornerRadius = 4
        selectedBtn.layer.masksToBounds = true
        
        unselectedBtn.layer.borderWidth = 1
        unselectedBtn.layer.borderColor = UIColor.clear.cgColor
        unselectedBtn.backgroundColor = UIColor.appThemetxtBackgroundColor
        unselectedBtn.setTitleColor(UIColor.white, for: .normal)
        unselectedBtn.layer.cornerRadius = 4
        unselectedBtn.layer.masksToBounds = true
    }
    
    // setting IBAction method
    func setRangeForDistance(range:RangeSeekSlider) {
        range.minValue = 0
        range.maxValue = 50
        range.disableRange = true
        if let gestures = range.gestureRecognizers {
            for item in gestures {
                print("item:=",item)
            }
        }
        lblDistanceValue.text = "\(Int(range.maxValue)) mi."
        
        range.hideLabels = true
        range.lineHeight = 2.0
        range.tintColor = UIColor.appThemeLightGrayColor
        range.colorBetweenHandles = UIColor.appThemePurpleColor
        range.handleColor = UIColor.appThemePurpleColor
        range.maxLabelColor = UIColor.appThemePurpleColor
        range.minLabelColor = UIColor.appThemePurpleColor
    }
    
    func setRangeForAgeRange(range:RangeSeekSlider) {
        range.minValue = 18
        range.maxValue = 50
        range.minDistance = 1
        lblRangeValue.text = "\(Int(range.minValue)) - \(Int(range.maxValue)) years"
        
        range.hideLabels = true
        range.lineHeight = 2.0
        range.tintColor = UIColor.appThemeLightGrayColor
        range.colorBetweenHandles = UIColor.appThemePurpleColor
        range.handleColor = UIColor.appThemePurpleColor
        range.maxLabelColor = UIColor.appThemePurpleColor
        range.minLabelColor = UIColor.appThemePurpleColor
    }
    
    @IBAction func btnUpdateSliderAction(_ sender: UIButton) {
//        print("userDetails:=",userDetails)
//        print("profileDetails:=",profileDetails)
        if let userID = profileDetails["userid"].string,!userID.isEmpty  {
            redirectToRangeSliderUpdateVC(userData: self.profileDetails, sliderType: sender.tag == 1 ? SliderType.age : SliderType.distance)
        }
    }
    
    
    @objc func setValueOfLocation(notification : Notification) {
        if let obj = notification.object as? NSArray {
            let long = JSON(obj[1]).double
            let lat = JSON(obj[2]).double
            let location = jsonString(from: [long, lat])
            self.makeUpdateProfileSettingsLocation(name: obj.firstObject as! String, location: location!)
            btnLocation.setTitle(obj.firstObject as? String, for: .normal)
        }
    }
    
    @IBAction func btnSetLocationTapped(_ sender: UIButton) {
        self.isLocationViewed = true
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ChooseLocationVc") as!  ChooseLocationVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func switchMenTapped(_ sender: UISwitch) {
        let isMale = self.switchMen.isOn ? "1" : "0"
        self.makeUpdateProfileSettings(key: "show_male", value: isMale)
        self.showGender()
    }
    
    @IBAction func switchWomenTapped(_ sender: UISwitch) {
        let isWoman = self.switchWomen.isOn ? "1" : "0"
        self.makeUpdateProfileSettings(key: "show_female", value: isWoman)
        self.showGender(isMale: false)
    }
    
    func showGender(isMale:Bool = true) {
        if !self.switchMen.isOn && !self.switchWomen.isOn {
            if isMale {
                self.switchWomen.isOn = true
                let isWoman = self.switchWomen.isOn ? "1" : "0"
                self.makeUpdateProfileSettings(key: "show_female", value: isWoman)
            }
            else {
                self.switchMen.isOn = true
                let isMale = self.switchMen.isOn ? "1" : "0"
                self.makeUpdateProfileSettings(key: "show_male", value: isMale)
            }
        }
    }
    
    @IBAction func switchShareMyFeedTapped(_ sender: UISwitch) {
        let isShare = self.switchShareMyFeed.isOn ? "1" : "0"
        self.makeUpdateProfileSettings(key: "myfeed", value: isShare)
    }
    
    @IBAction func btnChangePasswordTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVc") as!  ResetPasswordVc
        obj.selectedController = .fromChangePassword
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnDeleteAccountTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "DeleteAccountVC") as!  DeleteAccountVC
        self.navigationController?.pushViewController(obj, animated: true)
        
        /*
        let alertController = UIAlertController(title: getCommonString(key: "Berry_key"), message: getCommonString(key: "Delete_account_msg_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.deleteAccount()
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)*/
    }
    
    @IBAction func btnLicenseTapped(_ sender: UIButton) {
        let details = self.profileDetails["settings"].dictionary
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WebvwVc") as!  WebvwVc
        obj.selectedController = .forLicenses
        obj.url = details?["license"]?.string ?? ""
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnPrivacyPolicyTapped(_ sender: UIButton) {
//        let details = self.profileDetails["settings"].dictionary
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WebvwVc") as!  WebvwVc
        obj.selectedController = .forPrivacyPolicy
        obj.url = "http://www.berrydates.com/privacy-policy/" //"http://www.berrydates.com/privacy-policy-2/" //details?["privacy"]?.string ?? ""
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    @IBAction func btnTermsofServiceTapped(_ sender: UIButton) {
//        let details = self.profileDetails["settings"].dictionary
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WebvwVc") as!  WebvwVc
        obj.selectedController = .forTermsofService
        obj.url = "http://www.berrydates.com/terms-of-use/" //details?["terms"]?.string ?? ""
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: getCommonString(key: "Berry_key"), message: getCommonString(key: "Logout_msg_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            if #available(iOS 10.0, *) {
                let center = UNUserNotificationCenter.current()
                center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
                center.removeAllDeliveredNotifications() // To remove all delivered notifications
            } else {
                UIApplication.shared.cancelAllLocalNotifications()
            }
            
            DispatchQueue.main.async {                
                if let cookies = HTTPCookieStorage.shared.cookies {
                    for cookie in cookies {
                        NSLog("\(cookie)")
                    }
                }
                let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                for cookie in cookieJar.cookies! as [HTTPCookie]{
                    NSLog("cookie.domain = %@", cookie.domain)
                    cookieJar.deleteCookie(cookie)
                    
                }
                
                URLCache.shared.removeAllCachedResponses()
                URLCache.shared.diskCapacity = 0
                URLCache.shared.memoryCapacity = 0
                
                guard ReachabilityTest.isConnectedToNetwork() else {
                    saveFCMLogoutToken(token: getFCMToken())
                    saveLogoutUserId(UserId: userDetails.getString(key: .userid))
                    self.redirectToLoginScreen()
                    return
                }
                self.serviceCallToLogout()
            }
        }
        
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func btnAddPhotoInCollectionView(sender : UIButton) {
        if arrayImageUser.count >= 6 {
            self.showToast(message: "You reach maximum limit of image upload")
        }
        else {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "PhotoPickerVc") as! PhotoPickerVc
            obj.delegateImage = self
            obj.intRemainingPhotoCount = 6 - arrayImageUser.count
            obj.selectedController = .addPhotoProfile
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    @IBAction func btnPremiumClicked(_ sender: Any) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "InAppPlansVC") as! InAppPlansVC
        obj.delegate = self
        if self.productsArray.count != 0 {
             obj.price = (self.productsArray.first!!).localizedPrice//self.priceStringForProduct(item: self.productsArray.first as! SKProduct) ?? ""
        }
        let nav = UINavigationController(rootViewController: obj)
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: false, completion: nil)
    }
    
    func connectInsta() {
        if (Defaults.value(forKey: "InstagramID") as? String) != nil
        {
            let authToken = Defaults.value(forKey: "authoToken") as! String
            getAllImageFromInstagram(authToken: authToken)
        }
        else
        {
            self.loginWithInstagram()
        }
    }
}

extension ProfileVc: InAppPlansDelegate {
    func InAppPlansFinish() {
        self.showActions()
    }
}

/*// MARK:- SliderDelegate
extension ProfileVc:SliderDelegate {
    func updateSliderValues(_ sliderType: SliderType, _ minValue: CGFloat, _ maxValue: CGFloat) {
        updateRangeSeekSliderUI(sliderType, minValue, maxValue)
    }
}*/

extension ProfileVc: InstagramLoginViewControllerDelegate{
    func loginWithInstagram() {
//        if let cookies = HTTPCookieStorage.shared.cookies {
//            for cookie in cookies {
//                NSLog("\(cookie)")
//            }
//        }
//        let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
//        for cookie in cookieJar.cookies! as [HTTPCookie]{
//            NSLog("cookie.domain = %@", cookie.domain)
//            cookieJar.deleteCookie(cookie)
//
//        }
//        
//        URLCache.shared.removeAllCachedResponses()
//        URLCache.shared.diskCapacity = 0
//        URLCache.shared.memoryCapacity = 0
        
        let vc = InstagramLoginViewController(clientId: GlobalVariables.instagramClientKey, redirectUri: GlobalVariables.instagramRedirectURL)
        // Login permissions (https://www.instagram.com/developer/authorization/)
        vc.scopes = [.basic] // basic by default
        // ViewController title, website title by default
        vc.title = "Instagram" // By default, the web title is displayed
        vc.delegate = self
        // Progress view tint color
        vc.progressViewTintColor = UIColor.green // #E1306C by default
        
        let nav = UINavigationController(rootViewController: vc)
        let button1 = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(instaCloseClicked))
        vc.navigationItem.rightBarButtonItem  = button1
        nav.modalPresentationStyle = .overFullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func instaCloseClicked() {
        self.switchSocialMedia.isOn = false
        dismiss(animated: true, completion: nil)
    }
    
    func instagramLoginDidFinish(accessToken: String?, error: InstagramError?) {
        guard let accessToken = accessToken else {
            print("Failed login: " + error!.localizedDescription)
            return
        }
        handleAuth(authToken: accessToken)
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleAuth(authToken: String)  {
        jsondata(authToken : authToken)
    }
    
    func jsondata(authToken : String){
        let url = "https://api.instagram.com/v1/users/self/?access_token=\(authToken)"
        let request = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) { (response, data, error) in
            
            if error == nil {
                do {
                    let jasonobject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    if let data = jasonobject.object(forKey: "data") as? NSDictionary {
                        var dictLinkedData = JSON(data)
                        
                        let instaID = dictLinkedData["id"].stringValue
                        Defaults.setValue(instaID, forKey: "InstagramID")
                        
                        let authoToken = authToken
                        Defaults.setValue(authoToken, forKey: "authoToken")
                        
                        Defaults.synchronize()
                        self.getAllImageFromInstagram(authToken: authoToken)
                    }
                }catch{
                }
            }
        }
    }
    
    func getAllImageFromInstagram(authToken : String) {
        self.arrayOfURLimagesForInsta.removeAll()
        self.arrayOfURLimagesForInstaJSON.removeAll()
        let url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=\(authToken)"
        let request = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        DispatchQueue.main.async {
            self.switchSocialMedia.isUserInteractionEnabled = false
        }
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) { (response, data, error) in
           if error != nil {
                DispatchQueue.main.async {
                    self.switchSocialMedia.isUserInteractionEnabled = true
                }
            }
            if error == nil {
                do {
                    self.arrayOfURLimagesForInsta.removeAll()
                    self.arrayOfURLimagesForInstaJSON.removeAll()
                    let jasonobject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    
                    if let data = jasonobject.object(forKey: "data") as? NSArray {
                        var dictLinkedData = JSON(data)
                        let instaID = dictLinkedData["id"].stringValue
                        Defaults.setValue(instaID, forKey: "InstagramID")
                        Defaults.synchronize()
                        
//                        for i in 0..<dictLinkedData.count
//                        {
//                            let dict = dictLinkedData[i]["images"].dictionaryValue
//                            if let dictResolution = dict["standard_resolution"]?.dictionaryValue
//                            {
//                                let imgURL = dictResolution["url"]?.url?.absoluteString
//                                self.arrayOfURLimagesForInsta.append(imgURL!)
//                            }
//                        }
                        
                        for i in 0..<dictLinkedData.count {
                            let json = JSON(dictLinkedData[i])
                            let carousel_media = json.getArray(key: .carousel_media)
                            if carousel_media.isEmpty {
                                let dict = dictLinkedData[i]["images"].dictionaryValue
                                if let dictResolution = dict["standard_resolution"]?.dictionaryValue
                                {
                                    if self.arrayOfURLimagesForInsta.count >= (self.showInstaLines * self.showInstaColumn) {
                                        
                                    }
                                    else {
                                        let imgURL = dictResolution["url"]?.url?.absoluteString
                                        self.arrayOfURLimagesForInsta.append(imgURL!)
                                        self.arrayOfURLimagesForInstaJSON.append(dictResolution["url"]!)
                                    }
                                }
                            }
                            else {
                                for j in 0..<carousel_media.count {
                                    let dict = carousel_media[j]["images"].dictionaryValue
                                    if let dictResolution = dict["standard_resolution"]?.dictionaryValue
                                    {
                                        if self.arrayOfURLimagesForInsta.count >= (self.showInstaLines * self.showInstaColumn) {
                                            
                                        }
                                        else {
                                            let imgURL = dictResolution["url"]?.url?.absoluteString
                                            self.arrayOfURLimagesForInsta.append(imgURL!)
                                            self.arrayOfURLimagesForInstaJSON.append(dictResolution["url"]!)
                                        }
                                    }
                                }
                            }
                        }

                        let json = JSON(jasonobject)
                        if let dict: JSON = json.getDictionary(key: .pagination) {
                            if !dict.isEmpty {
                                if self.arrayOfURLimagesForInsta.count >= (self.showInstaLines * self.showInstaColumn) {
                                    DispatchQueue.main.async {
                                        self.switchSocialMedia.isUserInteractionEnabled = true
                                        self.instaCollectionView.reloadData {
                                            self.instaCollectionView.layoutIfNeeded()
                                            self.instaCollectionHeight.constant = self.instaCollectionView.contentSize.height
                                        }
                                    }
                                }
                                else {
                                    let next_url = dict.getString(key: .next_url)
                                    self.getNextImageFromInstagram(url: next_url)
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.switchSocialMedia.isUserInteractionEnabled = true
                                    self.instaCollectionView.reloadData {
                                        self.instaCollectionView.layoutIfNeeded()
                                        self.instaCollectionHeight.constant = self.instaCollectionView.contentSize.height
                                    }
                                }
                            }
                        }
                       
                    }
                }catch{
                    DispatchQueue.main.async {
                        self.switchSocialMedia.isUserInteractionEnabled = true
                    }
                    print("error")
                }
            }
        }
    }
    
    func getNextImageFromInstagram(url : String) {
        let request = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        DispatchQueue.main.async {
            self.switchSocialMedia.isUserInteractionEnabled = false
        }
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) { (response, data, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.switchSocialMedia.isUserInteractionEnabled = true
                }
            }
            if error == nil {
                do {
                    let jasonobject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    
                    if let data = jasonobject.object(forKey: "data") as? NSArray {
                        var dictLinkedData = JSON(data)
                        let instaID = dictLinkedData["id"].stringValue
                        Defaults.setValue(instaID, forKey: "InstagramID")
                        Defaults.synchronize()
                        
//                        for i in 0..<dictLinkedData.count
//                        {
//                            let dict = dictLinkedData[i]["images"].dictionaryValue
//                            if let dictResolution = dict["standard_resolution"]?.dictionaryValue
//                            {
//                                let imgURL = dictResolution["url"]?.url?.absoluteString
//                                self.arrayOfURLimagesForInsta.append(imgURL!)
//                            }
//                        }
                        
                        for i in 0..<dictLinkedData.count {
                            let json = JSON(dictLinkedData[i])
                            let carousel_media = json.getArray(key: .carousel_media)
                            if carousel_media.isEmpty {
                                let dict = dictLinkedData[i]["images"].dictionaryValue
                                if let dictResolution = dict["standard_resolution"]?.dictionaryValue
                                {
                                    if self.arrayOfURLimagesForInsta.count >= (self.showInstaLines * self.showInstaColumn) {
                                        
                                    }
                                    else {
                                        let imgURL = dictResolution["url"]?.url?.absoluteString
                                        self.arrayOfURLimagesForInsta.append(imgURL!)
                                        
                                        self.arrayOfURLimagesForInstaJSON.append(dictResolution["url"]!)
                                    }
                                }
                            }
                            else {
                                for j in 0..<carousel_media.count {
                                    let dict = carousel_media[j]["images"].dictionaryValue
                                    if let dictResolution = dict["standard_resolution"]?.dictionaryValue
                                    {
                                        if self.arrayOfURLimagesForInsta.count >= (self.showInstaLines * self.showInstaColumn) {
                                            
                                        }
                                        else {
                                            let imgURL = dictResolution["url"]?.url?.absoluteString
                                            self.arrayOfURLimagesForInsta.append(imgURL!)
                                            self.arrayOfURLimagesForInstaJSON.append(dictResolution["url"]!)
                                        }
                                    }
                                }
                            }
                        }

                        let json = JSON(jasonobject)
                        if let dict: JSON = json.getDictionary(key: .pagination) {
                            if !dict.isEmpty {
                                if self.arrayOfURLimagesForInsta.count >= (self.showInstaLines * self.showInstaColumn) {
                                    DispatchQueue.main.async {
                                        self.switchSocialMedia.isUserInteractionEnabled = true
                                        self.instaCollectionView.reloadData {
                                            self.instaCollectionView.layoutIfNeeded()
                                            self.instaCollectionHeight.constant = self.instaCollectionView.contentSize.height
                                        }
                                    }
                                }
                                else {
                                    let next_url = dict.getString(key: .next_url)
                                    self.getNextImageFromInstagram(url: next_url)
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.switchSocialMedia.isUserInteractionEnabled = true
                                    self.instaCollectionView.reloadData {
                                        self.instaCollectionView.layoutIfNeeded()
                                        self.instaCollectionHeight.constant = self.instaCollectionView.contentSize.height
                                    }
                                }
                            }
                        }
                        
                    }
                }catch{
                    print("error")
                    DispatchQueue.main.async {
                        self.switchSocialMedia.isUserInteractionEnabled = true
                    }
                }
            }
        }
    }
}

extension ProfileVc {
    func redirectToLoginScreen() {
        deletJSON(key: USER_DETAILS_KEY)
        Defaults.removeObject(forKey: "facebookID")
        Defaults.removeObject(forKey: "authoToken")
        Defaults.removeObject(forKey: "InstagramID")
        Defaults.synchronize()
        AppDelegate.shared.window?.rootViewController = nil
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RootNav") as! UINavigationController
        AppDelegate.shared.window?.rootViewController = vc
        SocketIOHandler.shared.disconnectSocket()
    }
    
    func serviceCallToLogout() {
        let param = ["userid": userDetails.getString(key: .userid), "det":DEVICE_TYPE,"token":getFCMToken()]
        print("param:=",param)
        ApiManager.shared.MakePostAPI(name: LOGOUT, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                //                self.profileDetails = json
                let error = json.getString(key: .error)
                if error != YES {
                    deletJSON(key: USER_DETAILS_KEY)
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Success", message: msg)
                    self.redirectToLoginScreen()
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.serviceCallToLogout()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
}


//MARK: - TextField Delegate

extension ProfileVc : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDate || textField == txtMonthName || textField == txtYear {
          
            /*let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 1
            obj.isSetMaximumDate = true
            
            let YearAgo18 = Calendar.current.date(byAdding: .year, value: -18, to: Date())

            obj.setMaximumDate = YearAgo18
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            */
            return false
            
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == " " {
                return false
            }
            
            if textField == self.txtJobTitle {
                if updatedText.count > 50 {
                    return false
                }
                self.txtCountOccupation.text = "\(updatedText.count)" + " / 50"
                
                let details = self.profileDetails.getDictionary(key: .myprofile)
                let job = details.getString(key: .job)
                self.isOccupationChanged = job.toTrim() != updatedText
            }
            
            if textField == self.txtSocialActivity {
                if updatedText.count > 50 {
                    return false
                }
                self.txtCountHobbies.text = "\(updatedText.count)" + " / 50"
                
                let details = self.profileDetails.getDictionary(key: .myprofile)
                let about = details.getString(key: .hobbies)
                self.isHobbiesChanged = about.toTrim() != updatedText
            }
        }
        return true
    }
}


extension ProfileVc : DateTimePickerDelegate {
    func setDateandTime(dateValue: Date, type: Int) {
        self.view.endEditing(true)
        if type == 1 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM"
            
            txtMonthName.text = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "yyyy"
            txtYear.text = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "dd"
            txtDate.text = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            self.birthSelected = dateFormatter.string(from: dateValue)
            
            let details = self.profileDetails.getDictionary(key: .myprofile)
            let dob = details.getString(key: .birth)
            self.isDOBChanged = dob != self.birthSelected
        }
    }
}

extension ProfileVc : UITextViewDelegate {
    private func textViewShouldReturn(_ textView: UITextView!) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
//    private func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let updatedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        if updatedText == " " {
//            return false
//        }
//
//        if textView == self.txtvwAbout {
//            if updatedText.count > 40 {
//                return false
//            }
//        }
//        return true
//    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let updatedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        if text == "\n" {   // Recognizes enter key in keyboard
            textView.resignFirstResponder()
            return false
        }
        
        if updatedText == " " {
            return false
        }
        
        let details = self.profileDetails.getDictionary(key: .myprofile)
        let about = details.getString(key: .about)
        self.isAboutChanged = about.toTrim() != updatedText
        return true
    }
//    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool {
//        if text == "\n" {   // Recognizes enter key in keyboard
//            textView.resignFirstResponder()
//            return false
//        }
//
//        if let updatedText = (textView.text as NSString).replacingCharacters(in: shouldChangeTextInRange, with: text) {
//            if updatedText == " " {
//                return false
//            }
//
//            if textView == self.txtvwAbout {
//                if updatedText.count > 40 {
//                    return false
//                }
//            }
//        }
//
//        return true
//    }
}

//MARK: - CollectionView delegate and DataSource

extension ProfileVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.instaCollectionView {
            return self.arrayOfURLimagesForInsta.count > (self.showInstaLines * self.showInstaColumn) ? (self.showInstaLines * self.showInstaColumn) : self.arrayOfURLimagesForInsta.count
        }
        return arrayImageUser.count == 6 ? arrayImageUser.count : arrayImageUser.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.instaCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YMSPhotoCell", for: indexPath) as! YMSPhotoCell
            let image = arrayOfURLimagesForInsta[indexPath.row]
            
            cell.imgSelected.isHidden = true
            cell.selectionVeil.isHidden = true
            cell.selectionVeil.alpha = 0
            cell.imageView.sd_setShowActivityIndicatorView(true)
            cell.imageView.sd_setIndicatorStyle(.gray)
            cell.imageView.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionUserImageCell", for: indexPath) as! collectionUserImageCell
            
            cell.btnAddPhoto.tag = indexPath.row
            
            if arrayImageUser.count == indexPath.row {
                
                cell.btnAddPhoto.isHidden = false
                
                cell.img.image = UIImage(named: "ic_add_swipe_card")
                cell.img.contentMode = .center
                cell.img.backgroundColor = UIColor.white
            }
            else  {
                let imageNew = arrayImageUser[indexPath.row] as! NSDictionary
                
                let isImage = imageNew.value(forKey: "isImage") as! String
                if isImage == "1" {
                    cell.img.image = imageNew.value(forKey: "imageURL") as? UIImage
                }
                else {
                    let url = imageNew.value(forKey: "imageURL") as! String
                    cell.img.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "user_placeholder_big"), completed: nil)
                }
                
                cell.btnAddPhoto.isHidden = true
                cell.img.contentMode = .scaleAspectFill
                cell.img.backgroundColor = UIColor.clear
            }
            
            cell.btnAddPhoto.addTarget(self, action: #selector(btnAddPhotoInCollectionView), for: .touchUpInside)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.instaCollectionView {
            self.isPhotoViewed = true
            if self.arrayOfURLimagesForInstaJSON.count != 0 {
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ShowMultiImagesVc") as! ShowMultiImagesVc
                obj.selectedController = .forMatchCardShowPhotoHeader
                obj.arrSocialMediaImagesMutable = self.arrayOfURLimagesForInstaJSON
                obj.currentImageIndex = indexPath.row
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
        else {
            if arrayImageUser.count == indexPath.row { // For cell tapped
                
            }
            else {
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CropMultiImagesVc") as! CropMultiImagesVc
                obj.delegateCropImage = self
                obj.arrSocialMediaImagesMutable = self.arrayImageUser
                obj.currentImageIndex = indexPath.row
                obj.selectedController = .forProfileShowPhoto
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.instaCollectionView {
            let width = (instaCollectionView.bounds.width - (15 + 14)) / 4
            return CGSize(width: width, height: width)
        }
        return setupCellSize()
    }
    
    func getCurrentRow() {
//        let width = collectionImages.collectionViewLayout.collectionViewContentSize.width / CGFloat(collectionImages.numberOfItems(inSection: 0))
//        let currentRow = collectionImages.contentOffset.x / width
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?)  {
        if motion == .motionShake {
            getCurrentRow()
        }
    }    
}

//MARK: - Change in YangMangShan
extension ProfileVc {
    func setupCellSize() -> CGSize {
        let layout = self.instaCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        
        // Fetch shorter length
        let arrangementLength: CGFloat = min(view.frame.width - 50, view.frame.height)
        
        let minimumInteritemSpacing: CGFloat? = layout?.minimumInteritemSpacing
        let sectionInset: UIEdgeInsets? = layout?.sectionInset
        
        let totalInteritemSpacing: CGFloat = CGFloat(max((Int(3) - 1), 0)) * (minimumInteritemSpacing ?? 0.0)
        let totalHorizontalSpacing: CGFloat = totalInteritemSpacing + (sectionInset?.left ?? 0.0) + (sectionInset?.right ?? 0.0)
        
        // Caculate size for portrait mode
        let size = CGFloat(floor((arrangementLength - totalHorizontalSpacing) / 3))
        cellPortraitSize = CGSize(width: size, height: size)
        
        return cellPortraitSize
    }
}


//MARK: - RangeSeeker Delegate

extension ProfileVc: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === rangeDistance {
            lblDistanceValue.text = "\(Int(maxValue)) mi."
        } else if slider === rangeAgeRange {
            print(minValue)
            print(maxValue)

            lblRangeValue.text = "\(Int(minValue)) - \(Int(maxValue)) years"
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {

    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        if slider == self.rangeDistance {
            self.makeUpdateProfileSettings(key: "distance", value: "\(slider.selectedMaxValue)")
        }
        else {
            self.makeUpdateProfileSettings(key: "max_range", value: "\(slider.selectedMaxValue)", keyMax: "min_range", ValueMax: "\(slider.selectedMinValue)")
        }
    }
}

extension ProfileVc : selectedImageDelegate {
    func setImagesWithUrlInCollection(arrayOfImages: NSMutableArray) {
        for image in arrayOfImages {
            arrayImageUser.add(image)
            self.isProfileChnaged = true
        }
        collectionImages.reloadData()
        self.btnApplyTapped(self)
    }
    
    func setImagesInCollection(arrayOfImages: [UIImage]) {
        collectionImages.reloadData()
    }
}


extension ProfileVc : CropImagesDelegate {
    func setCropImagesWithUrlInCollection(arrayOfImages: NSMutableArray) {
        self.arrayImageUser = arrayOfImages
        self.collectionImages.reloadData()
    }
    
    func setCropImagesInCollection(arrayOfImages: [UIImage]) {
        collectionImages.reloadData()
    }
}

extension ProfileVc: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func requestProductInfo() {
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(object: "BerryProductRenew");
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    func showActions() {
        if self.productsArray.count != 0 {
            showProgress(vc: self)
            let payment = SKPayment(product: self.productsArray.first as! SKProduct)
            SKPaymentQueue.default().add(payment)
        }
    }
    
    func priceStringForProduct(item: SKProduct) -> String? {
        let price = item.price
        if price == NSDecimalNumber(decimal: 0.00) {
            return "GET" //or whatever you like really... maybe 'Free'
        } else {
            let numberFormatter = NumberFormatter()
            let locale = item.priceLocale
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = locale
            return numberFormatter.string(from: price)
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products.count)
        if response.products.count != 0 {
            self.productsArray.removeAll()

            for product in response.products {
                productsArray.append(product)
            }
//            tblProducts.reloadData()
        }
        else {
            print("There are no products.")
        }
        
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                self.receiptValidation(transaction: transaction)
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case SKPaymentTransactionState.restored:
                print("Transaction Restored");
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
//                print(transaction.transactionIdentifier)
//                SKPaymentQueue.default().finishTransaction(transaction)
//                SKPaymentQueue.default().finishTransaction(transaction)
//                self.receiptValidation(transaction: transaction, isRestore: true)

            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
                SKPaymentQueue.default().finishTransaction(transaction)

            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        queue.transactions.forEach({ (payment) in
            print(payment.transactionIdentifier)
        })
    }
    
    
    //If an error occurs, the code will go to this function
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        print(error)
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        print(request)
        print(error)
    }
    
    func receiptValidation(transaction : SKPaymentTransaction?, isRestore:Bool = false) {
        let SUBSCRIPTION_SECRET = "5e16f666b50a493194cb3c4d681140fb"
          let recieptURL = Bundle.main.appStoreReceiptURL!
        if let reciept = NSData(contentsOf: recieptURL){
            let receiptData = reciept.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let requestContents = ["receipt-data" : receiptData,"password":SUBSCRIPTION_SECRET]
//            print("requestContents-->",requestContents)
            
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestContents, options: JSONSerialization.WritingOptions(rawValue : 0))
                
                //let storeURL = NSURL(string: Constants.URL.VALIDATE_APPSTORESANDBOXURL)
                let storeURL = NSURL(string: "https://sandbox.itunes.apple.com/verifyReceipt")
                let request = NSMutableURLRequest(url: storeURL! as URL)
                request.httpMethod = "Post"
                request.httpBody = requestData
                
                let session = URLSession.shared
                let task = session.dataTask(with: request as URLRequest, completionHandler: { (responseData: Data?, response : URLResponse?, error : Error?) -> Void in
                    if let data = responseData , error == nil {
                        do {
                            DispatchQueue.main.async {
                                self.stopAnimating()
                            }
                            let json = try JSONSerialization.jsonObject(with: data) as? NSDictionary
                            print(json)
                            if isRestore {
                                return
                            }
                            //print("success. here is the json representation of the app receipt: \(json)")
                            // if you are using your server this will be a json representation of whatever your server provided
                            if(json!.object(forKey: "status") as! NSNumber) == 0 {
                                
                                let receipt_dict = json!["receipt"] as! NSDictionary
                                if let purchases = receipt_dict["in_app"] as? NSArray {
                                    // print("purchases-->",purchases)
                                    //self.validatePurchaseArray(purchases: purchases)
                                    let lastReceipt = purchases.lastObject as! NSDictionary
                                    
                                    // Get last receipt
//                                    print("LAST RECEIPT INFORMATION \n",lastReceipt)                                    
                                    let transactionId = lastReceipt.getString(key: .transaction_id)
                                    self.premiumAccount(transactionId: transactionId, receipt: receiptData)
                                    
                                    if transaction != nil {
//                                        SKPaymentQueue.default().finishTransaction(transaction!)
                                    }
                                }
                            }
                                
                            else {
                                print(json!.object(forKey: "status") as! NSNumber)
                            }
                            
                        } catch let error as NSError {
                            print("json serialization failed with error: \(error)")
                        }
                    } else {
                        print("the upload task returned an error: \(error)")
                    }
                    
                })
                task.resume()
                
            } catch {
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
                print(error)
            }
        }
        else {
            
            print("No Reciept")
        }
        
    }
    
    func receiptValidation1(transaction : SKPaymentTransaction?) {
        let SUBSCRIPTION_SECRET = "5e16f666b50a493194cb3c4d681140fb"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            
            print(base64encodedReceipt!)
            
            
            let requestDictionary = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]
            
            guard JSONSerialization.isValidJSONObject(requestDictionary) else {  print("requestDictionary is not valid JSON");  return }
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
                let validationURLString = IN_APP_PURCHASE.SANDBOX.rawValue  // this works but as noted above it's best to use your own trusted server
                guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
                let session = URLSession(configuration: URLSessionConfiguration.default)
                var request = URLRequest(url: validationURL)
                request.httpMethod = "POST"
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
                let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
                    if let data = data , error == nil {
                        do {
                            let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
//                            print("success. here is the json representation of the app receipt: \(appReceiptJSON)")
                            print(JSON(appReceiptJSON))
                            print(JSON(appReceiptJSON).rawString())
                            // if you are using your server this will be a json representation of whatever your server provided
//                            self.premiumAccount(orderData: JSON(appReceiptJSON).rawString()!)
                            
                            if transaction != nil {
                                SKPaymentQueue.default().finishTransaction(transaction!)
                            }
                        } catch let error as NSError {
                            print("json serialization failed with error: \(error)")
                        }
                    } else {
                        print("the upload task returned an error: \(error)")
                    }
                }
                task.resume()
            } catch let error as NSError {
                print("json serialization failed with error: \(error)")
            }
        }
    }
    
    func premiumAccount(transactionId:String, receipt:String) {
        let param = ["userid": userDetails.getString(key: .userid), "orderData":["transaction_id": transactionId, "receipt": receipt]] as [String : Any]
        print(param)
        ApiManager.shared.MakePostAPI(name: PREMIUM_ACCOUNT, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                print(json)
//                let data = json.getDictionary(key: .data)
//                var jsonOld = loadJSON(key: USER_DETAILS_KEY)
//                jsonOld.setDictionaryValue(key: .myprofile, value: data.dictionaryValue)
//
//                self.profileDetails = jsonOld
                let error = json.getString(key: .error)
                if error != YES {
//                    saveJSON(j: jsonOld, key: USER_DETAILS_KEY)
//                    userDetails = jsonOld
//                    self.profileDetails = jsonOld
//                    self.showDetails()
                    self.getProfileDetails(isLoading: false)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                AlertView(title: "Failed", message: error ?? "")
//                self.premiumAccount(transactionId: transactionId, receipt: receipt)
            }
        }
    }
}


extension SKProduct {
//    fileprivate static var formatter: NumberFormatter {
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//        return formatter
//    }
//
//    var localizedPrice: String {
//        if self.price == 0.00 {
//            return "Get"
//        } else {
//            let formatter = SKProduct.formatter
//            formatter.locale = self.priceLocale
//
//            guard let formattedPrice = formatter.string(from: self.price) else {
//                return "Unknown Price"
//            }
//
//            return formattedPrice
//        }
//    }
    
    var localizedPrice: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = priceLocale
        return formatter.string(from: price)!
    }
}
