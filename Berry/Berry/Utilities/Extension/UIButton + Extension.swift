//
//  UIButton + Extension.swift
//  Liber
//
//  Created by YASH on 25/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension CustomButton {
    
    func setupShadowThemeButtonUI() {
        self.setTitleColor(UIColor.appThemePurpleColor, for: .normal)
        self.backgroundColor = UIColor.white
        self.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        self.layer.cornerRadius = self.layer.bounds.height/2
        self.shadowOffset = CGSize(width: 1, height: 1)
        self.shadowOpacity = 0.2
        self.shadowColor = UIColor.black
    }
}
