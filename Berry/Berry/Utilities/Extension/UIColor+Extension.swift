//
//  UIColor+Extension.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var appThemePurpleColor: UIColor { return UIColor.init(displayP3Red: 58/255, green: 26/255, blue: 90/255, alpha: 1.0) }
    
    static var appThemeLightPinkColor: UIColor { return UIColor.init(displayP3Red: 246/255, green: 244/255, blue: 249/255, alpha: 1.0) }
    
    static var appThemeDarkGrayColor: UIColor { return UIColor.init(displayP3Red: 99/255, green: 99/255, blue: 99/255, alpha: 1.0) }
    
    static var appThemeLightGrayColor: UIColor { return UIColor.init(displayP3Red: 142/255, green: 142/255, blue: 142/255, alpha: 1.0) }
    
    static var appThemetxtBackgroundColor: UIColor { return UIColor.init(displayP3Red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0) }
    
    static var appThemeRedColor: UIColor { return UIColor.init(displayP3Red: 232/255, green: 0/255, blue: 48/255, alpha: 1.0) }
    
    static var appThemeGreenColor: UIColor { return UIColor.init(displayP3Red: 15/255, green: 187/255, blue: 55/255, alpha: 1.0) }
    
    static var appThemebtnBackgroundColor: UIColor { return UIColor(red: 51.0/255.0, green: 72.0/255.0, blue: 173.0/255.0, alpha: 1.0) }
    
    static var appThemeLightShadBackgroundColor: UIColor { return UIColor(red: 241.0/255.0, green: 242.0/255.0, blue: 243.0/255.0, alpha: 1.0) }
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
   

