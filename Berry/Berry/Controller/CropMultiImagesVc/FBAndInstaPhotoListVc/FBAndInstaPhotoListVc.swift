//
//  FBAndInstaPhotoListVc.swift
//  Berry
//
//  Created by YASH on 21/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
//import YangMingShan

protocol redirectAfterSocialMediaPhotoSelection {
    func navigationToController(arrayOfSelectedImageURL : [URL])
}

class FBAndInstaPhotoListVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var collectionPhotos: UICollectionView!
    
    //MARK: - Variables
    var numberOfPhotoToSelect = 1    // maximum 6 photos selection
    var intSelectedPhotoCount = 0
    var cellPortraitSize = CGSize()
    var delegateSocialMediaImage : redirectAfterSocialMediaPhotoSelection?
    var arrayURL = [URL]()     // All images url of social media(append value from PhotoPickerVc)
    var arrayImages : [JSON] = [] // Convert all url into image for collection use
    var arraySelectedImagesURL = [URL]()    // Selected images url
    var btnDoneToolbar = UIButton()
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionPhotos.register(UINib(nibName: "YMSPhotoCell", bundle: nil), forCellWithReuseIdentifier:"YMSPhotoCell")
        collectionPhotos.alwaysBounceVertical = true
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemePurpleColor
        self.navigationController?.navigationBar.isTranslucent = false
        let btn1 = UIButton(type: .custom)
        btn1.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        btn1.setTitleColor(UIColor.white, for: .normal)
        btn1.addTarget(self, action: #selector(cancelNavigationTapped), for: .touchUpInside)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: btn1), animated: true)
        
        btnDoneToolbar.setTitle(getCommonString(key: "Done_key"), for: .normal)
        btnDoneToolbar.setTitleColor(UIColor.white, for: .normal)
        btnDoneToolbar.addTarget(self, action: #selector(doneNavigationTapped), for: .touchUpInside)
        btnDoneToolbar.titleLabel?.font = themeFont(size: 18, fontname: .medium)
        btnDoneToolbar.alpha = 0.5
        btnDoneToolbar.isUserInteractionEnabled = false
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: btnDoneToolbar), animated: true)
    }
    
    func setupUI() {
        for i in 0..<arrayURL.count {
            var dict = JSON()
            dict["imgUrl"].url = arrayURL[i]
            dict["selected"] = "0"
            self.arrayImages.append(dict)
        }
    }
    
    @objc func cancelNavigationTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func doneNavigationTapped() {
        self.dismiss(animated: true) {
            for i in 0..<self.arrayImages.count {
                if self.arrayImages[i]["selected"].stringValue == "1" {
                    self.arraySelectedImagesURL.append(self.arrayImages[i]["imgUrl"].url!)
                }
            }
            self.delegateSocialMediaImage?.navigationToController(arrayOfSelectedImageURL: self.arraySelectedImagesURL)
        }
    }
}

//MARK: - Change in YangMangShan
extension FBAndInstaPhotoListVc {
    func setupCellSize() -> CGSize {
        let layout = collectionPhotos.collectionViewLayout as? UICollectionViewFlowLayout
        
        // Fetch shorter length
        let arrangementLength: CGFloat = min(view.frame.width, view.frame.height)
        
        let minimumInteritemSpacing: CGFloat? = layout?.minimumInteritemSpacing
        let sectionInset: UIEdgeInsets? = layout?.sectionInset
        
        let totalInteritemSpacing: CGFloat = CGFloat(max((Int(3) - 1), 0)) * (minimumInteritemSpacing ?? 0.0)
        let totalHorizontalSpacing: CGFloat = totalInteritemSpacing + (sectionInset?.left ?? 0.0) + (sectionInset?.right ?? 0.0)
        
        // Caculate size for portrait mode
        let size = CGFloat(floor((arrangementLength - totalHorizontalSpacing) / 3))
        cellPortraitSize = CGSize(width: size, height: size)
        
        return cellPortraitSize
    }
}

//MARK: - CollectionView delegate and DataSource
extension FBAndInstaPhotoListVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YMSPhotoCell", for: indexPath) as! YMSPhotoCell
        let dict = arrayImages[indexPath.row]
        if dict["selected"].stringValue == "1" {
            cell.imgSelected.isHidden = false
            cell.selectionVeil.isHidden = false
            cell.selectionVeil.alpha = 1
        }
        else {
            cell.imgSelected.isHidden = true
            cell.selectionVeil.isHidden = true
            cell.selectionVeil.alpha = 0
        }
        cell.imageView.sd_setShowActivityIndicatorView(true)
        cell.imageView.sd_setIndicatorStyle(.gray)
        cell.imageView.sd_setImage(with: dict["imgUrl"].url, placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dict = self.arrayImages[indexPath.row]        
        if dict["selected"].stringValue == "0" {
            if intSelectedPhotoCount < numberOfPhotoToSelect {
                dict["selected"].stringValue = "1"
                intSelectedPhotoCount += 1
            }
        }
        else {
            if intSelectedPhotoCount <= numberOfPhotoToSelect {
                dict["selected"].stringValue = "0"
                intSelectedPhotoCount -= 1
            }
        }
        
        self.arrayImages[indexPath.row] = dict
        self.collectionPhotos.reloadData()
        if intSelectedPhotoCount >= 1 {
            btnDoneToolbar.alpha = 1.0
            btnDoneToolbar.isUserInteractionEnabled = true
        }
        else {
            btnDoneToolbar.alpha = 0.5
            btnDoneToolbar.isUserInteractionEnabled = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //        let img = arrayInstagramImage[indexPath.row]
        //        self.selectedPhotos.remove(at: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return setupCellSize()
    }
}
