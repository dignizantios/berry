//
//  RangeSliderUpdateVC.swift
//  Berry
//
//  Created by Anil on 31/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import RangeSeekSlider
import SwiftyJSON

enum SliderType {
    case distance
    case age
}

class RangeSliderUpdateVC: UIViewController {

    //MARK: - @IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSliderValue: UILabel!
    @IBOutlet weak var slider: RangeSeekSlider!
    
    //MARK: - Variable
    var handlerChagesValue:(_ minValue:CGFloat, _ maxValue:CGFloat) -> Void = {_,_ in}
    var sliderType:SliderType = SliderType.distance
    var profileDetails : JSON = JSON()
//    var delegate:SliderDelegate?
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIU()
    }
    
    func setupIU() {
        slider.delegate = self
        slider.handleDiameter = 30
        slider.selectedHandleDiameterMultiplier = 1
        lblTitle.textColor = UIColor.appThemeLightGrayColor
        lblTitle.font = themeFont(size: 18, fontname: .bold)
//        lblTitle.font = themeFont(size: 19, fontname: .bold)
//        lblTitle.textColor = UIColor.appThemeDarkGrayColor
        [lblSliderValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .light)
            lbl?.textColor = UIColor.appThemePurpleColor
        }
        
        setRange(range: slider)
        
        initialUpdateRngeValue()
    }
    
    func setRange(range:RangeSeekSlider) {
        
        range.minValue = 0
        range.maxValue = 50
        if sliderType == SliderType.age {
            range.minValue = 18
            range.minDistance = 1
            range.disableRange = false
            lblSliderValue.text = "\(Int(range.minValue)) - \(Int(range.maxValue)) years"
        } else {
            lblSliderValue.text = "\(Int(range.maxValue)) mi."
            range.disableRange = true
        }

        if let gestures = range.gestureRecognizers {
            for item in gestures {
                print("item:=",item)
            }
        }
        
        range.hideLabels = true
        range.lineHeight = 2.0
        range.tintColor = UIColor.appThemeLightGrayColor
        range.colorBetweenHandles = UIColor.appThemePurpleColor
        range.handleColor = UIColor.appThemePurpleColor
        range.maxLabelColor = UIColor.appThemePurpleColor
        range.minLabelColor = UIColor.appThemePurpleColor
    }
    
    func setTheData(_ data:JSON, _ sliderType:SliderType)  {
        self.profileDetails = data
        self.sliderType = sliderType
    }

    func initialUpdateRngeValue() {
        let details = self.profileDetails.getDictionary(key: .myprofile)
        if sliderType == SliderType.distance {
            lblTitle.text = "Distance"
            self.slider.selectedMaxValue = CGFloat(details.getInt(key: .distance))
            self.lblSliderValue.text = "\(Int(self.slider.selectedMaxValue)) mi."
        } else {
            lblTitle.text = "Age range"
            self.slider.selectedMinValue = CGFloat(details.getInt(key: .min_range))
            self.slider.selectedMaxValue = CGFloat(details.getInt(key: .max_range))
            print("slider.selectedMinValue:=\(slider.selectedMinValue), slider.selectedMaxValue:=\(slider.selectedMaxValue)")
            lblSliderValue.text = "\(Int(self.slider.selectedMinValue)) - \(Int(self.slider.selectedMaxValue)) years"
        }
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        /*if sliderType == SliderType.distance {
                        handlerChagesValue(-1.0, slider.selectedMaxValue)
//            delegate?.updateSliderValues(sliderType, -1.0, slider.selectedMaxValue)
        }
        else {
                        handlerChagesValue(slider.selectedMinValue, slider.selectedMaxValue)
//            delegate?.updateSliderValues(sliderType, slider.selectedMinValue, slider.selectedMaxValue)
        }*/
    }
}
//MARK: - RangeSeeker Delegate
extension RangeSliderUpdateVC: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if sliderType == SliderType.distance {
            lblSliderValue.text = "\(Int(maxValue)) mi."
        } else if sliderType == SliderType.age {
            print(minValue)
            print(maxValue)
            
            lblSliderValue.text = "\(Int(minValue)) - \(Int(maxValue)) years"
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        
        if sliderType == SliderType.distance {
            handlerChagesValue(-1.0, slider.selectedMaxValue)
//            delegate?.updateSliderValues(sliderType, -1.0, slider.selectedMaxValue)
        }
        else {
            handlerChagesValue(slider.selectedMinValue, slider.selectedMaxValue)
//            delegate?.updateSliderValues(sliderType, slider.selectedMinValue, slider.selectedMaxValue)
        }
    }
}
