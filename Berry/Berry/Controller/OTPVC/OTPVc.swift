//
//  OTPVc.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class OTPVc: UIViewController, ChangeMobileNumberDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var lblOTPMsg: UILabel!
    @IBOutlet weak var lblOTP: UILabel!
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var btnDidntReceive: UIButton!
    @IBOutlet weak var btnChangeNumber: UIButton!
    
    //MARK: - Variables
    var isForgot: Bool = false
    var userId: String = ""
    var mobileNo: String = ""
    var otpString: String = ""
    var countryDialCode: String = ""
    var isOpenAppDelegate: Bool = false
    var isOpenSignUP:Bool = false
    var isOpenSocial:Bool = false
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if isForgot {
        }
        else {
//            if isOpenAppDelegate || !isOpenSignUP{
                sendOTP()
//            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isOpenAppDelegate {
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Enter_OTP_key"), isRoot: true)
        }
        else {
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Enter_OTP_key"))
        }        
    }
    
    func setupUI() {
        self.OTPsetup()
        
        lblOTPMsg.text = getCommonString(key: "OTP_msg_key")
        lblOTPMsg.font = themeFont(size: 16, fontname: .light)
        lblOTPMsg.textColor = UIColor.black
        
        lblOTP.text = getCommonString(key: "OTP_key")
        lblOTP.font = themeFont(size: 17, fontname: .bold)
        lblOTP.textColor = UIColor.black
        
        btnDidntReceive.setTitle(getCommonString(key: "Didnt_receive_OTP_key"), for: .normal)
        btnDidntReceive.setTitleColor(UIColor.appThemePurpleColor, for: .normal)
        btnDidntReceive.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        
        btnChangeNumber.setTitleColor(UIColor.appThemePurpleColor, for: .normal)
        btnChangeNumber.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        btnChangeNumber.underline(text: (self.btnChangeNumber.titleLabel?.text!)!)
        
        btnSubmit.setupShadowThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "SUBMIT_key"), for: .normal)
    }
    
    func OTPsetup() {
        otpView.otpFieldDefaultBackgroundColor = UIColor.appThemetxtBackgroundColor
        otpView.otpFieldDefaultBorderColor = UIColor.white
        otpView.otpFieldEnteredBackgroundColor = UIColor.appThemetxtBackgroundColor
        otpView.otpFieldEnteredBorderColor = UIColor.white
        otpView.otpFieldsCount = 6
        otpView.otpFieldSeparatorSpace = 5
        otpView.otpFieldSize = 50
        otpView.otpFieldFont = themeFont(size: 20, fontname: .medium)
        otpView.otpFieldDisplayType = .square
        otpView.delegate = self
        otpView.initalizeUI()
    }
    
    func sendOTP() {
        let param = ["userid": self.userId,"mobile_no":self.mobileNo, "co": self.countryDialCode.dropFirst()] as [String : Any]
        ApiManager.shared.MakePostAPI(name: GENERATE_OTP, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                let msg = json.getString(key: .msg)
                if error != YES {
                    AlertView(title: "Success", message: msg)
                    self.btnChangeNumber.isHidden = true
                }
                else {
                    AlertView(title: "Failed", message: msg)
                    self.btnChangeNumber.isHidden = false
                }
            }
            else {
                self.sendOTP()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func sendOTPForgotPassword() {
        let param = ["mobile_no":self.mobileNo, "co": self.countryDialCode.dropFirst()] as [String : Any]
        ApiManager.shared.MakePostAPI(name: VERIFY_MOBILE, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                let msg = json.getString(key: .msg)
                if error != YES {
                    AlertView(title: "Success", message: "otp generated!")
                }
                else {
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.sendOTPForgotPassword()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func verifyOTP(otp: String) {
        let param = ["userid": self.userId,"otp":otp]
        ApiManager.shared.MakePostAPI(name: VERIFY_OTP, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
                    saveJSON(j: userDetails, key: USER_DETAILS_KEY)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        AppDelegate.shared.OutTime(type: "login")
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVc") as! MainVc
                        obj.isLogin = true
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.verifyOTP(otp: otp)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func verifyResetOTP(otp: String) {
        let param = ["mobile_no": self.mobileNo,"otp":otp, "co": self.countryDialCode.dropFirst()] as [String : Any]
        ApiManager.shared.MakePostAPI(name: VERIFY_RESET_OTP, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
                    let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVc") as!  ResetPasswordVc
                    obj.selectedController = .fromOTP
                    obj.mobileNo = self.mobileNo
                    obj.countryDialCode = self.countryDialCode
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.verifyResetOTP(otp: otp)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    @IBAction func btnRendOtpClicked(_ sender: Any) {
        if isForgot {
            self.sendOTPForgotPassword()
        }
        else {
            self.sendOTP()
        }
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if self.otpString == "" {
            AlertView(title: "Failed", message: OTP_VALIDATION)
        }
        else {
            if isForgot {
                self.verifyResetOTP(otp: self.otpString)
            }
            else {
                self.verifyOTP(otp: self.otpString)
            }
        }
    }
    
    @IBAction func btnChangeMobileNumberClicked(_ sender: Any) {
//        if self.isOpenSocial {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ChangeMobileNumberVC") as! ChangeMobileNumberVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.delegate = self
            obj.mobileNumber = self.mobileNo
            self.present(obj, animated:  false, completion: nil)
//        }
//        else if self.isOpenSignUP {
//            IS_FOR_OTP_FAIL = true
//            self.navigationController?.popToRootViewController(animated: false)
//        }
//        else if self.isOpenAppDelegate {
//            let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RootNav") as! UINavigationController
//            AppDelegate.shared.window?.rootViewController = vc
//        }
//        else {
//            self.navigationController?.popViewController(animated: true)
//        }
    }
    
    func ChangeMobileNumberDidFinish(mobileNo: String, countryDialCode: String, countryName: String, CountryCode: String) {
        self.countryDialCode = countryDialCode
        self.mobileNo = mobileNo
//        if isOpenAppDelegate || isOpenSocial {
        if isForgot {
            self.sendOTPForgotPassword()
        }
        else {
            self.sendOTP()
        }
//        }
    }
}


extension OTPVc: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        return true
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        self.otpString = otpString
    }
}

extension UIButton {
    func underline(text: String, state: UIControl.State = .normal, color:UIColor? = nil) {
        var titleString = NSMutableAttributedString(string: text)
        
        if let color = color {
            titleString = NSMutableAttributedString(string: text,
                                                    attributes: [NSAttributedString.Key.foregroundColor: color])
        }
        
        let stringRange = NSMakeRange(0, text.count)
        titleString.addAttribute(NSAttributedString.Key.underlineStyle,
                                 value: NSUnderlineStyle.single.rawValue,
                                 range: stringRange)
        
        self.setAttributedTitle(titleString, for: state)
    }
    
}
