//
//  UIViewController+Extension.swift
//  Liber
//
//  Created by om on 10/3/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

func convertDate(str:String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    return dateFormatter.date(from:str)!
}

extension Date {
    var day: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        return dateFormatter.string(from: self)
    }
    
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIViewController
{
    func ReportDateIssueToserver(data:JSON)
    {
        let param = ["userid":data.getString(key: .userid), "date":data.getString(key: .date), "opp": data.getString(key: .opp),"messageid": data.getString(key: ._id)] as [String : Any]
        ApiManager.shared.MakePostAPI(name: REPORT_CRASH_DATE, params: param, progress: false, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                print(json)
            }
        }
    }
    
    func convertFormate(str:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from:str)!
        return date.toString(dateFormat: "dd MMMM, yyyy")
    }
    
    func convertDate(str:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.date(from:str)!
    }
    
    func getDay(str:String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy HH:mm:ss"
        //        dateFormatter.dateFormat = "yyyy-MM-DD'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from:str)!
        return Int(date.day)!
    }
    
    func getFirstDate(str:String,messageData:JSON) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "dd MMMM, yyyy HH:mm:ss"
        if let date = dateFormatter.date(from:str)
        {
            return date.toString(dateFormat: "EEE dd MMM, HH:mm")
        }
        self.ReportDateIssueToserver(data: messageData)
        return ""
    }
    
    func getTimeOnly(str:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "dd MMMM, yyyy HH:mm:ss"
        let date = dateFormatter.date(from:str)!
        return date.toString(dateFormat: "HH:mm")
    }
    
    func UTCToLocal(date:String, messageData:JSON) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        if let dt = dateFormatter.date(from: date)
        {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "dd MMMM, yyyy HH:mm:ss"
            return dateFormatter.string(from: dt)
        }
        self.ReportDateIssueToserver(data: messageData)
        return ""
    }
    
    func UTCToLocalAccount(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        return dateFormatter.string(from: dt!)
    }

    
    //MARK: - Set TimeStamp    
    func timeAgoSinceDate(str:String, numericDates:Bool) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy HH:mm:ss"
//        dateFormatter.dateFormat = "yyyy-MM-DD'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from:str)!
        let calendar = Calendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
}
