//
//  GalleryCollectionViewCell.swift
//  testingTinderSwipe
//
//  Created by om on 10/15/18.
//  Copyright © 2018 Nicky. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet var imgvwGalleryImage : UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        imgvwGalleryImage.layer.cornerRadius = 5
        imgvwGalleryImage.layer.masksToBounds = true
        
    }

}
