//
//  MatchCardFirstTblCell.swift
//  Berry
//
//  Created by YASH on 31/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit


//protocol ExpandingCellDelegate {
//    func updateCellHeight (indexPath: NSIndexPath, comment:String)
//}


class MatchCardFirstTblCell: UITableViewCell {

    @IBOutlet weak var imgOppUser: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgOppUserProfile: UIImageView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var vwGreadient: UIView!
    @IBOutlet weak var txtvwAnswer: UITextView!
    @IBOutlet weak var heightTxtVw: NSLayoutConstraint!
    @IBOutlet weak var vwBackColor: UIView!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var lblQuestions: UILabel!    
    @IBOutlet weak var lblOtherAns: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
    let gradientLayer:CAGradientLayer = CAGradientLayer()
    
    //MARK: - AwakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgOppUser.layer.cornerRadius = self.imgOppUser.bounds.width / 2
        self.imgOppUser.layer.masksToBounds = true
        
        self.imgUser.layer.cornerRadius = self.imgUser.bounds.width / 2
        self.imgUser.layer.masksToBounds = true
        
        self.imgOppUserProfile.layer.cornerRadius = self.imgOppUserProfile.bounds.width / 2
        self.imgOppUserProfile.layer.masksToBounds = true
        
        self.imgUserProfile.layer.cornerRadius = self.imgUserProfile.bounds.width / 2
        self.imgUserProfile.layer.masksToBounds = true
        txtvwAnswer.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        
        vwGreadient.layer.cornerRadius = 5
        vwGreadient.layer.masksToBounds = true
        
        gradientLayer.colors =
            [UIColor(red: 249.0/255.0, green: 231.0/255.0, blue: 0.0/255.0, alpha: 1).cgColor,UIColor(red: 249.0/255.0, green: 195.0/255.0, blue: 0.0/255.0, alpha: 1).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        //Use diffrent colors
        
        vwGreadient.layer.addSublayer(gradientLayer)
        // Initialization code
    }
 
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradientLayer.frame = self.bounds
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        vwBackColor.roundCorners(corners: [.bottomLeft,.bottomRight,.topRight], radius: 5.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }    
}
