//
//  MatchCardVc.swift
//  Berry
//
//  Created by YASH on 31/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import NVActivityIndicatorView
import SDWebImage
//import GiphyCoreSDK

class MatchCardVc: UIViewController, SockerConnectionDelegate, UnMatchUserDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var collectionUsers: UICollectionView!
    @IBOutlet weak var vwUpperCollectionUser: UIView!
    @IBOutlet weak var constantVwupperCollectionUser: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtvwMsg: UITextView!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var constantBottomofView: NSLayoutConstraint!
    @IBOutlet weak var vwBottomContain: UIView!
    @IBOutlet weak var heightBottom: NSLayoutConstraint!
    @IBOutlet weak var constantTopOfHeaderVw: NSLayoutConstraint!        
    @IBOutlet weak var lblNotFound: UILabel!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var viewGif: UIView!
    @IBOutlet weak var gifCollectionView: UICollectionView!    
    @IBOutlet weak var txtSearchGIF: UITextField!
    
    //MARK: - Variables
    var lastScrollOffset : CGFloat = 0
    var arrayUser : [JSON] = []
    var textViewHeight = CGFloat()
    var isKeyboardOpen = false
    var selectedUser : JSON = JSON()
    var questionstArray : [JSON] = [JSON]()
    var gifArray : [String] = [String]()
    var roomId : String = ""
    let profile = userDetails.getDictionary(key: .myprofile)
    var isUserScrolled : Bool = false
    var isLoadMore : Bool = false
    var page : Int = 1
    var isFirstTime : Bool = true
    var isReload: Bool = true
    var lastMessageId : String = ""
    var isGIFShow: Bool = false
    var offsetGif : Int = 0
    var isFilterGIF : Bool = false
    var tableviewTxtviewisClicked = false
    weak var delegate: ChangeIndexCarbonkitDelegate?
    var isLoaded:Bool = false

    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewGif.isHidden = true
        self.viewGif.addShadow(location: .top, color: UIColor.black, opacity: 0.1, radius: 5)
        self.view.layoutIfNeeded()
        self.tblChat.layoutIfNeeded()
        self.viewMain.isHidden = true
        self.txtSearchGIF.delegate = self
        collectionUsers.register(UINib(nibName: "userCollectionCell", bundle: nil), forCellWithReuseIdentifier:"userCollectionCell")
        tblChat.register(UINib(nibName: "RightSideTblCell", bundle: nil), forCellReuseIdentifier: "RightSideTblCell")
        tblChat.register(UINib(nibName: "MatchCardFirstTblCell", bundle: nil), forCellReuseIdentifier: "MatchCardFirstTblCell")
        tblChat.register(UINib(nibName: "LeftSideTblCell", bundle: nil), forCellReuseIdentifier: "LeftSideTblCell")
        tblChat.register(UINib(nibName: "RightSideImageTblCell", bundle: nil), forCellReuseIdentifier: "RightSideImageTblCell")
        tblChat.register(UINib(nibName: "LeftSideImageTblCell", bundle: nil), forCellReuseIdentifier: "LeftSideImageTblCell")
        SocketIOHandler.shared.readMatch()
        tblChat.tableFooterView = UIView()
        txtvwMsg.delegate = self
        txtMessage.delegate = self
        txtMessage.autocapitalizationType = .sentences
        txtvwMsg.tag = 1
        self.tblChat.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.tblChat.addGestureRecognizer(tap)        
        //        self.tblChat = UITableView(frame: .zero, style: .grouped)
        //        vwBottomContain.isHidden = true
        self.vwBottomContain.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshView(notification:)), name: NSNotification.Name(rawValue: "refreshView"), object: nil)
        self.resetDetails()
        self.getGifImages()

        self.lblNotFound.text = "Get swiping!" //"No match yet. Get swiping!"//"There is no more match found at this time."

        self.lblNotFound.underline()
        
        let tapLabel = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        self.lblNotFound.isUserInteractionEnabled = true
        self.lblNotFound.addGestureRecognizer(tapLabel)
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        self.delegate?.ChangeIndexCarbonkit(index: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !SocketIOHandler.shared.isSocketConnected() {
            SocketIOHandler.shared.Connect()
        }
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
   
    
    @objc func refreshView(notification : Notification) {
        self.refreshData()
    }
    
    func refreshData() {
        CHAT_USER_ID = ""
        if SELECTED_TAB == 2 {
            if !SocketIOHandler.shared.isSocketConnected() {
                SocketIOHandler.shared.Connect()
            }
            CHAT_USER_ID = ""
            SocketIOHandler.shared.readMatch()
            self.isGIFShow = false
            self.viewGif.isHidden = true
            self.resetDetails()
        }
    }
    
    func resetDetails() {
        self.roomId = ""
        self.tblChat.isHidden = true
        if !self.isLoaded {
            self.isLoaded = true
            self.showProgress(vc: self)
        }
        self.lastMessageId = ""
        self.page = 1
        self.vwBottomContain.isHidden = true
        self.tblChat.isUserInteractionEnabled = false
        self.viewMain.isHidden = true
        self.selectedUser = JSON()
        self.questionstArray.removeAll()
        self.tblChat.reloadData()
        
        SocketIOHandler.shared.delegate = self
        
        if PUSH_TYPE == "chat" {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.tblChat.isHidden = false
                SocketIOHandler.shared.chatUsersList()
            }
        }
        else {
            SocketIOHandler.shared.chatUsersList()
        }
        self.constantTopOfHeaderVw.constant = 0 //-160
        //Register Notification for keyboard hide/show
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1)) {
            DispatchQueue.main.async {
                self.stopAnimating()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func dismissKeyboard() {
        self.isGIFShow = false
        self.viewGif.isHidden = true
        view.endEditing(true)
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            UIView.performWithoutAnimation {
                if self.questionstArray.count != 0 {
                    let indexPath = IndexPath(row: (self.questionstArray.count)-1, section: 0)
                    self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1)) {
                        self.isUserScrolled = true
                    }
                }
            }
        }
    }
    
    func reloadUserListAndChatHistoryWithSpecific(Opp:String) {
        
        if let index = arrayUser.index(where: {$0["userid"].stringValue == Opp}) {
            arrayUser.remove(at: index)
        }
        constantTopOfHeaderVw.constant = -40
        vwBottomContain.isHidden = true
        tblChat.isUserInteractionEnabled = false
        viewGif.isHidden = true
        selectedUser = JSON()
        questionstArray.removeAll()
        collectionUsers.reloadData()
        tblChat.reloadData()
        
    }
    
    // MARK:- SockerConnectionDelegate
    func reloadChatUser(response: JSON) {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
        self.arrayUser.removeAll()
        let data = response.getArray(key: .data)
        for profile in data {
            var profileData = profile
            if profile.getString(key: .userid) == self.selectedUser.getString(key: .userid) {
                profileData["selected"] = "1"
            }
            else {
                profileData["selected"] = "0"
            }
            self.arrayUser.append(profileData)
        }
        print("arrayUser Count : ", self.arrayUser.count)
        self.lblNotFound.isHidden = data.count == 0 ? false : true        
        self.viewMain.isHidden = self.arrayUser.count == 0 ? true : false
        self.constantTopOfHeaderVw.constant = 0
        self.collectionUsers.reloadData()

        if PUSH_TYPE == "chat" {
            PUSH_TYPE = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                self.OpenPushChat()
            }
        }
    }
    
    
    func OpenPushChat() {
        for (index, dict) in arrayUser.enumerated() {
            if dict.getString(key: .userid) == PUSH_USER_ID {
                PUSH_USER_ID = ""
                self.selectedUser = dict
                CHAT_USER_ID = self.selectedUser.getString(key: .userid)
                self.page = 1
                self.isFirstTime = true
                self.view.endEditing(true)
                self.collectionUsers.scrollToItem(at: IndexPath(item: index, section: 0), at: .left, animated: false)
                self.lastMessageId = ""
                self.isLoadMore = false
                self.showProgress(vc: self)
                self.questionstArray.removeAll()
                self.tblChat.reloadData {
                    for i in 0..<self.arrayUser.count {
                        var dict = self.arrayUser[i]
                        dict.setValue(key: .selected, value: "0")
                        self.arrayUser[i] = dict
                    }
                    
                    var dict = self.arrayUser[index]
                    dict.setValue(key: .selected, value: "1")
                    self.arrayUser[index] = dict
                    self.collectionUsers.reloadData()
                    SocketIOHandler.shared.SingleChatDetails(oppUserId: dict.getString(key: .userid))
                }
            }
        }
    }
    
    func reloadSingleChatDetails(response: JSON) {
        self.questionstArray.removeAll()
        let data = response.getDictionary(key: .data)
        self.questionstArray.append(data)
        self.roomId = data.getString(key: .roomId)
        self.isLoadMore = true
        let bothUserInfo = data.getArray(key: .bothUserInfo)
        var isShow : Bool = false
        for user in bothUserInfo {
            if user.getString(key: .ans) == "" {
                isShow = true
            }
        }
        constantTopOfHeaderVw.constant = 0.0
        self.vwBottomContain.isHidden = isShow
        print("isShow:=",isShow)
        self.heightBottom.constant = isShow ? 0 : 50
        
        SELECTED_TAB = 2
        if SELECTED_TAB == 2 {
            SocketIOHandler.shared.readMsg(oppId: self.selectedUser.getString(key: .userid), roomId: self.roomId)
            SocketIOHandler.shared.ChatListDetails(roomId: self.roomId, lastId: self.lastMessageId)
        }
    }
    
    func reloadChatList(response: [JSON]) {
        self.view.isUserInteractionEnabled = true
        self.tblChat.isUserInteractionEnabled = true

        DispatchQueue.main.async {
            self.stopAnimating()
        }
        let array = response.reversed()
        if page == 1 {
            for chat in array {
                self.questionstArray.append(chat)
            }
            print(self.questionstArray)
            if self.questionstArray.count != 1 && self.questionstArray.count != 0 {
                self.lastMessageId = self.questionstArray[1].getString(key: ._id)
            }
            
            self.tblChat.reloadData {
                if self.isFirstTime {
                    self.isFirstTime = false
                    if self.questionstArray.count != 0 && self.questionstArray.count != 1 {
                        self.isUserScrolled = false
                        self.scrollToBottom()
                    }
                }
            }
        }
        else {
            for chat in response {
                self.questionstArray.insert(chat, at: 1)
            }
            
            if self.questionstArray.count != 1 {
                self.lastMessageId = self.questionstArray[1].getString(key: ._id)
            }
            
            if response.count != 0 {
                UIView.performWithoutAnimation {
                    let position = self.tblChat.contentOffset
                    self.tblChat.beginUpdates()
                    self.tblChat.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .none)
                    self.tblChat.endUpdates()
                    self.tblChat.contentOffset = position
                }
            }
        }
        
        if response.count != 0 {
            self.isLoadMore = true
            self.page += 1
        }
    }
    
    func reloadSendChat(response: JSON) {
        print(response)
        if self.selectedUser.getString(key: .userid) != "" {
            if SELECTED_TAB == 2 {
                SocketIOHandler.shared.readMsg(oppId: self.selectedUser.getString(key: .userid), roomId: self.roomId)
            }
        }
        if response.isEmpty == false {
            print(self.roomId)
            if response.getString(key: .roomId) == self.roomId {
                self.questionstArray.append(response)
            }
            else {
                for (index, data) in arrayUser.enumerated() {
                    if data.getString(key: .userid) == response.getString(key: .userid) {
                        self.arrayUser[index].setIntValue(key: .read, value: 0)
                        self.collectionUsers.reloadItems(at: [IndexPath(item: index, section: 0)])
                    }
                }
            }
        }
        
        self.tblChat.reloadData {
            self.isUserScrolled = false
            self.scrollToBottom()
        }
    }
    
    func reloadOnline(response: JSON) {
        var reloadIndex : Int = 0
        for (index, user) in self.arrayUser.enumerated() {
            if response.getString(key: .userid) == user.getString(key: .userid) {
                reloadIndex = index
                self.arrayUser[index].setIntValue(key: .status, value: response.getInt(key: .status))
            }
        }
        self.collectionUsers.reloadItems(at: [IndexPath(row: reloadIndex, section: 0)])
    }    
    
    func showProgress(vc: UIViewController) {
        DispatchQueue.main.async {
            let LoaderString:String = "Loading..."
            let LoaderSize = CGSize(width: 30, height: 30)
            vc.startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        }
    }
    
    func reloadUnreadMsg(response: JSON) {
        print(response.getInt(key: .totalCount))
        delegate?.ChangeBadge(badgeValue: String(response.getInt(key: .totalCount)))
    }
    
    func reloadMatchUser(response: JSON) {
        self.viewMain.isHidden = false
        self.lblNotFound.isHidden = true
        print(response)
        var isSearched: Bool = false
        for profile in self.arrayUser {
            if profile.getString(key: .userid) == response.getString(key: .userid) {
                isSearched = true
            }
        }
        if !isSearched {
            var profileData = response
            profileData["selected"] = "0"
            self.arrayUser.append(profileData)
            self.collectionUsers.reloadData()
        }
    }
    
    func UnMatchUserDidFinish(userId: String) {
        self.refreshData()
    }
}

//MARK: - IBAction method
extension MatchCardVc {
    
    @IBAction func btnGIFTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewGif.isHidden = self.isGIFShow ? true : false
        self.isGIFShow = !isGIFShow
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
        
    }    
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        guard ReachabilityTest.isConnectedToNetwork() else {
            AlertView(title: "Failed", message: "No internet connection available")
            return
        }
        
        if self.txtMessage.text!.toTrim() == "" {
            return
        }
        
        SocketIOHandler.shared.SendMsg(oppUserId: self.selectedUser.getString(key: .userid), msg: self.txtMessage.text!.toTrim())
        self.txtMessage.text = ""
    }
}


//    MARK:- KeyBord Notifiction Action
extension MatchCardVc {
    @objc func keyboardWillShow(_ notification: NSNotification) {
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            let keyboardHeight = keyboardRectValue.height
            isKeyboardOpen = true
            if tableviewTxtviewisClicked {
                self.tblChat.contentInset.bottom = keyboardHeight
                /*
                UIView.performWithoutAnimation {
                    tblChat.contentInset.bottom = keyboardHeight
//                        UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
                }*/
                return
            }
            UIView.animate(withDuration: 1.5, animations: {
                var bottomPadding: CGFloat = 0.0
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
                }
                self.constantBottomofView.constant = keyboardHeight - bottomPadding
                //   self.view.layoutIfNeeded()
            }, completion: { (status) in
                self.scrollToBottom()
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        isKeyboardOpen = false
        if tableviewTxtviewisClicked {
            tableviewTxtviewisClicked = false
            tblChat.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return
        }
        UIView.animate(withDuration: 1.5, animations: {
            self.constantBottomofView.constant = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension MatchCardVc : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        tableviewTxtviewisClicked = false
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtSearchGIF {
            if let text = textField.text,
                let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                self.offsetGif = 0
                isFilterGIF = updatedText == "" ? false : true
                if updatedText == "" {
                    self.getGifImages()
                }
                else {
                    self.searchGifImages(text: updatedText)
                }
            }
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        tableviewTxtviewisClicked = false
        return true
    }
}
   
//MARK: - Textview Delegate method
extension MatchCardVc : UITextViewDelegate {
    
    func textViewShouldReturn(_ textView: UITextView!) -> Bool {
        self.view.endEditing(true)
        if textView != self.txtvwMsg {
            
        }
        tableviewTxtviewisClicked = false
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.tag == 1 {
            if(textView.text == "") {
//                lblPlaceholder.isHidden = false
            }
            else{
//                lblPlaceholder.isHidden = true
            }
        }
        else if textView.tag == 2 {
            // for updating the tableView appearence (don't use "reloadData", it will resignFirstResponder the textView)
            UIView.setAnimationsEnabled(false)
            let loc = self.tblChat.contentOffset
            tblChat.beginUpdates()
            let cell = tblChat.cellForRow(at: IndexPath(row: 0, section: 0)) as! MatchCardFirstTblCell
            if textView.text == "" {
                cell.lblPlaceHolder.isHidden = false
            }
            else {
                cell.lblPlaceHolder.isHidden = true
                cell.txtvwAnswer.text = textView.text
            }
            tblChat.endUpdates()
            tblChat.contentOffset = loc
            UIView.setAnimationsEnabled(true)
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.tag == 2 {
            tableviewTxtviewisClicked = true
        } else {
            tableviewTxtviewisClicked = false
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            if textView != self.txtvwMsg {
                if textView.text! == "" {
                    self.showToast(message: "Please enter answer")
                    return false
                }
                guard ReachabilityTest.isConnectedToNetwork() else {
                    AlertView(title: "Failed", message: "No internet connection available")
                    return false
                }
                
                SocketIOHandler.shared.AddQuestions(oppUserId: self.selectedUser.getString(key: .userid), ans: textView.text!)
                textView.isUserInteractionEnabled = false
            }
            else {
                textView.resignFirstResponder()
                if textView.tag == 2 {
                    //TODO: - Changes
                    //                textView.isUserInteractionEnabled = false
                    //                vwBottomContain.isHidden = false
                }
                return false
            }
        }
        return true
    }
    
    func actionDiscovered(opp: String, type: TYPE_ACTION) {
        let param = ["userid": userDetails.getString(key: .userid),"opp": opp, "type": type.rawValue] as [String : Any]
        ApiManager.shared.MakePostAPI(name: ACTION_DESCOVERED, params: param, progress: false, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.actionDiscovered(opp: opp, type: type)
//                AlertView(title: "Failed", message: error.debugDescription)
            }
        }
    }    
}

extension MatchCardVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == gifCollectionView {
            if (indexPath.row + 5) == self.gifArray.count {
                self.isFilterGIF ? self.searchGifImages(text: self.txtSearchGIF.text!) : self.getGifImages()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == collectionUsers ? arrayUser.count : gifArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionUsers {
            return CGSize(width: 65, height: 80)
        }
        else {
            return CGSize(width: 100, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView != collectionUsers {            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GIFImagsCell", for: indexPath) as! GIFImagsCell
            
            let url = URL(string: self.gifArray[indexPath.row])
            cell.imgGIF.gifImage = nil            
            cell.imgGIF.setGifFromURL(url, completionHandler: { (done) in
            })
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for: indexPath) as! userCollectionCell
            
            let dict = arrayUser[indexPath.row]
            if dict.getInt(key: .status) == 1 {
//                cell.vwGreenOnline.isHidden = false
                cell.imgUser.layer.borderWidth = 2
                cell.imgUser.layer.borderColor = UIColor.hexStringToUIColor(hex: "#00D100").cgColor
            }
            else {
//                cell.vwGreenOnline.isHidden = true
                let superLike = dict.getInt(key: .superLike)
                if superLike == 1 {
                    cell.imgUser.layer.borderWidth = 2
                    cell.imgUser.layer.borderColor = UIColor.hexStringToUIColor(hex: "#4B296D").cgColor
                }
                else {
                    cell.imgUser.layer.borderWidth = 0
                    cell.imgUser.layer.borderColor = UIColor.clear.cgColor
                }
            }
            
            let read = dict.getInt(key: .read)
            if read == 0 {
                cell.vwGreenOnline.isHidden = false
//                cell.imgUser.layer.borderWidth = 2
//                cell.imgUser.layer.borderColor = UIColor.hexStringToUIColor(hex: "#00D100").cgColor
            }
            else {
                cell.vwGreenOnline.isHidden = true

//                let superLike = dict.getInt(key: .superLike)
//                if superLike == 1 {
//                    cell.imgUser.layer.borderWidth = 2
//                    cell.imgUser.layer.borderColor = UIColor.hexStringToUIColor(hex: "#4B296D").cgColor
//                }
//                else {
//                    cell.imgUser.layer.borderWidth = 0
//                    cell.imgUser.layer.borderColor = UIColor.clear.cgColor
//                }
            }
            cell.imgUser.clipsToBounds = true
            cell.imgUser?.sd_setImage(with: URL(string: dict.getString(key: .profile)), completed: nil)
            cell.imgUser.layer.masksToBounds = true
            if dict.getString(key: .selected) == "0" {
                cell.constantImgWidth.constant = 45
                cell.ConstantimgHeight.constant = 45
                cell.imgUser.layer.cornerRadius = 22.5
                cell.constantImgTop.constant = 10
                cell.lblName.isHidden = true
                cell.lblName.text = ""
            }
            else {
                cell.constantImgWidth.constant = 50
                cell.ConstantimgHeight.constant = 50
                cell.imgUser.layer.cornerRadius = 25
                cell.constantImgTop.constant = 5
                cell.lblName.isHidden = false
                cell.lblName.text = dict.getString(key: .name).capitalized
            }
            return cell
        }
    }    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != collectionUsers {
            guard ReachabilityTest.isConnectedToNetwork() else {
                AlertView(title: "Failed", message: "No internet connection available")
                return
            }
            self.isGIFShow = false
            self.viewGif.isHidden = true
            let image = BERRY_MEDIA + self.gifArray[indexPath.row]
            
            SocketIOHandler.shared.SendMsg(oppUserId: self.selectedUser.getString(key: .userid), msg: image)
        }
        else {
            self.tblChat.isHidden = false
            let dict = arrayUser[indexPath.row]
            self.selectedUser = dict
            CHAT_USER_ID = self.selectedUser.getString(key: .userid)
            self.page = 1
            self.isFirstTime = true
            self.view.endEditing(true)
            if dict.getString(key: .selected) == "1" {
                self.isReload = false
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MatchCardProfileVc") as! MatchCardProfileVc
                obj.oppUserId = dict.getString(key: .userid)
                obj.delegate = self
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else {
                self.lastMessageId = ""
                self.isLoadMore = false
                if !self.isLoaded {
                    self.isLoaded = true
                    self.showProgress(vc: self)
                }
                self.questionstArray.removeAll()
                self.tblChat.reloadData {
                    for i in 0..<self.arrayUser.count {
                        var dict = self.arrayUser[i]
                        dict.setValue(key: .selected, value: "0")
                        self.arrayUser[i] = dict
                    }
                    
                    var dict = self.arrayUser[indexPath.row]
                    dict.setValue(key: .selected, value: "1")
                    dict.setIntValue(key: .read, value: 1)
                    print(dict)
                    self.arrayUser[indexPath.row] = dict
                    self.collectionUsers.reloadData()
                    SocketIOHandler.shared.readMatch()
                    SocketIOHandler.shared.SingleChatDetails(oppUserId: dict.getString(key: .userid))
                }
            }
        }
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //    {
    //
    //        let width = (collectionView.frame.size.width/4)
    //        // let height = (collectionView.frame.size.height/4)
    //
    //        return CGSize(width: width , height:width)
    //    }
}


extension MatchCardVc : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let _ = scrollView as? UICollectionView {
            return
        }
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            //scroll Direction - Down
            self.constantTopOfHeaderVw.constant = -80 //-160
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            if isUserScrolled {
                if scrollView.panGestureRecognizer.translation(in: scrollView).y <  -100 {
                    //                    self.dismissKeyboard()
                }
            }
        }
        else {
            //scroll Direction - up
            if tableviewTxtviewisClicked {
                return
            }
            if self.tblChat.contentOffset.y < 1500 && self.isLoadMore{
                self.isLoadMore = false
                self.tblChat.setContentOffset(CGPoint(x: 0, y: self.tblChat.contentOffset.y), animated: false)
                self.view.isUserInteractionEnabled = false
                SocketIOHandler.shared.ChatListDetails(roomId: self.roomId, lastId: self.lastMessageId)
            }
            self.constantTopOfHeaderVw.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension MatchCardVc: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 400 : 100//162
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.questionstArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let details = self.questionstArray.first
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchCardFirstTblCell") as!  MatchCardFirstTblCell
            cell.txtvwAnswer.delegate = self
            cell.txtvwAnswer.tag = 2
            cell.lblQuestions.text = details?.getString(key: .question)
            let bothUserInfo = details?.getArray(key: .bothUserInfo)
            for user in bothUserInfo! {
                if user.getString(key: .userid) != userDetails.getString(key: .userid) {
                    cell.lblOtherAns.text = user.getString(key: .ans) == "" ? "Question not answered yet." : user.getString(key: .ans)
                    cell.imgOppUser.sd_setImage(with: URL(string: self.selectedUser.getString(key: .profile)), completed: nil)
                    cell.imgOppUserProfile.sd_setImage(with: URL(string: self.selectedUser.getString(key: .profile)), completed: nil)
                }
                else {
                    let profileURL = (self.profile.getArray(key: .profile).first?.string)!
                    cell.imgUser.sd_setImage(with: URL(string: profileURL), completed: nil)
                    cell.imgUserProfile.sd_setImage(with: URL(string: profileURL), completed: nil)
                    cell.imgLock.isHidden = user.getString(key: .ans) == "" ? false : true
                    cell.lblOtherAns.isHidden = user.getString(key: .ans) == "" ? true : false
                    cell.txtvwAnswer.text = user.getString(key: .ans)
                    cell.txtvwAnswer.isUserInteractionEnabled = user.getString(key: .ans) == "" ? true : false
                    cell.lblPlaceHolder.isHidden = user.getString(key: .ans) == "" ? false : true
                }
            }
            
            if bothUserInfo?.first?.getString(key: .ans) == "" && bothUserInfo?[1].getString(key: .ans) == "" {
                cell.imgLock.isHidden = true
                cell.lblOtherAns.isHidden = false
            }
            
            return cell
        }
        else {
            let details = self.questionstArray[indexPath.row]
            if details.getString(key: .userid) != userDetails.getString(key: .userid) {
                let msg = details.getString(key: .msg)
                if (msg.range(of: BERRY_MEDIA) != nil) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeftSideImageTblCell") as!  LeftSideImageTblCell

                    let image = msg.replace(target: BERRY_MEDIA, withString: "")
                    cell.layoutIfNeeded()
                    cell.contentView.layoutIfNeeded()
                    let url = URL(string: image)
                    if self.questionstArray.count == (indexPath.row + 1) && cell.imgLeftMessage.gifImage != nil {
                        cell.imgLeftMessage.gifImage = nil
                    }
                    cell.imgLeftMessage.setGifFromURL(url, completionHandler: { (done) in
                    })
                    /*
                    cell.imgLeftMessage.sd_setImage(with: url) { (image, error, cacheType, url) in
                        let imageConverted = image?.resize(maxWidthHeight: Double(UIScreen.main.bounds.width - 80))
                        cell.height.constant = imageConverted!.size.height
                        cell.imgLeftMessage.image = imageConverted
                        cell.imgLeftMessage.setGifFromURL(url, completionHandler: { (done) in
                            cell.height.constant = imageConverted!.size.height
                        })
                    }
                     */
                    cell.lblName.text = self.selectedUser.getString(key: .name).capitalized
                    
                    let index = indexPath.row == 1 ? 0 : 1
                    let date = details.getString(key: .date)
                    let detailsOld = self.questionstArray[indexPath.row - index]
                    let dateOld = detailsOld.getString(key: .date)
                    if index == 0
                    {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else if self.getDay(str: dateOld) != self.getDay(str: date) {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else {
                        cell.lblTime.text = self.getTimeOnly(str: date) //timeAgoSinceDate(str: details.getString(key: .date), numericDates: false)
                    }
                    
//                    cell.lblTime.text = timeAgoSinceDate(str: details.getString(key: .date), numericDates: false)
                    cell.imgLeftProfile.sd_setImage(with: URL(string: self.selectedUser.getString(key: .profile)), completed: nil)
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeftSideTblCell") as!  LeftSideTblCell
                    cell.lblLeftMsg.text = details.getString(key: .msg)
                    cell.lblName.text = self.selectedUser.getString(key: .name).capitalized
                    
                    let index = indexPath.row == 1 ? 0 : 1
                    let date = details.getString(key: .date)
                    let detailsOld = self.questionstArray[indexPath.row - index]
                    let dateOld = detailsOld.getString(key: .date)
                    if index == 0
                    {
//                        if indexPath.row == 1
//                        {
//                            self.ReportDateIssueToserver(data: details)
//                        }
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else if self.getDay(str: dateOld) != self.getDay(str: date) {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else {
                        cell.lblTime.text = self.getTimeOnly(str: date) //timeAgoSinceDate(str: details.getString(key: .date), numericDates: false)
                    }
                    
                    cell.imgLeftProfile.sd_setImage(with: URL(string: self.selectedUser.getString(key: .profile)), completed: nil)
                    return cell
                }
            
            }
            else {
                let msg = details.getString(key: .msg)
                if (msg.range(of: BERRY_MEDIA) != nil) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "RightSideImageTblCell") as!  RightSideImageTblCell
                 
                    cell.layoutIfNeeded()
                    cell.contentView.layoutIfNeeded()
                    cell.setNeedsDisplay()
                    cell.contentView.setNeedsDisplay()
                    cell.imgRightMessage.setNeedsDisplay()
                    let image = msg.replace(target: BERRY_MEDIA, withString: "")

                    let url = URL(string: image)
                    if self.questionstArray.count == (indexPath.row + 1) && cell.imgRightMessage.gifImage != nil {
                        cell.imgRightMessage.gifImage = nil
                    }
                    cell.imgRightMessage.setGifFromURL(url, completionHandler: { (done) in
                    })
                    /*
                    cell.imgRightMessage.sd_setImage(with: url) { (image, error, cacheType, url) in
                        let imageConverted = image?.resize(maxWidthHeight: Double(UIScreen.main.bounds.width - 80))
                        cell.height.constant = imageConverted!.size.height
                        cell.width.constant = imageConverted!.size.width
                        cell.imgRightMessage.image = imageConverted
                        cell.imgRightMessage.setGifFromURL(url, completionHandler: { (done) in
                            cell.height.constant = imageConverted!.size.height
                            cell.width.constant = imageConverted!.size.width
                        })
                    }
                     */
                    cell.lblName.text = self.profile.getString(key: .name).capitalized

                    let index = indexPath.row == 1 ? 0 : 1
                    let date = details.getString(key: .date)
                    let detailsOld = self.questionstArray[indexPath.row - index]
                    let dateOld = detailsOld.getString(key: .date)
                    
                    if index == 0 {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else if self.getDay(str: dateOld) != self.getDay(str: date) {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else {
                        cell.lblTime.text = self.getTimeOnly(str: date) //timeAgoSinceDate(str: details.getString(key: .date), numericDates: false)
                    }
                    
                    let profileURL = (self.profile.getArray(key: .profile).first?.string)!
                    cell.imgRightProfile.sd_setImage(with: URL(string: profileURL), completed: nil)
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "RightSideTblCell") as!  RightSideTblCell
                    cell.lblRightMsg.text = details.getString(key: .msg)
                    cell.lblName.text = self.profile.getString(key: .name).capitalized
                    
                    let index = indexPath.row == 1 ? 0 : 1
                    let date = details.getString(key: .date)
                    let detailsOld = self.questionstArray[indexPath.row - index]
                    let dateOld = detailsOld.getString(key: .date)
                    if index == 0 {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else if self.getDay(str: dateOld) != self.getDay(str: date) {
                        cell.lblTime.text = self.getFirstDate(str: date, messageData: details)
                    }
                    else {
                        cell.lblTime.text = self.getTimeOnly(str: date) //timeAgoSinceDate(str: details.getString(key: .date), numericDates: false)
                    }
                    
                    let profileURL = (self.profile.getArray(key: .profile).first?.string)!
                    cell.imgRightProfile.sd_setImage(with: URL(string: profileURL), completed: nil)
                    return cell
                }
            }
        }
    }
    
    
    func getGifImages() {
        GiphyCore.shared.trending(.gif, offset: self.offsetGif, limit: 20, rating: GPHRatingType.ratedG) { (response, error) in
            if let response = response {
                
                if self.offsetGif == 0 {
                    self.gifArray.removeAll()
                }
                
                if response.data?.count != 0 {
                    self.offsetGif += 1
                    for data in response.data! {
                        self.gifArray.append("https://i.giphy.com/media/\(data.id)/giphy-preview.gif")
                    }
                }
                print(self.gifArray)
                DispatchQueue.main.async {
                    self.gifCollectionView.reloadData()
                }
            } else {
                print("No Results Found")
            }
        }
    }
    
    
    func searchGifImages(text: String) {
        GiphyCore.shared.search(text, media: .gif, offset: self.offsetGif, limit: 20, rating: GPHRatingType.ratedG, completionHandler: { (response, error) in
            if let response = response {
                if self.offsetGif == 0 {
                    self.gifArray.removeAll()
                }
                
                if response.data?.count != 0 {
                    self.offsetGif += 1
                    for data in response.data! {
                        self.gifArray.append("https://i.giphy.com/media/\(data.id)/giphy-preview.gif")
                    }
                }
                DispatchQueue.main.async {
                    self.gifCollectionView.reloadData()
                }
            } else {
                print("No Results Found")
            }
        })
    }
}
