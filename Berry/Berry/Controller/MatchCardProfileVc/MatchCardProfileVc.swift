//
//  MatchCardProfileVc.swift
//  Berry
//
//  Created by YASH on 15/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import NVActivityIndicatorView
import KRPullLoader

//import LNICoverFlowLayout

protocol UnMatchUserDelegate: class {
    func UnMatchUserDidFinish(userId:String)
}

class MatchCardProfileVc: UIViewController, KRPullLoadViewDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var collectionHeaderUserImages: UICollectionView!
    @IBOutlet weak var coverFlowLayout: LNICoverFlowLayout!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblActivityValue: UILabel!
    @IBOutlet weak var lblJob: UILabel!
    @IBOutlet weak var lblJobValue: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblAboutValue: UILabel!
    @IBOutlet weak var lblInstagramPhotos: UILabel!
    @IBOutlet weak var collectionViewInstagramPhotos: UICollectionView!
    @IBOutlet weak var constraintInstagramPhotosHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBottomLine: UIView!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnUnmatch: UIButton!    
    @IBOutlet weak var scrollView: UIScrollView!
    //MARK: - Variables
    var originalItemSize = CGSize.zero
    var originalCollectionViewSize = CGSize.zero
    var profileDetails : JSON = JSON()
    var arrayImageUser: [JSON] = [JSON]()
    var arrayInstagramPhotos: [JSON] = [JSON]()
    var oppUserId: String = ""
    let refreshView = KRPullLoadView()
    weak var delegate:UnMatchUserDelegate?
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        originalItemSize = coverFlowLayout.itemSize
        originalCollectionViewSize = collectionHeaderUserImages.bounds.size
        setupUI()
        self.getProfileDetails()
        collectionHeaderUserImages.register(UINib(nibName: "collectionUserImageCell", bundle: nil), forCellWithReuseIdentifier:"collectionUserImageCell")
        
        collectionViewInstagramPhotos.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"galleryImageCell")
        collectionViewInstagramPhotos.alwaysBounceVertical = true
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Profile_key"))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            self.collectionHeaderUserImages.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        scrollView.removePullLoadableView(type: .refresh)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        coverFlowLayout.invalidateLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        coverFlowLayout.itemSize = CGSize(width: collectionHeaderUserImages.bounds.size.width * originalItemSize.width / originalCollectionViewSize.width, height: collectionHeaderUserImages.bounds.size.height * originalItemSize.height / originalCollectionViewSize.height)
        
        setInitialValues()
        
        collectionHeaderUserImages.setNeedsLayout()
        collectionHeaderUserImages.layoutIfNeeded()
        collectionHeaderUserImages.reloadData()
        collectionViewInstagramPhotos.reloadData()
        
        constraintInstagramPhotosHeight.constant = collectionViewInstagramPhotos.contentSize.height
    }
    
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case .pulling(_, _):
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                self.getProfileDetails(progress: false)
                completionHandler()
            }
        }
    }
    
    func setInitialValues() {
        coverFlowLayout.maxCoverDegree = 0
        coverFlowLayout.coverDensity = 0
        coverFlowLayout.minCoverScale = 0.90
        coverFlowLayout.minCoverOpacity = 1.0
    }
    
    func setupUI() {
        lblName.textColor = UIColor.appThemePurpleColor
        lblName.font = themeFont(size: 17, fontname: .bold)
        
        [lblActivity,lblJob,lblAbout].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 14, fontname: .bold)
        }
        
        [lblActivityValue,lblJobValue,lblAboutValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 14, fontname: .light)
        }
        
        lblInstagramPhotos.textColor = UIColor.appThemeDarkGrayColor
        lblInstagramPhotos.font = themeFont(size: 17, fontname: .bold)
        
        lblActivity.text = getCommonString(key: "Activity_key")
        lblJob.text = getCommonString(key: "Job_key")
        lblAbout.text = getCommonString(key: "About_key")
        lblInstagramPhotos.text = getCommonString(key: "Instagram_Photos_key")
        self.scrollView.alwaysBounceVertical = true
        self.scrollView.bounces  = true
        self.refreshView.delegate = self
        self.scrollView.addPullLoadableView(refreshView, type: .refresh)
    }
    
    func getProfileDetails(progress:Bool = true) {
        let param = ["opp": userDetails.getString(key: .userid), "userid": self.oppUserId]
        ApiManager.shared.MakeGetAPI(name: PROFILE, params: param as [String : Any], progress: progress,vc: self, completionHandler: { (result, error) in
            if result != nil {
                let json = JSON(result!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    let myprofile = json.getDictionary(key: .myprofile)
                    self.profileDetails = myprofile
                    self.showDetails()
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.getProfileDetails()
//                AlertView(title: "Failed", message: error!)
            }
        })
    }
    
    func showDetails() {
        self.arrayImageUser.removeAll()
        self.arrayInstagramPhotos.removeAll()
    
        for image in profileDetails.getArray(key: .profile) {
            self.arrayImageUser.append(image)
        }
        self.collectionHeaderUserImages.reloadData()
        if profileDetails.getInt(key: .insta) == 1 {
            for image in profileDetails.getArray(key: .insta_links) {
                self.arrayInstagramPhotos.append(image)
            }
        }
        else {
            self.lblInstagramPhotos.isHidden = true
            self.viewBottomLine.isHidden = true
        }        
        
        self.collectionViewInstagramPhotos.reloadData {
            self.constraintInstagramPhotosHeight.constant = self.collectionViewInstagramPhotos.contentSize.height
        }

        self.lblName.text = profileDetails.getString(key: .name).capitalized
        self.lblAboutValue.text = profileDetails.getString(key: .about)
        self.lblActivityValue.text = profileDetails.getString(key: .hobbies)
        self.lblJobValue.text = profileDetails.getString(key: .job)       
        if profileDetails.getBool(key: .match) {
            
            let icon1 = UIImage(named: "ic_unmatched_round_gray")?.withRenderingMode(.alwaysOriginal)
            let iconSize1 = CGRect(origin: CGPoint.zero, size: CGSize(width: 25, height: 25))
            let iconButton1 = UIButton(frame: iconSize1)
            iconButton1.setBackgroundImage(icon1, for: .normal)
            
            let btnUnmatch = UIBarButtonItem(customView: iconButton1)
            iconButton1.addTarget(self, action: #selector(btnUnmatchTapped(_:)), for: .touchUpInside)
            
            
            let icon2 = UIImage(named: "ic_report_round_gray")?.withRenderingMode(.alwaysOriginal)
            let iconSize2 = CGRect(origin: CGPoint.zero, size: CGSize(width: 25, height: 25))
            let iconButton2 = UIButton(frame: iconSize2)
            iconButton2.setBackgroundImage(icon2, for: .normal)
            
            let btnReport = UIBarButtonItem(customView: iconButton2)
            iconButton2.addTarget(self, action: #selector(btnReportTapped(_:)), for: .touchUpInside)
            self.navigationItem.rightBarButtonItems = [btnReport, btnUnmatch]
        }
        else {
            let icon2 = UIImage(named: "ic_report_round_gray")?.withRenderingMode(.alwaysOriginal)
            let iconSize2 = CGRect(origin: CGPoint.zero, size: CGSize(width: 25, height: 25))
            let iconButton2 = UIButton(frame: iconSize2)
            iconButton2.setBackgroundImage(icon2, for: .normal)
            
            let btnReport = UIBarButtonItem(customView: iconButton2)
            iconButton2.addTarget(self, action: #selector(btnReportTapped(_:)), for: .touchUpInside)
            self.navigationItem.rightBarButtonItem = btnReport
        }
    }
    
    @IBAction func btnReportTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ReportSpamPopupVc") as! ReportSpamPopupVc
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.oppUserId = oppUserId
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnUnmatchTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: getCommonString(key: "Berry_key"), message: getCommonString(key: "unmatchUser_msg_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            //            Yes
            self.unmatchedWithOppositeUser(opp: self.oppUserId)
        }
        
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            //            No
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func unmatchedWithOppositeUser(opp: String) {
        self.showProgress(vc: self)
        let param = ["userid": userDetails.getString(key: .userid),"opp": opp] as [String : Any]
        ApiManager.shared.MakePostAPI(name: UNMATCH, params: param, progress: false, vc: self) { (result, error) in
            DispatchQueue.main.async {
                self.stopAnimating()
            }
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Success", message: msg)
                    self.delegate?.UnMatchUserDidFinish(userId: self.oppUserId)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.unmatchedWithOppositeUser(opp: opp)
            }
        }
    }
    
    func showProgress(vc: UIViewController) {
        DispatchQueue.main.async {
            let LoaderString:String = "Loading..."
            let LoaderType:Int = 32
            let LoaderSize = CGSize(width: 30, height: 30)
            vc.startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        }
    }
}


//MARK: - CollectionView delegate and DataSource
extension MatchCardProfileVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionHeaderUserImages {
            return arrayImageUser.count
        }
        else {
            return arrayInstagramPhotos.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionHeaderUserImages {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionUserImageCell", for: indexPath) as! collectionUserImageCell
            cell.btnAddPhoto.isHidden = true
            cell.img.contentMode = .scaleAspectFill
            cell.img.backgroundColor = UIColor.clear
            cell.img.sd_setImage(with: URL(string: arrayImageUser[indexPath.row].string!), placeholderImage: UIImage(named: "user_placeholder_big"), completed: nil)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryImageCell", for: indexPath) as! GalleryCollectionViewCell
            cell.imgvwGalleryImage.sd_setImage(with: URL(string: arrayInstagramPhotos[indexPath.row].string!), placeholderImage: UIImage(named: "user_placeholder_big"), completed: nil)

            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionHeaderUserImages {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ShowMultiImagesVc") as! ShowMultiImagesVc
            obj.selectedController = .forMatchCardShowPhotoHeader
            obj.arrSocialMediaImagesMutable = self.arrayImageUser
            obj.currentImageIndex = indexPath.row
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ShowMultiImagesVc") as! ShowMultiImagesVc
            obj.selectedController = .forMatchCardShowPhotoHeader
            obj.arrSocialMediaImagesMutable = self.arrayInstagramPhotos
            obj.currentImageIndex = indexPath.row
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionHeaderUserImages {
            return CGSize()
        }
        else {
            let width = (collectionView.frame.size.width/3)
            // let height = (collectionView.frame.size.height/4)            
            return CGSize(width: width , height:width )
        }
    }
}
