//
//  Basicstuff.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
//import MaterialComponents
import NVActivityIndicatorView
//import AlamofireSwiftyJSON
import SwiftyJSON
//import Alamofire


struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH < 568
    static let IS_IPHONE_5_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH <= 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}

struct GlobalVariables {
    
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let cardNumberLimit = 16
    
    static let googleMapKey = "AIzaSyBOOpOc8SNjr5x5WWgZIhz5g-lINaelIDI" //"AIzaSyClhaz56TaxMJpvmSsFqHUGXOXKXthJIZE" //mjtest
    
    static let instagramClientKey = "2efce2404e6340a7b0652ce663c254f9" //"4cdccd2719734e648a1c7b859f95eb5e"
    static let instagramRedirectURL = "http://berrydates.com/" //"http://www.dignizant.com/"
    
    
    //MARK:- API flag
    
    static var localTimeZoneName: String { return TimeZone.current.identifier }
    static let deviceType = "1"
    static let strLang = "0"
    static let strSuccessResponse = "1"
    static let strAccessDenied = "-1"
    //MARK: - PhoneLength
    
    static let phoneNumberLimit = 15//10
}


let appdelgate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard


//MARK: - User Location

var userCurrentLocation:CLLocation?

//MARK: - Setup mapping

let StringFilePath = Bundle.main.path(forResource: "Language", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)


func getCommonString(key:String) -> String {
    return dictStrings?.object(forKey: key) as? String ?? ""
}

//MARK: - Storagae


//func getUserDetail(_ forKey: String) -> String{
//    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
//    let data = JSON(userDetail)
//    return data[forKey].stringValue
//}



//MARK: - navigate with sidemenu

//func navigateLGSideEffect(obj : UIViewController,forSideMenu:SideMenuSetup)
//{
//    let navigationController = NavigationController(rootViewController: obj)
//    var mainViewController = MainViewController()
//
//    mainViewController.forSideMenu = forSideMenu
//
//    mainViewController.rootViewController = navigationController
//    mainViewController.setup(type: 0)
//
//    let window = UIApplication.shared.delegate!.window!!
//    window.rootViewController = mainViewController
//}



//MARK: - Set Toaster

//func makeToast(strMessage : String){
//
//    let messageSnack = MDCSnackbarMessage()
//    messageSnack.text = strMessage
//    MDCSnackbarManager.show(messageSnack)
//
//}
//

//extension UIViewController : NVActivityIndicatorViewable


extension UIViewController : NVActivityIndicatorViewable {
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - Validation email
    
    func isValidEmail(emailAddressString:String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    // MARK: -  For Loader NVActivityIndicatorView Process
    func showLoader() {
        DispatchQueue.main.async {
            let LoaderString:String = "Loading..."
            let LoaderType:Int = 32
            let LoaderSize = CGSize(width: 30, height: 30)
            
            self.startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        }
    }

    func hideLoader() {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
    
    func setupNavigationbarTitle(titleText:String) {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationItem.hidesBackButton = true
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.white
        HeaderView.font = themeFont(size: 16, fontname: .light)

        self.navigationController?.navigationBar.barTintColor = UIColor.appThemePurpleColor

        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func setupNavigationbar(titleText:String) {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
       
        self.navigationItem.hidesBackButton = true
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header") , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = UIColor.appThemePurpleColor
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.white
        HeaderView.font = themeFont(size: 21, fontname: .light)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    
   
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func backButtonRootTapped() {
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RootNav") as! UINavigationController
//        let rearNavigation = UINavigationController(rootViewController: vc)
        AppDelegate.shared.window?.rootViewController = vc
    }

    func setupNavigationbarwithBackButton(titleText:String, isRoot: Bool = false) {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header") , style: .plain, target: self, action: isRoot ? #selector(backButtonRootTapped) : #selector(backButtonTapped))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 16, fontname: .light)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemePurpleColor
        
        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }

    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField) {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemePurpleColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(textfield:UITextField) {
        self.view.endEditing(true)
    }

    
    //MARK: - Date and Time Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = OrignalFormatter
        guard let convertedDate = dateformatter.date(from: strDate) else {
            return ""
        }
        dateformatter.dateFormat = YouWantFormatter
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
}


 func getCountryPhonceCode(_ country : String) -> JSON
 {
    let defaultCountry = JSON.init(parseJSON:"{\"name\": \"United States\",\"dial_code\": \"+1\",\"code\": \"US\"}")
    if let path = Bundle.main.path(forResource: "CountryCodes", ofType: "json") {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            print(JSON(jsonResult))
            let countryList = JSON(jsonResult)
            let record = countryList.arrayValue.filter { $0["code"].string == country }.first
            return record ?? defaultCountry
        } catch {
            // handle error
            return defaultCountry
        }
    }
    return defaultCountry
}


