//
//  ImageCollectionViewCell.swift
//  testingTinderSwipe
//
//  Created by om on 10/13/18.
//  Copyright © 2018 Nicky. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet var lblUserName : UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var imgDistance: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSuperLike: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        let attributedText = NSMutableAttributedString(string: NAMES[Int(arc4random_uniform(UInt32(NAMES.count)))] + " | 22", attributes: [.foregroundColor: UIColor.white,.font:UIFont.boldSystemFont(ofSize: 23)])
//        
//        attributedText.append(NSAttributedString(string: "\nFriends Gathering", attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 14)]))
//        
//        attributedText.append(NSAttributedString(string: "\nB.Tech Student,IIT", attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 14)]))
//        
//        lblUserName.attributedText = attributedText
        lblUserName.numberOfLines = 4
        
        lblDistance.textColor = UIColor.white
        lblDistance.font = UIFont.systemFont(ofSize: 18)
        
        
        /*let distanceAttributedText = NSMutableAttributedString()
        
        var attachment: NSTextAttachment = NSTextAttachment()
        //        attachment.image = UIImage(named: imageName)?.resizableImage(withCapInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), resizingMode: .stretch)
        
        attachment.image = UIImage(named:"ic_pin_white")
        
        let imageOffsetY:CGFloat = -2.0;
        
        attachment.bounds = CGRect(x: 0, y: imageOffsetY, width: attachment.image!.size.width, height: attachment.image!.size.height)
        
        //        attachment.image = resizeImage(image: UIImage(named: "ic_pin_white")!, targetSize: CGSize(width:15,height:18))
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        
        let strLabelText = NSAttributedString(string: " 5 km away", attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 18)])
        
        let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
        mutableAttachmentString.append(strLabelText)
        
        distanceAttributedText.append(mutableAttachmentString)
        
        lblDistance.attributedText = distanceAttributedText
       */
    }

}


