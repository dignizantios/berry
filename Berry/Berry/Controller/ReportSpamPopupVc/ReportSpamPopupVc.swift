//
//  ReportSpamPopupVc.swift
//  Berry
//
//  Created by YASH on 01/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class ReportSpamPopupVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var lblReportSpam: UILabel!
    @IBOutlet weak var lblPickaSubject: UILabel!
    @IBOutlet weak var txtPickaSubject: CustomTextField!
    @IBOutlet weak var lblEnterDescription: UILabel!
    @IBOutlet weak var txtVwDescription: CustomTextview!
    @IBOutlet weak var lblPlaceHolderEnterHere: UILabel!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - Variables
    var subjectDD = DropDown()
    var items : [String] = []
    var oppUserId : String = ""
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        items = ["Inappropriate messages",
                 "Inappropriate photos",
                 "Inappropriate offline behaviour",
                 "Feels like spam",
                 "other"]
        self.subjectDD.dataSource = self.items
    }
    
    override func viewDidLayoutSubviews() {
        self.configDD(dropdown: self.subjectDD, sender: self.txtPickaSubject)
        selectionIndex()
    }
    
    func setupUI() {
        lblReportSpam.textColor = UIColor.appThemeLightGrayColor
        lblReportSpam.font = themeFont(size: 18, fontname: .medium)
        
        [lblPickaSubject,lblEnterDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        txtPickaSubject.setupCustomTextField()
        txtPickaSubject.delegate = self
        
        txtVwDescription.font = themeFont(size: 16, fontname: .regular)
        txtVwDescription.textColor = UIColor.appThemeDarkGrayColor
        txtVwDescription.delegate = self
        
        lblPlaceHolderEnterHere.font = themeFont(size: 15, fontname: .light)
        lblPlaceHolderEnterHere.textColor = UIColor.appThemeLightGrayColor
        
        btnSubmit.setupShadowThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "SUBMIT_key"), for: .normal)
        
        lblPlaceHolderEnterHere.text = getCommonString(key: "Enter_here_(Optional)_key")
        
        lblReportSpam.text = getCommonString(key: "Report_spam_key")
        
        lblPickaSubject.text = getCommonString(key: "Pick_a_subject_key")
        txtPickaSubject.placeholder = getCommonString(key: "Pick_a_subject_key")
        
        lblEnterDescription.text = getCommonString(key: "Enter_description_key")
    }
    
    func validation() {
        if self.txtPickaSubject.text! == "" {
            AlertView(title: "Failed", message: "Please select subject")
        }
        else {
            saveReport()
        }
    }
    
    func saveReport() {
        let param = ["userid":userDetails.getString(key: .userid),"opp":self.oppUserId,"subject":self.txtPickaSubject.text!.toTrim(), "description":self.txtVwDescription.text!.toTrim()]
        ApiManager.shared.MakePostAPI(name: SEND_REPORT, params: param, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                    self.dismiss(animated: true, completion: nil)
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.saveReport()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
}

//MARK: - IBAction method

extension ReportSpamPopupVc {
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.validation()
    }
    
    //DropDown Setup
    func configDD(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.appThemeDarkGrayColor
        dropdown.selectionBackgroundColor = UIColor.clear
    }
    
    func selectionIndex() {
        self.subjectDD.selectionAction = { (index, item) in
            self.txtPickaSubject.text = item
            self.view.endEditing(true)
            //            self.strSelectedQuestionID = (self.jsonArrayQuestionList[index]["security_id"]).stringValue
            self.subjectDD.hide()
        }
    }
}

//MARK: - TextField Delegate
extension ReportSpamPopupVc : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtPickaSubject {
            subjectDD.show()
            return false
        }
        return true
    }
}

extension ReportSpamPopupVc : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            self.lblPlaceHolderEnterHere.isHidden = false
        }
        else {
            self.lblPlaceHolderEnterHere.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            txtVwDescription.resignFirstResponder()
            return false
        }
        return true
    }
}

