//
//  PhotoPickerVc.swift
//  Berry
//
//  Created by YASH on 17/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Photos
import FBSDKLoginKit
import FBSDKCoreKit
import SwiftyJSON
import SDWebImage
import InstagramLogin
//import YangMingShan


protocol selectedImageDelegate {
    func setImagesInCollection(arrayOfImages : [UIImage])
    func setImagesWithUrlInCollection(arrayOfImages : NSMutableArray)
}

enum checkParentControllerAddOrRegistration {
    case forRegistration
    case addPhotoProfile
}

class PhotoPickerVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var imgFacebook: UIImageView!
    @IBOutlet weak var lblFacebookImages: UILabel!
    @IBOutlet weak var lblFBPhotosNumber: UILabel!
    @IBOutlet weak var imgInstagram: UIImageView!
    @IBOutlet weak var lblInstagramImages: UILabel!
    @IBOutlet weak var lblInstaPhotosNumber: UILabel!
    @IBOutlet weak var imgPhotos: UIImageView!
    @IBOutlet weak var lblPhotos: UILabel!
    @IBOutlet weak var lblPhotosNumber: UILabel!
    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnInsta: UIButton!
    
    //MARK: - Variables
    var arrayImages : [JSON] = [] // Convert all url into image for collection use
    var arrSocialMediaImagesMutable = NSMutableArray()
    var delegateImage : selectedImageDelegate?
    var selectedController = checkParentControllerAddOrRegistration.forRegistration
    var intRemainingPhotoCount = Int()
    var arrayOfURLimagesForFB = [URL]()
    var arrayOfURLimagesForInsta = [URL]()
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        checkPhotoLibraryPermission()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrSocialMediaImagesMutable = []
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Select_Photos_key"))
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            
            let fetchOptions = PHFetchOptions()
            let allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            
            self.lblPhotosNumber.text = "  \(allPhotos.count) Photos"
            imgPhotos.image = getUIImage(asset: allPhotos.lastObject ?? UIImage() as! PHAsset)
            
        case .denied, .restricted , .notDetermined  :
            break
            //handle denied status case .notDetermined: // ask for permissions PHPhotoLibrary.requestAuthorization { status in switch status { case .authorized: // as above case .denied, .restricted: // as above case .notDetermined: // won't happen but still } } } }
        }
    }
    
    func getUIImage(asset: PHAsset) -> UIImage? {
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            if let data = data {
                img = UIImage(data: data)
            }
        }
        return img
    }
    
    func setupUI() {
        [lblFacebookImages,lblInstagramImages,lblPhotos].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        [lblFBPhotosNumber,lblInstaPhotosNumber,lblPhotosNumber].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 14, fontname: .light )
        }
        
        lblFacebookImages.text = getCommonString(key: "Facebook_images_key")
        lblInstagramImages.text = getCommonString(key: "Instagram_images_key")
        lblPhotos.text = getCommonString(key: "Photos_key")
        
        lblPhotosNumber.text = getCommonString(key: "Loading..._key")
        
        if (Defaults.value(forKey: "facebookID") as? String) != nil {
            //Alerady login With FB
            btnFB.isUserInteractionEnabled = false
            lblFBPhotosNumber.text = getCommonString(key: "Loading..._key")
            returnUserData()
        }
        else {
            btnFB.isUserInteractionEnabled = true
            lblFBPhotosNumber.text = getCommonString(key: "Connect_to_get_Photos_key")
        }
        
        if (Defaults.value(forKey: "InstagramID") as? String) != nil {
            //            Alerady login With Insta
            btnInsta.isUserInteractionEnabled = false
            lblInstaPhotosNumber.text = getCommonString(key: "Loading..._key")
            let authToken = Defaults.value(forKey: "authoToken") as! String
            getAllImageFromInstagram(authToken: authToken)
        }
        else {
            btnInsta.isUserInteractionEnabled = true
            lblInstaPhotosNumber.text = getCommonString(key: "Connect_to_get_Photos_key")
        }
    }
}

extension PhotoPickerVc {
    //MARK: - IBAction method
    @IBAction func btnFacebookImagesTapped(_ sender: UIButton) {
        if (Defaults.value(forKey: "facebookID") as? String) != nil {
            if arrayOfURLimagesForFB.count == 0 {
                returnUserData()
            }
            else {
                fbRedirect()
            }
        }
        else {
            self.checkUserLoginWithFB()
        }
    }
    
    @IBAction func btnInstagramImagesTapped(_ sender: UIButton) {
        if (Defaults.value(forKey: "InstagramID") as? String) != nil {
            if arrayOfURLimagesForInsta.count == 0 {
                let authToken = Defaults.value(forKey: "authoToken") as! String
                getAllImageFromInstagram(authToken: authToken)
            }
            else {
                instaRedirect()
            }
        }
        else {
            loginWithInstagram()
        }
    }
    
    @IBAction func btnPhotsTappedAction(_ sender: UIButton) {
        openGallary()
    }
}

//MARK: - FB process
extension PhotoPickerVc {
    func checkUserLoginWithFB() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: { (result, error) -> Void in
            if (error == nil) {
                if (result?.grantedPermissions != nil) {
                    self.showLoader()
                    self.loginWIthFBData()
                }
                else {
                    self.hideLoader()
                }
            }
            else  {
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
            }
        })
    }
    
    func loginWIthFBData() {
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    let dataFB = JSON(result!)
                    let fbID = dataFB["id"].stringValue
                    Defaults.setValue(fbID, forKey: "facebookID")
                    Defaults.synchronize()
                    self.returnUserData()
                }
            })
        }
    }
    
    func returnUserData() {
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me/photos", parameters: ["fields": "images","type" : "uploaded","limit":"100000"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    self.hideLoader()
                    let dataFB = JSON(result!)
                    let data = dataFB["data"].arrayValue
                    
                    for i in 0..<data.count {
                        let arrayFBImages = data[i]["images"].arrayValue
                        self.arrayOfURLimagesForFB.append(arrayFBImages[0]["source"].url!)
                    }
                    
                    self.btnFB.isUserInteractionEnabled = true
                    self.lblFBPhotosNumber.text = "  \(self.arrayOfURLimagesForFB.count) Photos"
                    let imgURL = self.arrayOfURLimagesForFB.first
                    
                    self.imgFacebook.sd_setShowActivityIndicatorView(true)
                    self.imgFacebook.sd_setIndicatorStyle(.gray)
                    self.imgFacebook.sd_setImage(with: imgURL, placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
                }
            })
        }
    }
    
    func fbRedirect() {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "FBAndInstaPhotoListVc") as! FBAndInstaPhotoListVc
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: obj)
        obj.numberOfPhotoToSelect = intRemainingPhotoCount
        obj.delegateSocialMediaImage = self
        obj.arrayURL = self.arrayOfURLimagesForFB
        obj.modalPresentationStyle = .overFullScreen
        self.present(navEditorViewController, animated: true, completion: nil)
    }
    
    func instaRedirect() {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "FBAndInstaPhotoListVc") as! FBAndInstaPhotoListVc
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: obj)
        obj.numberOfPhotoToSelect = intRemainingPhotoCount
        obj.delegateSocialMediaImage = self
        obj.arrayURL = self.arrayOfURLimagesForInsta
        obj.modalPresentationStyle = .overFullScreen
        self.present(navEditorViewController, animated: true, completion: nil)
    }
}

//MARK: - Instagram Method
extension PhotoPickerVc: InstagramLoginViewControllerDelegate {
    func loginWithInstagram() {
//        if let cookies = HTTPCookieStorage.shared.cookies {
//            for cookie in cookies {
//                NSLog("\(cookie)")
//            }
//        }
//        
//        let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
//        for cookie in cookieJar.cookies! as [HTTPCookie]{
//            NSLog("cookie.domain = %@", cookie.domain)
//            cookieJar.deleteCookie(cookie)
//        }
//        
//        URLCache.shared.removeAllCachedResponses()
//        URLCache.shared.diskCapacity = 0
//        URLCache.shared.memoryCapacity = 0
        
        let vc = InstagramLoginViewController(clientId: GlobalVariables.instagramClientKey, redirectUri: GlobalVariables.instagramRedirectURL)
        // Login permissions (https://www.instagram.com/developer/authorization/)
        vc.scopes = [.basic] // basic by default
        // ViewController title, website title by default
        vc.title = "Instagram" // By default, the web title is displayed
        vc.delegate = self
        // Progress view tint color
        vc.progressViewTintColor = UIColor.green // #E1306C by default
        
        let nav = UINavigationController(rootViewController: vc)
        let button1 = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(instaCloseClicked))
        vc.navigationItem.rightBarButtonItem  = button1
        nav.modalPresentationStyle = .overFullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func instaCloseClicked() {
        dismiss(animated: true, completion: nil)
    }
    
    func instagramLoginDidFinish(accessToken: String?, error: InstagramError?) {
        
        guard let accessToken = accessToken else {
            print("Failed login: " + error!.localizedDescription)
            return
        }
        handleAuth(authToken: accessToken)
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleAuth(authToken: String)  {
        jsondata(authToken : authToken)
    }
    
    func jsondata(authToken : String) {
        let url = "https://api.instagram.com/v1/users/self/?access_token=\(authToken)"
        let request = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) { (response, data, error) in
            if error == nil{
                do{
                    let jasonobject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    
                    if let data = jasonobject.object(forKey: "data") as? NSDictionary {
                        var dictLinkedData = JSON(data)
                        
                        let instaID = dictLinkedData["id"].stringValue
                        Defaults.setValue(instaID, forKey: "InstagramID")
                        
                        let authoToken = authToken
                        Defaults.setValue(authoToken, forKey: "authoToken")
                        
                        Defaults.synchronize()
                        self.getAllImageFromInstagram(authToken: authoToken)
                    }
                }catch{
                }
            }
        }
    }
    
    func getAllImageFromInstagram(authToken : String) {
        
        self.arrayOfURLimagesForInsta.removeAll()
        let url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=\(authToken)"
        print(url)
        let request = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) { (response, data, error) in
            
            if error == nil{
                do{
                    let jasonobject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    print(jasonobject)
                    self.arrayOfURLimagesForInsta.removeAll()
                    if let data = jasonobject.object(forKey: "data") as? NSArray {
                        var dictLinkedData = JSON(data)
                        let instaID = dictLinkedData["id"].stringValue
                        Defaults.setValue(instaID, forKey: "InstagramID")
                        Defaults.synchronize()
                        
//                        for i in 0..<dictLinkedData.count {
//                            let dict = dictLinkedData[i]["images"].dictionaryValue
//                            if let dictResolution = dict["standard_resolution"]?.dictionaryValue {
//                                let imgURL = dictResolution["url"]?.url
//                                self.arrayOfURLimagesForInsta.append(imgURL!)
//                            }
//                        }
                        
                        for i in 0..<dictLinkedData.count {
                            let json = JSON(dictLinkedData[i])
                            let carousel_media = json.getArray(key: .carousel_media)
                            if carousel_media.isEmpty {
                                let dict = dictLinkedData[i]["images"].dictionaryValue
                                if let dictResolution = dict["standard_resolution"]?.dictionaryValue {
                                    let imgURL = dictResolution["url"]?.url
                                    self.arrayOfURLimagesForInsta.append(imgURL!)
                                }
                            }
                            else {
                                for j in 0..<carousel_media.count {
                                    let dict = carousel_media[j]["images"].dictionaryValue
                                    if let dictResolution = dict["standard_resolution"]?.dictionaryValue {
                                        let imgURL = dictResolution["url"]?.url
                                        self.arrayOfURLimagesForInsta.append(imgURL!)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.btnInsta.isUserInteractionEnabled = true
                            self.lblInstaPhotosNumber.text = "  \(self.arrayOfURLimagesForInsta.count) Photos"
                        }
                        
                        let imgURL = self.arrayOfURLimagesForInsta.first
                        
                        self.imgInstagram.sd_setShowActivityIndicatorView(true)
                        self.imgInstagram.sd_setIndicatorStyle(.gray)
                        self.imgInstagram.sd_setImage(with: imgURL, placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
                        
                        let json = JSON(jasonobject)
                        if let dict: JSON = json.getDictionary(key: .pagination) {
                            if !dict.isEmpty {
                                let next_url = dict.getString(key: .next_url)
                                self.getNextImageFromInstagram(url: next_url)
                            }
                        }
                    }
                }catch{
                }
            }
        }
    }
    
    func getNextImageFromInstagram(url : String) {
        let request = NSMutableURLRequest()
        request.url = URL(string: url)
        request.httpMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue()) { (response, data, error) in
            
            if error == nil{
                do{
                    let jasonobject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    print(jasonobject)
                    if let data = jasonobject.object(forKey: "data") as? NSArray {
                        var dictLinkedData = JSON(data)
                        let instaID = dictLinkedData["id"].stringValue
                        Defaults.setValue(instaID, forKey: "InstagramID")
                        Defaults.synchronize()
                        
//                        for i in 0..<dictLinkedData.count {
//                            let dict = dictLinkedData[i]["images"].dictionaryValue
//                            if let dictResolution = dict["standard_resolution"]?.dictionaryValue {
//                                let imgURL = dictResolution["url"]?.url
//                                self.arrayOfURLimagesForInsta.append(imgURL!)
//                            }
//                        }
                        
                        for i in 0..<dictLinkedData.count {
                            let json = JSON(dictLinkedData[i])
                            let carousel_media = json.getArray(key: .carousel_media)
                            if carousel_media.isEmpty {
                                let dict = dictLinkedData[i]["images"].dictionaryValue
                                if let dictResolution = dict["standard_resolution"]?.dictionaryValue {
                                    let imgURL = dictResolution["url"]?.url
                                    self.arrayOfURLimagesForInsta.append(imgURL!)
                                }
                            }
                            else {
                                for j in 0..<carousel_media.count {
                                    let dict = carousel_media[j]["images"].dictionaryValue
                                    if let dictResolution = dict["standard_resolution"]?.dictionaryValue {
                                        let imgURL = dictResolution["url"]?.url
                                        self.arrayOfURLimagesForInsta.append(imgURL!)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.btnInsta.isUserInteractionEnabled = true
                            self.lblInstaPhotosNumber.text = "  \(self.arrayOfURLimagesForInsta.count) Photos"
                        }
                        
                        let imgURL = self.arrayOfURLimagesForInsta.first
                        
                        self.imgInstagram.sd_setShowActivityIndicatorView(true)
                        self.imgInstagram.sd_setIndicatorStyle(.gray)
                        self.imgInstagram.sd_setImage(with: imgURL, placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
                    }
                    
                    let json = JSON(jasonobject)
                    if let dict: JSON = json.getDictionary(key: .pagination) {
                        if !dict.isEmpty {
                            let next_url = dict.getString(key: .next_url)
                            self.getNextImageFromInstagram(url: next_url)
                        }
                    }
                }catch{
                }
            }
        }
    }
}




//MARK:- YangMingShan Library

extension PhotoPickerVc : YMSPhotoPickerViewControllerDelegate
{
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        picker.present(alertController, animated: true, completion: nil)
    }
    
    
    func openGallary() {
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = UInt(intRemainingPhotoCount)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = UIColor.appThemePurpleColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = UIColor.appThemePurpleColor
        pickerViewController.theme.cameraVeilColor = UIColor.appThemePurpleColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        pickerViewController.delegate = self
        pickerViewController.modalPresentationStyle = .overFullScreen
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        //        if ((6 - intRemainingPhotoCount) + photoAssets.count) < 3 {
        //            AlertView(title: "Alert", message: "Please select minimam 3 images")
        //            return
        //        }
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            for asset: PHAsset in photoAssets
            {
                imageManager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
                    if image != nil {
                        let dict: NSDictionary = ["imageURL" : image!,"isImage": "1"]
                        self.arrSocialMediaImagesMutable.add(dict)
                    }
                })
            }
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CropMultiImagesVc") as! CropMultiImagesVc
            obj.delegateCropImage = self
            obj.arrSocialMediaImagesMutable = self.arrSocialMediaImagesMutable
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}

//MARK: - Redirect back delegate
extension PhotoPickerVc : CropImagesDelegate {
    func setCropImagesWithUrlInCollection(arrayOfImages: NSMutableArray) {
        self.delegateImage?.setImagesWithUrlInCollection(arrayOfImages: arrayOfImages)
        if selectedController == .forRegistration {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is SignUpVc {
                    self.navigationController!.popToViewController(aViewController, animated: false)                }
            }
        }
        else if selectedController == .addPhotoProfile {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is MainVc {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    func setCropImagesInCollection(arrayOfImages: [UIImage]) {
        self.delegateImage?.setImagesInCollection(arrayOfImages: arrayOfImages)
        if selectedController == .forRegistration {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is SignUpVc {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
        else if selectedController == .addPhotoProfile {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is MainVc {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
}

//MARK: - Social Media Delegate
extension PhotoPickerVc: redirectAfterSocialMediaPhotoSelection {
    func navigationToController(arrayOfSelectedImageURL: [URL]) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CropMultiImagesVc") as! CropMultiImagesVc
        obj.delegateCropImage = self
        obj.arrSelectedImageURLFromSocialMedia = arrayOfSelectedImageURL
        obj.selectedController = .forAddPhotoFromSocialMedia
        self.navigationController?.pushViewController(obj, animated: true)
    }
}


