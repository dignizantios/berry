//
//  PopupDeleteAccountVC.swift
//  Berry
//
//  Created by Anil on 08/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class PopupConfirmationDeleteAccountVC: UIViewController {

    @IBOutlet weak var viewBerryAndSomeOneElse: UIView!
    @IBOutlet weak var btnShareBerry: UIButton!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    
    var type:DeleteAccount = DeleteAccount.None
    var handlerDeleteAccount:(_ isDelete:Bool) -> Void = {_ in}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        
        btnShareBerry.setTitle("GO BACK TO BERRY", for: .normal)
        btnShareBerry.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        btnShareBerry.backgroundColor = UIColor.appThemePurpleColor
        btnShareBerry.setTitleColor(UIColor.white, for: .normal)
        
        btnDeleteAccount.setTitle("DELETE ACCOUNT", for: .normal)
        btnDeleteAccount.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        btnDeleteAccount.setTitleColor(UIColor.white, for: .normal)
        btnDeleteAccount.backgroundColor = UIColor.appThemeRedColor
    }

    func validateDeleteAccount() {
        var reason = ""
        if type == DeleteAccount.FoundOnBerry {
            reason = getCommonString(key: "Found_someone_onBerry_key")
        } else if type == DeleteAccount.FoundSomeoneElse {
            reason = getCommonString(key: "Found_someone_elsewhere_key")
        } else if type == DeleteAccount.DonotLikeBerry {
            reason = getCommonString(key: "Dont_like_Berry_key")
        } else if type == DeleteAccount.Other {
            reason = getCommonString(key: "Other_key")
        }
        
        deleteAccount(strReason: reason, strComment: "")
    }
    
    // MARK:- IBAction    
    @IBAction func btnBackBerry(_ sender: Any) {
        self.view.endEditing(true)
        self.handlerDeleteAccount(false)
    }
    
    @IBAction func btnDeleteAccount(_ sender: Any) {
        self.view.endEditing(true)
        validateDeleteAccount()
    }
}

//MARK:- APi Call
extension PopupConfirmationDeleteAccountVC {
    func deleteAccount(strReason:String, strComment:String) {
        let param = ["userid": userDetails.getString(key: .userid),"reason":strReason,"comment":strComment]
        print("param:=",param)
        ApiManager.shared.MakePostAPI(name: DELETE_ACCOUNT, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
                    deletJSON(key: USER_DETAILS_KEY)
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Success", message: msg)
                    self.handlerDeleteAccount(true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.deleteAccount(strReason: strReason, strComment: strComment)
            }
        }
    }
}
