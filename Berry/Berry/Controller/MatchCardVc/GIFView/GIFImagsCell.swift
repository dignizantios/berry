//
//  GIFImagsCell.swift
//  Berry
//
//  Created by Anil on 25/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class GIFImagsCell: UICollectionViewCell {
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var imgGIF: UIImageView!
}
