//
//  CropMultiImagesVc.swift
//  Berry
//
//  Created by YASH on 19/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import TOCropViewController
import SwiftyJSON

class ShowMultiImagesVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var collectionSelectedImages: UICollectionView!
    @IBOutlet weak var pageControlBottom: UIPageControl!  
    
    //MARK: - Variables
    var arrSelectedImageURLFromSocialMedia = [URL]() // Social media images URL
    var arrSocialMediaImages : [JSON] = []
    var arrSocialMediaImagesMutable = [JSON]()
    var arrInstaImages = [String]()
    var isInstaImages: Bool = false

    var delegateCropImage : CropImagesDelegate?
    var currentImageIndex = 0
    var selectedController = checkParentControllerForCropImagesVc.forAddPhotoFromDevicePhotos
    var userId : String = ""
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSelectedImages.register(UINib(nibName: "cropMultiImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "cropMultiImageCollectionCell")
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupUI() {
        if selectedController == .forAddPhotoFromDevicePhotos {
            
        }
        else if selectedController == .forProfileShowPhoto {
            if(arrSocialMediaImagesMutable.count > 0) {
                collectionSelectedImages.reloadData()
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
                pageControlBottom.currentPage = currentImageIndex
                collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        else if selectedController == .forMatchCardShowPhotoHeader {
            collectionSelectedImages.reloadData()
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
            
            pageControlBottom.currentPage = currentImageIndex
            collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
        else if selectedController == .forRegistration {
            if(arrSocialMediaImagesMutable.count > 0) {
                collectionSelectedImages.reloadData()
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
                pageControlBottom.currentPage = currentImageIndex
                collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        else if selectedController == .forMatchCardShowInstagramPhotos {
            pageControlBottom.isHidden = true
            collectionSelectedImages.reloadData()
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
            collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
        
    }
}


extension ShowMultiImagesVc: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControlBottom.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}

extension ShowMultiImagesVc: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedController == .forAddPhotoFromSocialMedia {
            self.pageControlBottom.numberOfPages = arrSocialMediaImagesMutable.count
            return arrSocialMediaImagesMutable.count
        }
        else {
            self.pageControlBottom.numberOfPages = arrSocialMediaImagesMutable.count
            return arrSocialMediaImagesMutable.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cropMultiImageCollectionCell", for: indexPath) as! cropMultiImageCollectionCell
        
        let image = self.arrSocialMediaImagesMutable[indexPath.row].string
        cell.imgSelectedPhotos!.sd_setImage(with: URL(string: image!), placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        cell.imgSelectedPhotos.isUserInteractionEnabled = true
        cell.imgSelectedPhotos.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth : CGFloat = 0.0
        var cellHeight : CGFloat = 0.0
        
        cellWidth  = collectionView.frame.size.width
        cellHeight  = collectionView.frame.size.height
        return CGSize(width: cellWidth , height:CGFloat(cellHeight))
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: false)
    }
}
