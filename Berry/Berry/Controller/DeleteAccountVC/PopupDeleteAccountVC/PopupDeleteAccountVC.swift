//
//  PopupDeleteAccountVC.swift
//  Berry
//
//  Created by Anil on 08/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class PopupDeleteAccountVC: UIViewController {

    @IBOutlet weak var viewBerryAndSomeOneElse: UIView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTypePlaceHolder: UILabel!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var txtComment: UITextView!
    
    @IBOutlet weak var constraintHeightOfView: NSLayoutConstraint! // 314.2
    
    @IBOutlet weak var btnRightIcon: UIButton!
    @IBOutlet weak var btnShareBerry: UIButton!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    var type:DeleteAccount = DeleteAccount.None
    var handlerDeleteAccount:(_ isDelete:Bool) -> Void = {_ in}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("scrollview.contentSize:=",scrollview.contentSize)
        constraintHeightOfView.constant = scrollview.contentSize.height
    }

    func setupUI() {
        lblMsg.textColor = UIColor.appThemeDarkGrayColor
        lblMsg.font = themeFont(size: 17, fontname: .bold)
        lblDescription.textColor = UIColor.appThemeLightGrayColor
        lblDescription.font = themeFont(size: 15, fontname: .bold)
        
        btnShareBerry.setTitle("SHARE BERRY", for: .normal)
        btnShareBerry.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        btnShareBerry.backgroundColor = UIColor.appThemePurpleColor
        btnShareBerry.setTitleColor(UIColor.white, for: .normal)
        
        btnDeleteAccount.setTitle("DELETE ACCOUNT", for: .normal)
        btnDeleteAccount.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        btnDeleteAccount.setTitleColor(UIColor.white, for: .normal)
        btnDeleteAccount.backgroundColor = UIColor.appThemeRedColor
        
        viewComment.isHidden = true
        viewComment.backgroundColor = UIColor.appThemeLightShadBackgroundColor
        lblTypePlaceHolder.font = themeFont(size: 14.0, fontname: .regular)
        lblTypePlaceHolder.textColor = UIColor.appThemeDarkGrayColor
        txtComment.font = themeFont(size: 14.0, fontname: .medium)
        txtComment.textColor = UIColor.black
        txtComment.delegate = self
        lblTypePlaceHolder.text = "Type here"
        
        var img = ""
        if type == DeleteAccount.FoundOnBerry {
            img = "ic_barry_popup_screen.png"
            lblMsg.text = getCommonString(key: "Congratulations_key")
            lblDescription.text = "That's great! Follow us on Instagram @berry.dates and tell us all about it! Don’t forget to share Berry with your friends.\n\nIf you delete your account, you will lose your profile, messages, photos and matches permanently.\n\nAre you sure you want to delete your account?"
        } else if type == DeleteAccount.FoundSomeoneElse {
            img = "ic_elsewhere_popup_screen.png"
            lblMsg.text = getCommonString(key: "Congratulations_key")
            lblDescription.text = "That's great! Follow us on Instagram @berry.dates and tell us all about it! Don’t forget to share Berry with your friends.\n\nIf you delete your account, you will lose your profile, messages, photos and matches permanently.\n\nAre you sure you want to delete your account?"
        } else if type == DeleteAccount.DonotLikeBerry {
            lblMsg.text = "We're really sorry to see you go." //getCommonString(key: "Dont_like_Berry_key")
            img = "ic_dont_like_berry_popup_screen.png"
            lblDescription.text = "At Berry we are constantly looking to enhance our service. We'd really appreciate any feedback you have about your time on the app and why you want to leave.\n\nYou can leave a comment in the box below.\n\nAre you sure you want to delete your account?"
//            lblDescription.text = "We're really sorry to see you go.\n\nAt Berry we are constantly looking to enhance our service. We'd really appreciate any feedback you have about your time on the app and why you want to leave.\n\nYou can leave a comment in the box below.\n\nAre you sure you want to delete your account?"
            btnShareBerry.setTitle("SUBMIT FEEDBACK", for: .normal)
            viewComment.isHidden = false
        } else if type == DeleteAccount.Other {
            lblMsg.text = "We're really sorry to see you go."//getCommonString(key: "Other_key")
            img = "ic_other_popup_screen.png"
            lblDescription.text = "At Berry we are constantly looking to enhance our service. We'd really appreciate any feedback you have about your time on the app and why you want to leave.\n\nYou can leave a comment in the box below.\n\nAre you sure you want to delete your account?"

//            lblDescription.text = "We're really sorry to see you go.\n\nAt Berry we are constantly looking to enhance our service. We'd really appreciate any feedback you have about your time on the app and why you want to leave.\n\nYou can leave a comment in the box below.\n\nAre you sure you want to delete your account?"
            btnShareBerry.setTitle("SUBMIT FEEDBACK", for: .normal)
            viewComment.isHidden = false
        }
        
        btnRightIcon.setImage(UIImage(named: img), for: .normal)
    
    }
    func openShareActivity() {
        let text = "Let me recommend you this application"
//        let image = UIImage(named: "Product")

        let myWebsite = URL(string:"https://itunes.apple.com/us/app/berry/id102102103?mt=8")!
        let shareAll = [text, myWebsite] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }

    func validateDeleteAccount() {
        var reason = ""
        if type == DeleteAccount.FoundOnBerry {
            reason = getCommonString(key: "Found_someone_onBerry_key")
        } else if type == DeleteAccount.FoundSomeoneElse {
            reason = getCommonString(key: "Found_someone_elsewhere_key")
        } else if type == DeleteAccount.DonotLikeBerry {
            reason = getCommonString(key: "Dont_like_Berry_key")
        } else if type == DeleteAccount.Other {
            reason = getCommonString(key: "Other_key")
        }
        
        let trim = txtComment.text.toTrim()
        if trim.isEmpty && (type == DeleteAccount.DonotLikeBerry || type == DeleteAccount.Other) {
            AlertView(title: "Failed", message: "Please enter comment")
            return
        }
        deleteAccount(strReason: reason, strComment: txtComment.text ?? "")
    }
    
    // MARK:- IBAction
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShareBerry(_ sender: Any) {
        self.view.endEditing(true)
        if type == DeleteAccount.FoundOnBerry || type == DeleteAccount.FoundSomeoneElse {
             openShareActivity()
        } else if type == DeleteAccount.DonotLikeBerry || type == DeleteAccount.Other {
            let trim = txtComment.text.toTrim()
            if trim.isEmpty {
                AlertView(title: "Failed", message: "Please enter comment")
                return
            }
            sendFeedBack(strFeedback: txtComment.text!)
        }
    }
    
    @IBAction func btnDeleteAccount(_ sender: Any) {
        self.view.endEditing(true)
        validateDeleteAccount()
    }
    
    
}
//MARK:- UITextViewDelegate
extension PopupDeleteAccountVC:UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let trim = textView.text.toTrim()
        lblTypePlaceHolder.isHidden = !trim.isEmpty
    }
}

//MARK:- APi Call
extension PopupDeleteAccountVC {
    func deleteAccount(strReason:String, strComment:String) {
        let param = ["userid": userDetails.getString(key: .userid),"reason":strReason,"comment":strComment]
        print("param:=",param)
        ApiManager.shared.MakePostAPI(name: DELETE_ACCOUNT, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
//                self.profileDetails = json
                let error = json.getString(key: .error)
                if error != YES {
                    deletJSON(key: USER_DETAILS_KEY)
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Success", message: msg)
                    self.handlerDeleteAccount(true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.deleteAccount(strReason: strReason, strComment: strComment)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func sendFeedBack(strFeedback:String) {
        let param = ["userid": userDetails.getString(key: .userid),"feedback":strFeedback]
        print("param:=",param)
        ApiManager.shared.MakePostAPI(name: SEND_FEEDBACK, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                //                self.profileDetails = json
                let error = json.getString(key: .error)
                if error != YES {
//                    deletJSON(key: USER_DETAILS_KEY)
//                    let msg = json.getString(key: .msg)
//                    AlertView(title: "Success", message: msg)
                    self.handlerDeleteAccount(false)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.sendFeedBack(strFeedback: strFeedback)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
}
