//
//  TinderCard.swift
//  Berry
//
//  Created by YASH on 2/11/18.
//  Copyright © 2017 Nicky. All rights reserved.
//

let THERESOLD_MARGIN = (UIScreen.main.bounds.size.width/2) * 0.75
let THERESOLD_VERTICAL_MARGIN = (UIScreen.main.bounds.size.height/2) * 0.45

let SCALE_STRENGTH : CGFloat = 4
let SCALE_RANGE : CGFloat = 0.90

import UIKit
import SwiftyJSON
import SDWebImage
import InMobiSDK

protocol TinderCardDelegate: NSObjectProtocol {
    func cardGoesLeft(card: TinderCard)
    func cardGoesRight(card: TinderCard)
    func cardGoesUp(card: TinderCard)
    func cardGoesDown(card: TinderCard)
    func currentCardStatus(card: TinderCard, distance: CGFloat)
    func imageSelected(index: Int, images: [JSON])
    func superLikeSelected()
}

class TinderCard: UIView
{
    var xCenter: CGFloat = 0.0
    var yCenter: CGFloat = 0.0
    var originalPoint = CGPoint.zero
    var imageViewStatus = UIImageView()
    var overLayImage = UIImageView()
    var isLiked = false
    var pageControll = UIPageControl()
    var collectionView : UICollectionView?
    
    var upperImageShadow = UIImageView()
    var bottomImageShadow = UIImageView()
    var json : JSON = JSON()
    let popUpView = Bundle.main.loadNibNamed("Card", owner: self, options: nil)?[0] as? Card
    let galleryView = Bundle.main.loadNibNamed("GalleryImage", owner: self, options: nil)?[0] as? GalleryImage
    var numberOfPages : Int = 0
    var numberOfInstaPages : Int = 0
    weak var delegate: TinderCardDelegate?
    
    public init(frame: CGRect, value: String,json: JSON, count:Int,atIndex :Int, superVC:SwipeCardVc) {
        super.init(frame: frame)
        self.json = json
        let isAds = self.json.getBool(key: .isAds)
        if isAds {
            setupAdsView(at: value,count:count,atIndex : atIndex, superVC: superVC)
        }
        else {
            setupView(at: value,count:count,atIndex : atIndex)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupAdsView(at value:String,count:Int,atIndex : Int, superVC:SwipeCardVc?) {
        layer.cornerRadius = 10
        clipsToBounds = true
        isUserInteractionEnabled = false
        originalPoint = center        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.beingDragged))
        addGestureRecognizer(panGestureRecognizer)
        //------New ------//
        self.numberOfPages = 1
        
        let detailsView = Bundle.main.loadNibNamed("InMobiAdsView", owner:
            self, options: nil)?.first as? InMobiAdsView
        detailsView?.frame = CGRect(x: 0, y: frame.origin.y + 10, width: frame.size.width, height: frame.size.height-20)
        detailsView!.backgroundColor = UIColor.lightGray
        detailsView!.layer.cornerRadius = 10
        detailsView!.layer.masksToBounds = true
        detailsView!.superVC = superVC
        addSubview(detailsView!)
        detailsView?.loadAds()
    
        upperImageShadow = UIImageView(frame: CGRect(x: 7, y: frame.origin.y, width: frame.size.width - 14, height: 10))
        upperImageShadow.image = UIImage(named: "top")
        
        if(count-1 != atIndex) {
            addSubview(upperImageShadow)
        }
        
        bottomImageShadow = UIImageView(frame: CGRect(x: 7, y: frame.height-10, width: frame.size.width - 14, height: 10))
        bottomImageShadow.image = UIImage(named: "bottom")
        
        if(count-1 != atIndex) {
            addSubview(bottomImageShadow)
        }
        
//        galleryView?.frame = frame
//        galleryView?.isHidden = true
//        galleryView?.btnBack.addTarget(self, action: #selector(btnBackAction), for: .touchUpInside)
//        addSubview(galleryView!)
    }
    
    func setupView(at value:String,count:Int,atIndex : Int) {
        layer.cornerRadius = 10
        clipsToBounds = true
        isUserInteractionEnabled = false
        originalPoint = center
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.beingDragged))
        addGestureRecognizer(panGestureRecognizer)
        
        //------New ------//
        let images = self.json.getArray(key: .profile).count
        self.numberOfPages += images
        let instaOn = self.json.getInt(key: .insta) == 0 ? false : true
        if instaOn {
            let instaLinks = self.json.getArray(key: .insta_links)
            self.numberOfInstaPages = instaLinks.count
            self.numberOfPages += (instaLinks.count == 0 ? 0 : 1)
        }
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: frame.size.width, height: frame.size.height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        let detailsCollectionView = UICollectionView(frame: CGRect(x: 0, y: frame.origin.y + 10, width: frame.size.width, height: frame.size.height-20), collectionViewLayout: layout)
        
        detailsCollectionView.delegate = self
        detailsCollectionView.dataSource = self
        detailsCollectionView.isPagingEnabled = true
        
        detailsCollectionView.layer.cornerRadius = 10
        detailsCollectionView.layer.masksToBounds = true
        
        
        detailsCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        detailsCollectionView.register(UINib(nibName: "DetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "detailCell")
        detailsCollectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imageCell")
        detailsCollectionView.register(UINib(nibName: "ImagesGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        
        collectionView = detailsCollectionView
        self.collectionView?.reloadData()
        addSubview(detailsCollectionView)
        
        upperImageShadow = UIImageView(frame: CGRect(x: 7, y: frame.origin.y, width: frame.size.width - 14, height: 10))
        upperImageShadow.image = UIImage(named: "top")
        
        if(count-1 != atIndex) {
            addSubview(upperImageShadow)
        }
        
        bottomImageShadow = UIImageView(frame: CGRect(x: 7, y: frame.height-10, width: frame.size.width - 14, height: 10))
        bottomImageShadow.image = UIImage(named: "bottom")
        
        if(count-1 != atIndex) {
            addSubview(bottomImageShadow)
        }
        
        pageControll = UIPageControl(frame: CGRect(x: 50, y: frame.size.height - 60, width: frame.size.width - 100, height: 20))
        pageControll.numberOfPages = self.numberOfPages
        pageControll.tintColor = UIColor.lightGray
        pageControll.pageIndicatorTintColor = UIColor.lightGray
        
        addSubview(pageControll)
        popUpView?.frame = frame
        popUpView?.isHidden = true
        
        popUpView?.collectionViewGallery.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "galleryImageCell")
        popUpView?.collectionViewGallery.tag = 2
        popUpView?.collectionViewGallery.delegate = self
        popUpView?.collectionViewGallery.dataSource = self
        popUpView?.collectionViewGallery.reloadData()
        popUpView?.btnBack.addTarget(self, action: #selector(btnBackAction), for: .touchUpInside)
        addSubview(popUpView!)
        
        galleryView?.frame = frame
        galleryView?.isHidden = true
        galleryView?.btnBack.addTarget(self, action: #selector(btnBackAction), for: .touchUpInside)
        addSubview(galleryView!)
    }
    
    @objc func  imageGetureReconizer(_ gestureRecognizer: UIPanGestureRecognizer) {
        let velocity = gestureRecognizer.velocity(in: self)
        if(velocity.x > 0) {
            //            Right
        }
        else {
            //            Left
        }
        
        if(velocity.y > 0) {
            //            Up
            swipe(gestureRecognizer)
        }
        else {
            //            Down
            swipe(gestureRecognizer)
        }
    }
    
    @objc func beingDragged(_ gestureRecognizer: UIPanGestureRecognizer) {
        
        let velocity = gestureRecognizer.velocity(in: self)
        if(velocity.x > 0) {
            //            Main view Right
        }
        else {
            //            Main view Left
        }
        
        if(velocity.y > 0) {
            //            Main view Up
            swipe(gestureRecognizer)
        }
        else {
            //            Main view Down
            swipe(gestureRecognizer)
        }
    }
    
    func swipe(_ gestureRecognizer: UIPanGestureRecognizer) {
        xCenter = gestureRecognizer.translation(in: self).x
        yCenter = gestureRecognizer.translation(in: self).y
        upperImageShadow.isHidden = true
        bottomImageShadow.isHidden = true
        
        switch gestureRecognizer.state {
        // Keep swiping
        case .began:
            //            began
            originalPoint = self.center;
            break;
        //in the middle of a swipe
        case .changed:
            //            Changed
            center = CGPoint(x: originalPoint.x, y: originalPoint.y + yCenter)
            break;
        case .ended:
            //            ended
            upperImageShadow.isHidden = false
            bottomImageShadow.isHidden = false
            afterSwipeAction()
            break;
        case .possible:break
        case .cancelled:
            //            cancelled
            break
        case .failed:
            //            failed
            break
        }
    }
    
    func updateOverlay(_ distance: CGFloat) {
        imageViewStatus.alpha = min(fabs(distance) / 100, 0.5)
        overLayImage.alpha = min(fabs(distance) / 100, 0.5)
        delegate?.currentCardStatus(card: self, distance: distance)
    }
    
    func afterSwipeAction() {
        if yCenter > THERESOLD_VERTICAL_MARGIN {
            downAction()
        }
        else if yCenter < -THERESOLD_VERTICAL_MARGIN {
            upAction()
        }
        else {
            //reseting image
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: [], animations: {
                self.center = self.originalPoint
                self.transform = CGAffineTransform(rotationAngle: 0)
                self.imageViewStatus.alpha = 0
                self.overLayImage.alpha = 0
                self.delegate?.currentCardStatus(card: self, distance:0)
            })
        }
    }
    
    func rightAction() {
        let finishPoint = CGPoint(x: frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        UIView.animate(withDuration: 0.5, animations: {
            self.center = finishPoint
        }, completion: {(_) in
            self.removeFromSuperview()
        })
        isLiked = true
        delegate?.cardGoesRight(card: self)
        //WATCHOUT RIGHT
    }
    
    func leftAction() {
        let finishPoint = CGPoint(x: -frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        UIView.animate(withDuration: 0.5, animations: {
            self.center = finishPoint
        }, completion: {(_) in
            self.removeFromSuperview()
        })
        isLiked = false
        delegate?.cardGoesLeft(card: self)
        //        WATCHOUT LEFT
    }
    
    func upAction(time:Double = 0.3) {
        upperImageShadow.isHidden = true
        bottomImageShadow.isHidden = true
        let finishPoint = CGPoint(x: originalPoint.x, y: -frame.size.height*2)
        UIView.animate(withDuration:time, animations: {
            self.center = finishPoint
        }, completion: {(_) in
            self.removeFromSuperview()
        })
        isLiked = false
        delegate?.cardGoesUp(card: self)
        //        WATCHOUT UP
    }
    
    func downAction(time:Double = 0.3) {
        upperImageShadow.isHidden = true
        bottomImageShadow.isHidden = true
        let finishPoint = CGPoint(x: originalPoint.x, y: frame.size.height*2)
        UIView.animate(withDuration: time, animations: {
            self.center = finishPoint
        }, completion: {(_) in
            self.removeFromSuperview()
        })
        isLiked = false
        delegate?.cardGoesDown(card: self)
        //        WATCHOUT DOWN
    }
    
    // right click action
    func rightClickAction() {
        imageViewStatus.image = #imageLiteral(resourceName: "btn_like_pressed")
        overLayImage.image = #imageLiteral(resourceName: "overlay_like")
        let finishPoint = CGPoint(x: center.x + frame.size.width * 2, y: center.y)
        imageViewStatus.alpha = 0.5
        overLayImage.alpha = 0.5
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.center = finishPoint
            self.transform = CGAffineTransform(rotationAngle: 1)
            self.imageViewStatus.alpha = 1.0
            self.overLayImage.alpha = 1.0
        }, completion: {(_ complete: Bool) -> Void in
            self.removeFromSuperview()
        })
        isLiked = true
        delegate?.cardGoesRight(card: self)
        //        WATCHOUT RIGHT ACTION
    }
    
    // left click action
    func leftClickAction() {
        
        imageViewStatus.image = #imageLiteral(resourceName: "btn_skip_pressed")
        overLayImage.image = #imageLiteral(resourceName: "overlay_skip")
        let finishPoint = CGPoint(x: center.x - frame.size.width * 2, y: center.y)
        imageViewStatus.alpha = 0.5
        overLayImage.alpha = 0.5
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.center = finishPoint
            self.transform = CGAffineTransform(rotationAngle: -1)
            self.imageViewStatus.alpha = 1.0
            self.overLayImage.alpha = 1.0
        }, completion: {(_ complete: Bool) -> Void in
            self.removeFromSuperview()
        })
        isLiked = false
        delegate?.cardGoesLeft(card: self)
        //        WATCHOUT LEFT ACTION
    }
    
    // undoing  action
    func makeUndoAction() {
        upperImageShadow.isHidden = false
        bottomImageShadow.isHidden = false
        imageViewStatus.alpha = 1.0
        overLayImage.alpha = 1.0
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            self.center = self.originalPoint
            self.transform = CGAffineTransform(rotationAngle: 0)
            self.imageViewStatus.alpha = 0
            self.overLayImage.alpha = 0
        })
        //        WATCHOUT UNDO ACTION
    }
    
    func rollBackCard() {
        UIView.animate(withDuration: 0.5) {
            self.removeFromSuperview()
        }
    }
    
    func shakeAnimationCard() {
        imageViewStatus.image = #imageLiteral(resourceName: "btn_skip_pressed")
        overLayImage.image = #imageLiteral(resourceName: "overlay_skip")
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.center = CGPoint(x: self.center.x - (self.frame.size.width / 2), y: self.center.y)
            self.transform = CGAffineTransform(rotationAngle: -0.2)
            self.imageViewStatus.alpha = 1.0
            self.overLayImage.alpha = 1.0
        }, completion: {(_) -> Void in
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                self.imageViewStatus.alpha = 0
                self.overLayImage.alpha = 0
                self.center = self.originalPoint
                self.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: {(_ complete: Bool) -> Void in
                self.imageViewStatus.image = #imageLiteral(resourceName: "btn_like_pressed")
                self.overLayImage.image =  #imageLiteral(resourceName: "overlay_like")
                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                    self.imageViewStatus.alpha = 1
                    self.overLayImage.alpha = 1
                    self.center = CGPoint(x: self.center.x + (self.frame.size.width / 2), y: self.center.y)
                    self.transform = CGAffineTransform(rotationAngle: 0.2)
                }, completion: {(_ complete: Bool) -> Void in
                    UIView.animate(withDuration: 0.5, animations: {() -> Void in
                        self.imageViewStatus.alpha = 0
                        self.overLayImage.alpha = 0
                        self.center = self.originalPoint
                        self.transform = CGAffineTransform(rotationAngle: 0)
                    })
                })
            })
        })
        //        WATCHOUT SHAKE ACTION
    }
}

extension TinderCard {
    @objc func btnBackAction(_ sender : UIButton) {
        popUpView?.isHidden = true
        galleryView?.isHidden = true
    }
}

extension TinderCard : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == collectionView) {
            // crollview did end called
        }
        let currentIndex = (collectionView?.contentOffset.x)! / (collectionView?.frame.size.width)!;
        pageControll.currentPage = Int(currentIndex)
    }
}

extension TinderCard : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == 1) {
            return self.numberOfInstaPages > 9 ? 9 : self.numberOfInstaPages
        }
        else if(collectionView.tag == 2) {
            return 30
        }
        else {
            return self.numberOfPages
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView.tag == 1 || collectionView.tag == 2) {
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryImageCell", for: indexPath) as! GalleryCollectionViewCell
            let image = self.json.getArray(key: .insta_links)
            
            if !image.isEmpty {
                if image.count > indexPath.row {
                    cell.imgvwGalleryImage.sd_setImage(with: URL(string: image[indexPath.row].string ?? ""), placeholderImage: UIImage(named: "placeholder_image_listing_square"), completed: nil)
                    
                    cell.imgvwGalleryImage.tag = indexPath.row
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageInstaTapped(tapGestureRecognizer:)))
                    cell.imgvwGalleryImage.isUserInteractionEnabled = true
                    cell.imgvwGalleryImage.addGestureRecognizer(tapGestureRecognizer)
                }
            }
            
            return cell
        }
        else {
            let images = self.json.getArray(key: .profile).count
            if images > (indexPath.row + 1) {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionViewCell
                cell.btnSuperLike.addTarget(self, action: #selector(self.superLikeClicked), for: .touchUpInside)
                cell.btnSuperLike.isHidden = indexPath.row == 0 ? false : true
                print(self.json)
                if indexPath.row == 0 {
                    cell.btnSuperLike.isHidden = self.json.getInt(key: .superLike) == 1 ? false : true
                }
                let attributedText = NSMutableAttributedString(string: self.json.getString(key: .name).capitalized + " | \(self.json.getInt(key: .years))", attributes: [.foregroundColor: UIColor.white,.font:UIFont.boldSystemFont(ofSize: 23)])
                attributedText.append(NSAttributedString(string: "\n" + self.json.getString(key: .hobbies), attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 14)]))
                attributedText.append(NSAttributedString(string: "\n" + self.json.getString(key: .job), attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 14)]))
                
                cell.lblUserName.attributedText = attributedText
                
                let distance = self.json.getInt(key: .distance)
                let distanceValue: String = 1 > distance ? "<1" : "\(distance)"
                cell.lblDistance.text = "\(distanceValue) km away"
//                cell.lblDistance.text = "\(self.json.getInt(key: .distance)) km away"
                
                let image = self.json.getArray(key: .profile)
                cell.imgProfile?.sd_setImage(with: URL(string: image[indexPath.row].string!), placeholderImage: UIImage(named: "placeholder_image_listing_square"), completed: nil)
                cell.lblDistance.isHidden = true
                cell.imgDistance.isHidden = true
                cell.imgProfile.tag = indexPath.row
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.imgProfile.isUserInteractionEnabled = true
                cell.imgProfile.addGestureRecognizer(tapGestureRecognizer)
                
                return cell
                
            }
            else if images == (indexPath.row + 1) {
                // get a reference to our storyboard cell
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "detailCell", for: indexPath) as! DetailsCollectionViewCell
                
                let attributedText = NSMutableAttributedString(string: self.json.getString(key: .name).capitalized + " | \(self.json.getInt(key: .years))", attributes: [.foregroundColor: UIColor.white,.font:UIFont.boldSystemFont(ofSize: 23)])
                
                attributedText.append(NSAttributedString(string: "\n" + self.json.getString(key: .hobbies), attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 14)]))
                
                attributedText.append(NSAttributedString(string: "\n" + self.json.getString(key: .job), attributes: [.foregroundColor: UIColor.white,.font:UIFont.systemFont(ofSize: 14)]))
                
                cell.lblUserName.attributedText = attributedText
                
                let distance = self.json.getInt(key: .distance)
                let distanceValue: String = 1 > distance ? "<1" : "\(distance)"
                cell.lblDistance.text = "\(distanceValue) km away"
//                cell.lblDistance.text = "\(self.json.getInt(key: .distance)) km away"
                cell.lblAbout.text = self.json.getString(key: .about)
                
                let image = self.json.getArray(key: .profile)
                cell.imgBackground?.sd_setImage(with: URL(string: image[indexPath.row].string!), placeholderImage: UIImage(named: "placeholder_image_listing_square"), completed: nil)
                cell.lblDistance.isHidden = false
                cell.imgDistance.isHidden = false
                
                cell.vwBlur.tag = indexPath.row
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.vwBlur.isUserInteractionEnabled = true
                cell.vwBlur.addGestureRecognizer(tapGestureRecognizer)
                return cell
            }
            else {
                //gridCell
                
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath) as! ImagesGridCollectionViewCell
                cell.collectionViewGrid.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "galleryImageCell")
                
                let image = self.json.getArray(key: .profile)
                cell.imgUser?.sd_setImage(with: URL(string: image.first!.string!), placeholderImage: UIImage(named: "placeholder_image_listing_square"), completed: nil)
                cell.collectionViewGrid.tag = 1
                cell.collectionViewGrid.delegate = self
                cell.collectionViewGrid.dataSource = self
                cell.collectionViewGrid.reloadData()
                return cell
            }
        }
    }
    
    @objc func superLikeClicked() {
        self.delegate?.superLikeSelected()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView.tag == 1) {
            //            popUpView?.isHidden = true
            //            galleryView?.isHidden = false
        }        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth : CGFloat = 0.0
        var cellHeight : CGFloat = 0.0
        if(collectionView.tag == 1 ) {
            cellWidth  = collectionView.frame.size.width/3
            cellHeight  = collectionView.frame.size.height/3
            return CGSize(width: cellWidth , height:CGFloat(cellHeight))
        }
        else if(collectionView.tag == 2) {
            cellWidth  = collectionView.frame.size.width/3
            cellHeight  = collectionView.frame.size.height/3
            return CGSize(width: cellWidth , height:CGFloat(cellWidth))
        }
        else {
            cellWidth  = collectionView.frame.size.width
            cellHeight  = collectionView.frame.size.height
            return CGSize(width: cellWidth , height:CGFloat(cellHeight))
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let tag = tapGestureRecognizer.view!.tag        
        self.delegate?.imageSelected(index: tag, images: self.json.getArray(key: .profile))
    }
    
    @objc func imageInstaTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let tag = tapGestureRecognizer.view!.tag
        self.delegate?.imageSelected(index: tag, images: self.json.getArray(key: .insta_links))
    }
}
