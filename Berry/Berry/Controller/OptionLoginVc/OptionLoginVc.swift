//
//  OptionLoginVc.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import GoogleSignIn

var IS_FOR_OTP_FAIL:Bool = false

class OptionLoginVc: UIViewController, GIDSignInDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var constantBottomUpperImg: NSLayoutConstraint!
    @IBOutlet weak var btnFB: CustomButton!
    @IBOutlet weak var btnGooglePlus: CustomButton!
    @IBOutlet weak var btnSignIn: CustomButton!
    @IBOutlet weak var btnNewUser: CustomButton!
    
    //MARK: - Variables
    var arrayOfURLimagesForFB = [URL]()
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance()?.uiDelegate = self
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.shared.checkOldUser()
        self.navigationController?.navigationBar.isHidden = true
        if IS_FOR_OTP_FAIL {
            IS_FOR_OTP_FAIL = false
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
            self.navigationController?.pushViewController(obj, animated: false)
        }
    }
    
    func setupUI() {
        [btnFB,btnGooglePlus].forEach { (btn) in
            btn?.layer.cornerRadius = btn!.layer.bounds.height / 2
            btn?.layer.masksToBounds = true
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .medium)
        }
        
        [btnSignIn,btnNewUser].forEach { (btn) in
            btn?.setupShadowThemeButtonUI()
        }
        
        if UIScreen.main.bounds.width < 375 {
            constantBottomUpperImg.constant = 15
            [btnFB,btnGooglePlus].forEach { (btn) in
                btn?.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
                btn?.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
            }
        }
        else {
            constantBottomUpperImg.constant = 30
            [btnFB,btnGooglePlus].forEach { (btn) in
                btn?.titleEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)
                btn?.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
            }
        }
        
        btnFB.setTitle(getCommonString(key: "LOGIN_WITH_FACEBOOK_key"), for: .normal)
        btnGooglePlus.setTitle(getCommonString(key: "LOGIN_WITH_GOOGLE_key"), for: .normal)
        
        btnSignIn.setTitle(getCommonString(key: "SIGN_IN_key"), for: .normal)
        btnNewUser.setTitle(getCommonString(key: "NEW_USER_key"), for: .normal)
        btnFB.isHidden = true
        btnGooglePlus.isHidden = true
    }
    
    func makeSignupWithSocial(isFacebook:Bool, type: String, email:String, name: String, birthDate:String, gender: String, authid: String, profile: String) {
        
        var param = ["type":type,"name":name,"email":email,"authid":authid,"profile":profile, "birth":birthDate, "gender":gender, "firebaseTokenId":getFCMToken(), "det": DEVICE_TYPE]
        if birthDate == "" {
            param.removeValue(forKey: "birth")
        }
        
        if gender == "" {
            param.removeValue(forKey: "gender")
        }
        print(param)
        ApiManager.shared.MakePostAPI(name: REGISTER, params: param, vc: self, completionHandler: { (response, error) in
            if response != nil {
                let json = JSON(response!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    let userid = json.getString(key: .userid)
                    let myprofile = json.getDictionary(key: .myprofile)
                    let missing = myprofile.getArray(key: .missing)
                    
                    if missing.count == 0 {
                        userDetails = json                        
                        if myprofile.getBool(key: .verified) == false {
                            let mobile = myprofile.getString(key: .mobile_no)
                            let co = myprofile.getString(key: .co)
                            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OTPVc") as! OTPVc
                            obj.userId = userid
                            obj.mobileNo = mobile
                            obj.countryDialCode = "+" + co
                            if type.lowercased() == "facebook".lowercased() || type.lowercased() == "google".lowercased() {
                                obj.isOpenSocial = true
                            }
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                        else {
                            saveJSON(j: json, key: USER_DETAILS_KEY)
                            AppDelegate.shared.OutTime(type: "login")
                            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVc") as! MainVc
                            obj.isLogin = true
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                    else {
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SignUpVc") as! SignUpVc
                        obj.isSignUpSocial = true
                        obj.userId = userid
                        obj.missingArray = missing
                        for image in json.getDictionary(key: .myprofile).getArray(key: .profile) {
                            let dict: NSDictionary = ["imageURL" : image.string!, "isImage": "0", "isNew": "0"]
                            obj.arrayImageUser.add(dict)
                        }
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }           
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeSignupWithSocial(isFacebook: isFacebook, type: type, email: email, name: name, birthDate: birthDate, gender: gender, authid: authid, profile: profile)
//                AlertView(title: "Failed", message: error!)
            }
        })
    }
}

//MARK: - @IBAction
extension OptionLoginVc {
    @IBAction func btnFBTapped(_ sender: UIButton) {
        AppDelegate.shared.checkOldUser()
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: { (result, error) -> Void in
            if (error == nil) {
                if (result?.grantedPermissions != nil) {
                    self.showLoader()
                    self.returnUserData()
                }
                else {
                    self.hideLoader()
                }
            }
            else {
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
            }
        })
    }
    
    @IBAction func btnGooglePlusTapped(_ sender: UIButton) {
        AppDelegate.shared.checkOldUser()
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        self.navigationController?.pushViewController(obj, animated: true)        
    }
    
    @IBAction func btnNewUserTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SignUpVc") as! SignUpVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:Google SignIn Delegate
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            let userId = user.userID
            let fullName = user.profile.name
            let email = user.profile.email
            let imageURL = user.profile.imageURL(withDimension: 100)

            self.makeSignupWithSocial(isFacebook: false, type: "google", email: email ?? "", name: fullName ?? "", birthDate: "", gender: "", authid: userId ?? "", profile: imageURL?.absoluteString ?? "")
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        
    }
    
    //MARK: - FaceBook Login
    func returnUserData() {
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, birthday, email, gender, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    self.hideLoader()
                    let info = result as! NSDictionary
                    print(info)
                    let imageURL = ((info["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String
                    
                    let dataFB = JSON(result!)
                    
                    let fbID = dataFB["id"].stringValue
                    let name = dataFB["name"].stringValue
                    let email = dataFB["email"].stringValue
                    let birthDate = convertDateFormater(dataFB["birthday"].stringValue)
                    let gender = dataFB["gender"].stringValue
//                    birthDate
                    Defaults.setValue(fbID, forKey: "facebookID")
                    Defaults.synchronize()
                    self.makeSignupWithSocial(isFacebook: true, type: "facebook", email: email, name: name, birthDate: birthDate, gender: gender, authid: fbID, profile: imageURL ?? "")
                }
            })
        }
    }
}
