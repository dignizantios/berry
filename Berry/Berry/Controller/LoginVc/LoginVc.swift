//
//  LoginVc.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginVc: UIViewController, CountryCodeDelegate {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var constantBottomUpperImg: NSLayoutConstraint!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignIn: CustomButton!
   
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgCountryCode: UIImageView!
    @IBOutlet weak var viewCountry: UIView!
    
    //MARK: - Variables
    var countryDialCode: String!
    var countryName: String!
    var countryCode: String!
    //MARK: - ViewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.setupNavigationbar(titleText: "")
        AppDelegate.shared.checkOldUser()
    }
    
    func setupUI()
    {
        let countryJSON = getCountryPhonceCode(Locale.current.regionCode ?? "")
        self.countryDialCode = countryJSON["dial_code"].stringValue
        self.countryName = countryJSON["name"].stringValue
        self.countryCode = countryJSON["code"].stringValue
        
        [btnSignIn].forEach { (btn) in
            btn?.setupShadowThemeButtonUI()
        }
        
        self.viewCountry.layer.cornerRadius = 8
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
        
        btnForgotPassword.setTitleColor(UIColor.appThemePurpleColor, for: .normal)
        btnForgotPassword.setTitle(getCommonString(key: "Forgot_password_key"), for: .normal)
        btnForgotPassword.titleLabel?.font = themeFont(size: 15, fontname: .light)
        
        if UIScreen.main.bounds.width < 375 {
            constantBottomUpperImg.constant = 15
        }
        else {
            constantBottomUpperImg.constant = 30
        }
        
        [txtMobileNumber,txtPassword].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.leftPaddingView = 10
            txt?.cornerRadius = 8
            txt?.delegate = self
        }
        
        txtMobileNumber.placeholder = getCommonString(key: "Mobile_number_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        btnSignIn.setTitle(getCommonString(key: "SIGN_IN_key"), for: .normal)
        addDoneButtonOnKeyboard(textfield: txtMobileNumber)
    }
    
    func validationLogin() {
        if self.txtMobileNumber.text!.toTrim() == "" {
            AlertView(title: "Failed", message: "Please enter mobile number")
        }
        else if self.txtPassword.text!.toTrim() == "" {
            AlertView(title: "Failed", message: "Please enter password")
        }
        else {
            self.makeLogin()
        }
    }
    
    func makeLogin() {
        
        let location = jsonString(from: [AppDelegate.shared.longitude,AppDelegate.shared.lattitude])
        var param: [String : Any]?
        if AppDelegate.shared.longitude != 0 && AppDelegate.shared.lattitude != 0 {
            param =                 ["type":"guest","mobile_no":self.txtMobileNumber.text!.toTrim(), "co": self.countryDialCode.dropFirst(), "password":self.txtPassword.text!.toTrim(), "firebaseTokenId":getFCMToken(),"location": "\"" + location! + "\"", "det": DEVICE_TYPE]
        }
        else {
            param =                 ["type":"guest","mobile_no":self.txtMobileNumber.text!.toTrim(),"password":self.txtPassword.text!.toTrim(), "co": self.countryDialCode.dropFirst(), "firebaseTokenId":getFCMToken(), "det": DEVICE_TYPE]
        }
        
        ApiManager.shared.MakePostAPI(name: LOGIN, params: param!, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                    let isVerifyed = json.getDictionary(key: .myprofile).getInt(key: .verified)
                    if isVerifyed == 1 {
                        saveJSON(j: json, key: USER_DETAILS_KEY)
                        userDetails = json
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                            AppDelegate.shared.OutTime(type: "login")
                            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVc") as! MainVc
                            obj.isLogin = true
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                    else {
                        saveJSON(j: json, key: USER_DETAILS_KEY)
                        userDetails = json
                        let userid = json.getString(key: .userid)
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OTPVc") as! OTPVc
                        obj.userId = userid
                        obj.countryDialCode = self.countryDialCode
                        obj.mobileNo = self.txtMobileNumber.text!                        
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.makeLogin()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
}


//MARK: - @IBAction

extension LoginVc {
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVc") as! ForgotPasswordVc
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        self.validationLogin()
    }
    
    @IBAction func btnConfirmShowPasswordTapped(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "password_not_viewed") {
            UIView.performWithoutAnimation {
                self.txtPassword.isSecureTextEntry = false
                sender.setImage(UIImage(named: "password_viewed"), for: .normal)
            }
        }
        else {
            UIView.performWithoutAnimation {
                self.txtPassword.isSecureTextEntry = true
                sender.setImage(UIImage(named: "password_not_viewed"), for: .normal)
            }
        }
    }
    
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        self.present(obj, animated:  false, completion: nil)
    }
    
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
    }
}

//MARK: - UITextfield Delegate

extension LoginVc : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if textField == txtMobileNumber {
            
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        else {
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)

                if updatedText == " " {
                    return false
                }
            }
        }
        return true
    }
}
