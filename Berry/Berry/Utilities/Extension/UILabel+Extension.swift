//
//  UILabel+Extension.swift
//  Liber
//
//  Created by YASH on 24/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func addImageToLabel(imageName: String, strText : String) {
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:imageName)
        //Set bound to reposition
        
        var imageOffsetY:CGFloat = 0
        if UIScreen.main.bounds.height <= 736 { // Below iphone X
            imageOffsetY = 0.0;
        }
        else {
            imageOffsetY = -2.0;
        }
        
        
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: strText)
        completeText.append(textAfterIcon)
        
        self.attributedText = completeText
    }    
}
