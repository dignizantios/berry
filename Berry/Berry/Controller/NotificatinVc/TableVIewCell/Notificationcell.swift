//
//  Notificationcell.swift
//  Berry
//
//  Created by YASH on 31/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class Notificationcell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDescription: LabelButton!
    @IBOutlet weak var vwImageBack: UIView!
    @IBOutlet weak var img: UIImageView!
    
    //MARK: - View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        [lblDescription].forEach { (lbl) in
//            lbl?.textColor = UIColor.appThemeDarkGrayColor
//            lbl?.font = themeFont(size: 15, fontname: .light)
//        }
        
        lblTime.textColor = UIColor.appThemeDarkGrayColor
        lblTime.font = themeFont(size: 15, fontname: .bold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.img.layer.cornerRadius = 5
        self.img.layer.masksToBounds = true
        self.img.clipsToBounds = true
        self.vwImageBack.backgroundColor = UIColor.clear
        self.vwImageBack.layer.shadowColor = UIColor.black.cgColor
        self.vwImageBack.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.vwImageBack.layer.shadowOpacity = 0.3;
        self.vwImageBack.layer.shadowRadius = 2.0;
        self.vwImageBack.layer.shadowPath = UIBezierPath(roundedRect: self.vwImageBack.bounds, cornerRadius: 5.0).cgPath
    }    
}
