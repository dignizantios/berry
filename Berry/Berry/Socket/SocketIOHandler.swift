//
//  SocketHandler.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON
//import GiphyCoreSDK

protocol SockerConnectionUnreadMsgDelegate {
    func SockerConnectionUnreadMsg(response: JSON)
}

protocol SockerConnectionDelegate {
    func reloadChatUser(response: JSON)
    func reloadSingleChatDetails(response: JSON)
    func reloadChatList(response: [JSON])
    func reloadSendChat(response: JSON)
    func reloadOnline(response: JSON)
    func reloadUnreadMsg(response: JSON)
    func reloadMatchUser(response: JSON)
}

class SocketIOHandler: NSObject {
    var delegate: SockerConnectionDelegate?
    var delegateUnreadMsg: SockerConnectionUnreadMsgDelegate?
    static let shared = SocketIOHandler()
    var manager: SocketManager?
    var socket: SocketIOClient?
    var isHandlerAdded:Bool = false
    var isJoinSocket:Bool = false
    var userId : String = ""
    var oppUserId : String = ""
    
    override init() {
        super.init()
        //            connectWithSocket()
    }
    
    func Connect() {
        if manager==nil && socket == nil {
            let credentialData = "admin:supersecret".data(using: String.Encoding.utf8)!
            let base64Credentials = credentialData.base64EncodedString(options: [])
            let headers = ["Authorization": "Basic \(base64Credentials)"]
            
            manager = SocketManager(socketURL: URL(string: SOCKET_SERVER_PATH)!, config: [.log(true), .compress, .connectParams(headers)])
            
            socket = manager?.defaultSocket
            self.userId = userDetails.getString(key: .userid)
        }
        
        self.socket?.connect()
        if(self.isSocketConnected()) {
            self.addHandlers()
            self.connectSocketWithStatus()
        }
        else {
            print("Socket Not Connected")
        }        
    }
    
    //MARK:- ConnectWithSocket
    func connectWithSocket() {
        if manager==nil && socket == nil {
            manager = SocketManager(socketURL: URL(string: SOCKET_SERVER_PATH)!, config: [.log(true), .compress, .forceNew(true)])
            socket = manager?.defaultSocket
        }
        
        socket?.connect()
        if(!isSocketConnected()) {
            connectSocketWithStatus()
        }
    }
    
    func isSocketConnected() -> Bool {
        if(socket != nil) {
            if(self.socket?.status == .connected || self.socket?.status == .connecting) {
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
    
    
    func isSocketConnecting() -> Bool {
        if(socket != nil) {
            if self.socket?.status == .connecting {
                return true;
            }
            else{
                return false;
            }
        }
        return true;
    }
    
    func connectSocketWithStatus() {
        socket?.on(clientEvent: .connect) {data, ack in
            print(self.userId)
            if self.userId != "" {
                self.joinRoom(data: self.userId)
            }
        }
        socket?.connect()
    }
    
    func disconnectSocket() {
        socket?.disconnect()
        socket = nil
        manager = nil
    }
    
    func joinRoom(data:String) {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketInit, "data":["userid":data]]
            socket?.emit(REQ, param)
        }
    }
    
//    func unreadNoUniqMsg(data:String) {
//        if(isSocketConnected()) {
//            let param: NSDictionary = ["en": SocketUnreadNoUniqMsg, "data":["userid":data]]
//            socket?.emit(REQ, param)
//        }
//    }
    
    func readMsg(oppId:String, roomId:String) {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketRead, "data":["userid":self.userId, "oppId":oppId, "roomId":roomId]]
            print(param)
            socket?.emit(REQ, param)
        }
    }
    
    func readMatch() {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketReadNotification, "data":["userid":self.userId]]
            socket?.emit(REQ, param)
        }
    }
    
    func chatUsersList() {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketChatUsersList, "data":["userid":self.userId]]
            socket?.emit(REQ, param)
        }        
    }
    
    func SingleChatDetails(oppUserId: String) {
        if(isSocketConnected()) {
            self.oppUserId = oppUserId
            let param: NSDictionary = ["en": SocketSingleChatDetails, "data":["userid":self.userId, "opp": oppUserId]]
            socket?.emit(REQ, param)
        }
    }
    
    func ChatListDetails(roomId: String, lastId: String) {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketChatList, "data":["userid":self.userId, "roomId": roomId, "lastId": lastId]]
            socket?.emit(REQ, param)
        }
        else {
            self.Connect()
        }
    }
    
    func SendMsg(oppUserId: String, msg: String) {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketSendMsg, "data":["userid":self.userId, "opp": oppUserId, "msg": msg]]
            print(param)
            socket?.emit(REQ, param)
        }
    }
    
    func AddQuestions(oppUserId: String, ans: String) {
        if(isSocketConnected()) {
            let param: NSDictionary = ["en": SocketAddQuestions, "data":["userid":self.userId, "opp": oppUserId, "ans": ans]]
            socket?.emit(REQ, param)
        }
    }   
    
    //Revecing handlers
    func addHandlers() {        
        //This will get all All New and update friends object
        if(isSocketConnected()) {
            socket?.on(RES, callback: { (data, ack) in
                print("Updated message:::: \(data)")

                let json = JSON(data.first!)
                let en = json.getString(key: .en)
                if en == SocketInit {
                    _ = json.getDictionary(key: .data)
                }
                else if en == SocketChatUsersList {
                    let dataNew = json.getDictionary(key: .data)
                    print(dataNew)
                    self.delegate?.reloadChatUser(response: dataNew)
                }
                else if en == SocketAddQuestions {
                    self.chatUsersList()
                }
                else if en == SocketSingleChatDetails {
                    let dataNew = json.getDictionary(key: .data)
                    self.delegate?.reloadSingleChatDetails(response: dataNew)
                }
                else if en == SocketChatList {
                    let data = json.getDictionary(key: .data)
                    let dataNew = data.getArray(key: .data)
                    self.delegate?.reloadChatList(response: dataNew)
                }
                else if en == SocketSendMsg {
                    let data = json.getDictionary(key: .data)
                    let dataNew = data.getDictionary(key: .data)
                    self.delegate?.reloadSendChat(response: dataNew)
                }
                else if en == SocketOnlineOffline {
                    let data = json.getDictionary(key: .data)
                    self.delegate?.reloadOnline(response: data)
                }
                else if en == SocketBlockorunblock {
                    let data = json.getDictionary(key: .data)
                    if data["blocked"].stringValue == "1" {
                        ApiManager.shared.redirectToLoginScreen()
                    }
                }
                else if en == SocketUnreadNoUniqMsg {
                    let data = json.getDictionary(key: .data)
//                    print(data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showBadgeCount"), object: data)
//                    if self.delegate != nil {
//                        self.delegate?.reloadUnreadMsg(response: data)
//                    }
//                    else {
//                        self.delegateUnreadMsg?.SockerConnectionUnreadMsg(response: data)
//                    }
                }
                else if en == SocketAddUserToChat {
                    print(json)
                    let data = json.getDictionary(key: .data)
                    let dataNew = data.getDictionary(key: .data)
                    print(dataNew)
                    self.delegate?.reloadMatchUser(response: dataNew)
                }
                else{
                    print("Updated message:::: \(data)")
                }
            })
        }
        else {
            print("Socket not connected")
        }
    }
}
