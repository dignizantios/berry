//
//  ImagesGridCollectionViewCell.swift
//  testingTinderSwipe
//
//  Created by om on 10/13/18.
//  Copyright © 2018 Nicky. All rights reserved.
//

import UIKit

class ImagesGridCollectionViewCell: UICollectionViewCell {

    @IBOutlet var collectionViewGrid : UICollectionView!
    @IBOutlet var vwInsta : UIView!
    @IBOutlet weak var vwBlur: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        vwInsta.layer.cornerRadius = vwInsta.frame.size.height/2
        vwInsta.layer.masksToBounds = true
        
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.prominent)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imgUser.bounds
        blurEffectView.alpha = 0.6
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imgUser.addSubview(blurEffectView)
        
    }

}

