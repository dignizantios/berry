//
//  GalleryImage.swift
//  testingTinderSwipe
//
//  Created by om on 10/20/18.
//  Copyright © 2018 Nicky. All rights reserved.
//

import Foundation
import UIKit

class GalleryImage: UIView {
    
    @IBOutlet var imgvwGallery : UIImageView!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var vwInsta : UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        vwInsta.layer.cornerRadius = vwInsta.frame.size.height/2
        vwInsta.layer.masksToBounds = true
    }
}
