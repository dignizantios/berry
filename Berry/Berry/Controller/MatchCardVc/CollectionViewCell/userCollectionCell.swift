//
//  userCollectionCell.swift
//  Berry
//
//  Created by YASH on 31/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class userCollectionCell: UICollectionViewCell {

    //MARK: - @IBOutlet
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var ConstantimgHeight: NSLayoutConstraint!
    @IBOutlet weak var constantImgWidth: NSLayoutConstraint!
    @IBOutlet weak var vwGreenOnline: CustomView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var constantImgTop: NSLayoutConstraint!
    
    //MARK: - Variable

    
    //MARK: - AwakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
