//
//  UIView+Extension.swift
//  Liber
//
//  Created by YASH on 24/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0);
        image.draw(in: CGRect(origin: .zero, size: CGSize(width: targetSize.width, height: targetSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
    }
    
    func roundCornersWithShadow(corners: UIRectCorner, radius: CGFloat) {
        let mask = CAShapeLayer()
        mask.shadowColor = UIColor.black.cgColor
        mask.shadowOffset = CGSize(width: 10.0, height: 10.0)
        mask.shadowOpacity = 1.0
        mask.shadowRadius = 20
        
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        mask.path = path.cgPath
        layer.mask = mask
    }    
}
