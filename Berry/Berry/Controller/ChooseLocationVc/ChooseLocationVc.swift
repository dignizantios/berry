//
//  ChooseLocationVc.swift
//  Berry
//
//  Created by YASH on 20/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlacePicker

class ChooseLocationVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var gooleMapVw: GMSMapView!
    @IBOutlet weak var txtAddress: CustomTextField!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCurrentLocation: UIButton!
    
    @IBOutlet weak var viewBGAutocomplete: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint! // 0
    let clearButton = UIButton(frame: CGRect(x: 0, y: 0, width: 16, height: 16))

    //MARK: - Variables
//    var tableDataSource = GMSAutocompleteTableDataSource()
//    var fetcher: GMSAutocompleteFetcher?
    
    var selectedCoordinate : CLLocationCoordinate2D?
    var selectedAddress : String = ""
    var searchResult:[GMSAutocompletePrediction] = []
    
    let placesClient = GMSPlacesClient()
    let filter = GMSAutocompleteFilter()
    var isSelectedText = false
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long:userCurrentLocation?.coordinate.longitude ?? 0.0, flag: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Choose_Location_key"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "addAddressInProfile"), object: nil)
    }
    
    func setupUI() {
        
        animateTableView(isHidden: true, duration: 0.0)
        txtAddress.font = themeFont(size: 16, fontname: .regular)
        txtAddress.placeholder = getCommonString(key: "Enter_address_key")
        txtAddress.delegate = self
        txtAddress.clearButtonMode = .whileEditing
        gooleMapVw.delegate = self
        
        // Set up the autocomplete filter.
//        let filter = GMSAutocompleteFilter()
//        filter.type = .establishment
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = 50.0
        tableView.register(UINib.init(nibName: "PlaceResultTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceResultTableViewCell")
        txtAddress.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.initialize()
    }
    
    func initialize() {
        clearButton.setImage(UIImage(named: "ic_clear")!, for: [])
        
        txtAddress.rightView = clearButton
        clearButton.addTarget(self, action: #selector(clearClicked), for: .touchUpInside)
        
        txtAddress.clearButtonMode = .never
        txtAddress.rightViewMode = .whileEditing
    }
    
    @objc func clearClicked(sender:UIButton)
    {
        txtAddress.text = ""
        clearButton.isHidden = true
        animateTableView(isHidden: true, duration: 0.5)
    }
    
    func animateTableView(isHidden:Bool,duration:Double) {
        if isHidden {
            constraintTableViewHeight.constant = 0
        } else {
            viewBGAutocomplete.isHidden = isHidden
            var height:CGFloat = 0.0
            if Device.IS_IPHONE {
                if Device.IS_IPHONE_5_OR_LESS {
                    height = searchResult.count > 3 ? CGFloat(50*3) : CGFloat(searchResult.count * 50)
                } else {
                    height = searchResult.count > 5 ? CGFloat(50*5) : CGFloat(searchResult.count * 50)
                }
            } else {
                height = searchResult.count > 3 ? CGFloat(50*3) : CGFloat(searchResult.count * 50)
            }
            constraintTableViewHeight.constant = height
        }
        UIView.animate(withDuration: duration, animations: {
            self.viewBGAutocomplete.layoutIfNeeded()
        }) { (completion) in
            if isHidden {
                self.viewBGAutocomplete.isHidden = isHidden
            }
        }
    }
    
    func fetchResult(strSearch:String) {
        placesClient.autocompleteQuery(strSearch, bounds: nil, filter: nil) { [unowned self] (result, error) in
            print("error:=",error == nil ? "empty error" : error!.localizedDescription)
            self.searchResult.removeAll()
            if let dataList = result {
                self.searchResult = dataList
            }
            print("self.searchResult.count:=",self.searchResult.count)
            self.animateTableView(isHidden: self.searchResult.count == 0, duration: 0.5)
            self.tableView.reloadData()
        }
    }
    
    func fetchLatLongFrom(predication:GMSAutocompletePrediction) {
        if predication.placeID != nil && predication.placeID != "" {
            let placeID = predication.placeID
            placesClient.lookUpPlaceID(placeID) { [weak self] (place, error) in
                guard let placeData = place else { return }
                let locationCoordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(placeData.coordinate.latitude, placeData.coordinate.longitude)
                
                self?.txtAddress.resignFirstResponder()
                let addressFrom = predication.attributedFullText.string
                //                predication.attributedPrimaryText.string + (predication.attributedSecondaryText == nil ? "" : predication.attributedSecondaryText!.string)
                self?.selectedAddress = addressFrom
                self?.selectedCoordinate = locationCoordinate
                self?.txtAddress.text = addressFrom
                self?.isSelectedText = true
                self?.setUpPinOnMapAccodtingToSelected(lat: self?.selectedCoordinate?.latitude ?? 0.0, long: self?.selectedCoordinate?.longitude ?? 0.0)
                //            self?.setUpPinOnMap(lat: self?.selectedCoordinate?.latitude ?? 0.0, long:self?.selectedCoordinate?.longitude ?? 0.0, flag: 0)
                self?.animateTableView(isHidden: true, duration: 0.5)
            }
        }

    }
}

extension ChooseLocationVc:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceResultTableViewCell") as! PlaceResultTableViewCell
        cell.setTheData(theData: searchResult[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchData = searchResult[indexPath.row]
        fetchLatLongFrom(predication: searchData)
    }
}

extension ChooseLocationVc {
    //MARK: - IBAction
    @IBAction func btnCurrentLocationTapped(_ sender: UIButton) {
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long:userCurrentLocation?.coordinate.longitude ?? 0.0, flag: 0)
    }
    
    @IBAction func btnDoneAddressSelectionTapped(_ sender: UIButton) {
        if txtAddress.text?.count != 0 {
            //            let array: NSArray = [txtAddress.text!, self.selectedCoordinate?.longitude ?? 0.0, selectedCoordinate?.latitude ?? 0.0]
            let array: NSArray = [self.selectedAddress, self.selectedCoordinate?.longitude ?? 0.0, selectedCoordinate?.latitude ?? 0.0]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addAddressInProfile"), object: array)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onBtnCloseTap(_ sender:UIButton) {
        animateTableView(isHidden: true, duration: 0.5)
    }
    
}

extension ChooseLocationVc {
    func setUpPinOnMap(lat:Double,long:Double,flag : Int) {
        if flag == 0 {   // Load time (First time Screen)
            let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
            self.gooleMapVw.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 10)
            let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: 10)
            self.gooleMapVw.animate(with: updateCamera)
            self.txtAddress.text = reverseGeoCoding(Latitude: lat, Longitude: long)
        }
        else if flag == 1 {  // From location PinSetup
            //            sourceMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            //            sourceMarker.title = txtFrom.text
            //            sourceMarker.icon = UIImage(named: "dot_source_location")
            //            sourceMarker.map = vwMap
            //
        }
        else if flag == 2 {   // To location PinSetup
            //            destinationMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            //            destinationMarker.title = txtTo.text
            //            destinationMarker.icon = UIImage(named: "dot_destination_location")
            //            destinationMarker.map = vwMap
        }
    }
    func setUpPinOnMapAccodtingToSelected(lat:Double,long:Double) {
        let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.gooleMapVw.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 10)
        let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: 10)
        self.gooleMapVw.animate(with: updateCamera)
    }
}

extension ChooseLocationVc: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.clearButton.isHidden = textField.text == "" ? true : false
//        textField.selectAll(self)
//        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       /* if textField == txtAddress {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            UINavigationBar.appearance().barTintColor = UIColor.appThemePurpleColor
            UINavigationBar.appearance().tintColor = UIColor.white
            let searchBarTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
            present(autocompleteController, animated: true, completion: nil)
            return false
        }*/
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        self.clearButton.isHidden = textField.text == "" ? true : false

        let text = textField.text!.toTrim()
        if text.count > 1 {
            fetchResult(strSearch: textField.text!)
//            animateTableView(isHidden: false, duration: 0.5)
        } else {
            animateTableView(isHidden: true, duration: 0.5)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}

//MARK: - Google placePicker

extension ChooseLocationVc: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let locationCoordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
        selectedCoordinate = locationCoordinate
        
        let addressFrom = place.formattedAddress?.components(separatedBy: ", ").joined(separator: ",")
        self.txtAddress.text = addressFrom
        setUpPinOnMap(lat: selectedCoordinate?.latitude ?? 0.0, long:selectedCoordinate?.longitude ?? 0.0, flag: 0)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        //   self.viewContainer.isHidden = true
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines
                currentAddress = (lines?.joined(separator: ",") ?? "")
                self.txtAddress.text = currentAddress
                print("country=\(address.country ?? "")")
                print("locality=\(address.locality ?? "")")
                print("locality=\(address.subLocality ?? "")")
                if address.locality ?? "" != "" && address.country ?? "" != "" {
                    self.selectedAddress = address.locality! + ", " + address.country!
                }
                else {
                    if address.subLocality ?? "" != "" && address.country ?? "" != "" {
                        self.selectedAddress = address.subLocality! + ", " + address.country!
                    }
                    else {
                        if address.country ?? "" != "" {
                            self.selectedAddress = address.country!
                        }
                    }
                }
            }
        }
        return currentAddress
    }
}

extension ChooseLocationVc : GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.view.endEditing(true)
        if isSelectedText {
            isSelectedText = false
            return
        }
        selectedCoordinate = CLLocationCoordinate2D(latitude: position.target.latitude, longitude: position.target.longitude)
        // selectedLocation.latitude  = position.target.latitude
        //  selectedLocation.longitude  = position.target.longitude
        self.txtAddress.text = reverseGeoCoding(Latitude: position.target.latitude, Longitude: position.target.longitude)
    }
}
