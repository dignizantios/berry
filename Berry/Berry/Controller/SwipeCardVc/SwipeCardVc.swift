 //
 //  SwipeCardVc.swift
 //  Berry
 //
 //  Created by YASH on 31/10/18.
 //  Copyright © 2018 YASH. All rights reserved.
 //
 
 
 let  MAX_BUFFER_SIZE = 3;
 let  SEPERATOR_DISTANCE = 0;
 let  TOPYAXIS = 75;
 
 import UIKit
 import SwiftyJSON
 import CarbonKit
 import InMobiSDK
 
 var IM_AD_INSERTION_POSITION = 10
 var IM_AD_INSERTION_POSITION_TOTAL = 10

 class SwipeCardVc: UIViewController, SuperLikeDelegate,ChangeIndexMatchDelegate, IMNativeDelegate {
    
    //MARK: @IBOutlet
    @IBOutlet weak var emojiView: UIView!
    @IBOutlet weak var viewTinderBackGround: UIView!
    @IBOutlet weak var buttonUndo: UIButton!
    @IBOutlet weak var viewActions: UIView!
    @IBOutlet weak var vwBotttomButton: UIView!
    @IBOutlet weak var btnUndo: UIButton!
    
    @IBOutlet weak var lblSearchSetting: UILabel!
    //MARK: Vaiables
    var currentIndex = 0
    var currentPage = 1
    var currentLoadedCardsArray = [TinderCard]()
    var allCardsArray = [TinderCard]()
    var response : [JSON] = [JSON]()
    var isReload: Bool = true
    var isSuperLike: Bool = false
    var SuperLikeMessage: String = ""
    var isDiscoverUser = false
    var userList:[String] = [String]()
    var isLoaded:Bool = false

    @IBOutlet weak var btnDisLike: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    
    weak var delegate: ChangeIndexCarbonkitDelegate?
    weak var delegateSwipe:ChangeIndexMatchDelegate?
    var addedCardCount = 0

    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
     
        viewActions.alpha = 0.0
        buttonUndo.alpha = 0
        self.lblSearchSetting.underline()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshView(notification:)), name: NSNotification.Name(rawValue: "refreshView"), object: nil)
        self.resetDetails()
        if !isKeyPresentInUserDefaults(key: USER_INFO_KEY) {
            let obj: InformationVC = self.storyboard?.instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
            obj.modalPresentationStyle = .overCurrentContext
            self.present(obj, animated: false, completion: nil)
            UserDefaults.standard.set("yes", forKey: USER_INFO_KEY)
            UserDefaults.standard.synchronize()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        if isReload {
//            self.resetDetails()
//        }
//        self.isReload = true
    }
    
    @objc func refreshView(notification : Notification) {
        if SELECTED_TAB == 1 {
            if !SocketIOHandler.shared.isSocketConnected() {
                SocketIOHandler.shared.Connect()
            }
            self.addedCardCount = 0
            if !loadJSON(key: USER_DETAILS_KEY).isEmpty {
                let json = loadJSON(key: USER_DETAILS_KEY)
                print(json)
                let myProfile = json.getDictionary(key: .myprofile)
                
                let accountStatus = myProfile.getString(key: .acountStatus).uppercased()
                IM_AD_INSERTION_POSITION = accountStatus == "N" ? IM_AD_INSERTION_POSITION_TOTAL : 0
                
//                let aedate = myProfile.getString(key: .aedate)
//                if aedate != "" {
//                    let date = convertDate(str: aedate)
//                    if date > Date() {
//                        IM_AD_INSERTION_POSITION = 0
//                    }
//                    else {
//                        IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                    }
//                }
//                else {
//                    IM_AD_INSERTION_POSITION = IM_AD_INSERTION_POSITION_TOTAL
//                }
            }
            
            self.resetDetails()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func ChangeIndexMatchFinish() {
        self.delegateSwipe?.ChangeIndexMatchFinish()
    }
    
    func resetDetails() {
        self.view.layoutIfNeeded()
        self.response.removeAll()
        for subview in self.viewTinderBackGround.subviews {
            if let view = subview.viewWithTag(999) {
                view.removeFromSuperview()
            }
        }
        currentIndex = 0
        currentPage = 1
        currentLoadedCardsArray.removeAll()
        allCardsArray.removeAll()
        if currentIndex == self.response.count
        {
            vwBotttomButton.isHidden = true
        }
        else
        {
            vwBotttomButton.isHidden = false
        }
        isDiscoverUser = true
        self.getDiscoverMyDistance(page: 1, progress: !self.isLoaded)
        self.btnUndo.isUserInteractionEnabled = false
    }
    
    //    @objc func animateEmojiView(timer : Timer){
    //        let sender = timer.userInfo as! UIView
    //        emojiView.rateValue =  emojiView.rateValue + 0.2
    //        if sender.rateValue >= 5 {
    //            timer.invalidate()
    //            emojiView.rateValue = 2.5
    //        }
    //    }
    
    //MARK: Functions
    func loadCardValues(json: [JSON]) {
        if json.count > 0 {
//            self.currentPage += 1
            let capCount = (json.count > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : json.count
            
            for (i,value) in json.enumerated() {
                let newCard = createTinderCard(at: i,value: "", json: value)
                newCard.tag = 999
                allCardsArray.append(newCard)
                if i < capCount {
                    currentLoadedCardsArray.append(newCard)
                }
            }
            
            for (i,_) in currentLoadedCardsArray.enumerated() {
                if i > 0 {
                    viewTinderBackGround.insertSubview(currentLoadedCardsArray[i], belowSubview: currentLoadedCardsArray[i - 1])
                }else {
                    viewTinderBackGround.addSubview(currentLoadedCardsArray[i])
                }
            }
            animateCardAfterSwiping()
            //perform(#selector(loadInitialDummyAnimation), with: nil, afterDelay: 1.0)
        }
    }
    
    @objc func loadInitialDummyAnimation() {
        let dummyCard = currentLoadedCardsArray.first;
        dummyCard?.shakeAnimationCard()
        UIView.animate(withDuration: 1.0, delay: 2.0, options: .curveLinear, animations: {
            self.viewActions.alpha = 1.0
        }, completion: nil)
        //        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.animateEmojiView), userInfo: emojiView, repeats: true)
    }
    
    func createTinderCard(at index: Int , value :String, json: JSON) -> TinderCard {
        
        var heightofView : CGFloat = 0
        if UIScreen.main.bounds.height <= 736 // Below iphone X
        {
            heightofView = UIScreen.main.bounds.height - 180
        }
        else
        {
            heightofView = UIScreen.main.bounds.height - 250
        }
        
        let card = TinderCard(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-20 , height: heightofView), value: value, json: json, count: self.response.count, atIndex: index,superVC: self)
        card.delegate = self
        return card
    }
    
    func removeObjectAndAddNewValues() {
        
        //        emojiView.rateValue =  2.5
        UIView.animate(withDuration: 0.5) {
            self.buttonUndo.alpha = 0
        }
        
        currentLoadedCardsArray.remove(at: 0)
        
        //TODO: - Yash changes
        if currentIndex == self.response.count - 1
        {
            vwBotttomButton.isHidden = true
        }
        else
        {
            vwBotttomButton.isHidden = false
        }
        //
        currentIndex = currentIndex + 1
        Timer.scheduledTimer(timeInterval: 1.01, target: self, selector: #selector(enableUndoButton), userInfo: currentIndex, repeats: false)
        
        if (currentIndex + currentLoadedCardsArray.count) < allCardsArray.count {
            let card = allCardsArray[currentIndex + currentLoadedCardsArray.count]
            var frame = card.frame
            frame.origin.y = CGFloat(MAX_BUFFER_SIZE * SEPERATOR_DISTANCE)
            card.frame = frame
            currentLoadedCardsArray.append(card)
            viewTinderBackGround.insertSubview(currentLoadedCardsArray[MAX_BUFFER_SIZE - 1], belowSubview: currentLoadedCardsArray[MAX_BUFFER_SIZE - 2])
        }
        print(currentLoadedCardsArray.count)
        if currentLoadedCardsArray.count == 0 {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                self.getDiscoverMyDistance(page: self.currentPage, progress: true)
            }
        }
        animateCardAfterSwiping()
    }
    
    func animateCardAfterSwiping() {
        for (i,card) in currentLoadedCardsArray.enumerated() {
            
            UIView.animate(withDuration: 0.5, animations: {
                if i == 0 {
                    card.isUserInteractionEnabled = true
                }
                
                var frame = card.frame
                frame.origin.y = CGFloat(i * SEPERATOR_DISTANCE)
                card.frame = frame
            })
        }
    }
    
    //MARK: @IBAction
    
    @IBAction func disLikeButtonAction(_ sender: Any) {
        self.btnDisLike.isUserInteractionEnabled = false
        let card = currentLoadedCardsArray.first
        card?.downAction(time: 0.8)
    }
    
    @IBAction func LikeButtonAction(_ sender: Any) {
        self.btnLike.isUserInteractionEnabled = false
        let card = currentLoadedCardsArray.first
        card?.upAction(time: 0.8)
    }
    
    @IBAction func btnSearchSettingClicked(_ sender: Any) {        
        self.delegate?.ChangeIndexCarbonkit(index: 0)
    }
    
    @IBAction func superLikeBerryButtonActionTapped(_ sender: UIButton) {
        if currentLoadedCardsArray.count != 0 {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SuperLikePopupVc") as! SuperLikePopupVc
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.delegate = self
            //        obj.oppUserId = self.selectedUser.getString(key: .userid)
            self.present(obj, animated: false, completion: nil)
        }
    }
    
    @IBAction func undoButtonAction(_ sender: Any) {
        self.btnUndo.isUserInteractionEnabled = false
        //TODO: - Yash Changes
        currentIndex =  currentIndex - 1
        print(allCardsArray.indices.contains(currentIndex))
        guard allCardsArray.indices.contains(currentIndex) else {
            currentIndex =  currentIndex + 1
            return
        }
        
        
        if currentLoadedCardsArray.count == MAX_BUFFER_SIZE {
            let lastCard = currentLoadedCardsArray.last
            lastCard?.rollBackCard()
            currentLoadedCardsArray.removeLast()
        }
        
        
        let undoCard = allCardsArray[currentIndex]
        undoCard.layer.removeAllAnimations()
        viewTinderBackGround.addSubview(undoCard)
        undoCard.makeUndoAction()
        currentLoadedCardsArray.insert(undoCard, at: 0)
        animateCardAfterSwiping()
        if currentIndex == 0 {
            UIView.animate(withDuration: 0.5) {
                self.buttonUndo.alpha = 0
            }
        }
        
        let card = currentLoadedCardsArray.first
        card?.makeUndoAction()
        let data = self.currentLoadedCardsArray.first?.json
        DispatchQueue.main.async {
            let isAds = data!.getBool(key: .isAds)
            if !isAds {
                self.actionDiscovered(opp: (card?.json.getString(key: .userid))!, type: .UNDO)
            }
        }
    }
    
    @objc func enableUndoButton(timer: Timer){
        let cardIntex = timer.userInfo as! Int
        if (currentIndex == cardIntex) {
            
            UIView.animate(withDuration: 0.5) {
                self.buttonUndo.alpha = 1.0
            }
        }
    }
    
    func SuperLikeFinish(userid: String, text: String) {
        self.isSuperLike = true
        self.SuperLikeMessage = text
        let card = currentLoadedCardsArray.first
        card?.upAction(time: 0.8)
    }
 }
 
 extension SwipeCardVc : TinderCardDelegate{
    func superLikeSelected() {
        let data = self.currentLoadedCardsArray.first?.json

        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SuperLikeMessagePopupVC") as! SuperLikeMessagePopupVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.message = (data?.getString(key: .message))!        
        self.present(obj, animated: false, completion: nil)
    }
    
    func imageSelected(index: Int, images: [JSON]) {
        self.isReload = false
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ShowMultiImagesVc") as! ShowMultiImagesVc
//        obj.delegateCropImage = self
        obj.selectedController = .forMatchCardShowPhotoHeader
        obj.arrSocialMediaImagesMutable = images
        obj.currentImageIndex = index
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func cardGoesUp(card: TinderCard) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.btnUndo.isUserInteractionEnabled = true
            self.btnLike.isUserInteractionEnabled = true
        }
        let data = self.currentLoadedCardsArray.first?.json
        let type = self.isSuperLike ? TYPE_ACTION.SUPER_LIKE : TYPE_ACTION.LIKE
        DispatchQueue.main.async {
            let isAds = data!.getBool(key: .isAds)
            if !isAds {
                self.actionDiscovered(opp: (data?.getString(key: .userid))!, type: type)
            }
        }
        removeObjectAndAddNewValues()
    }
    
    func cardGoesDown(card: TinderCard) {
        self.btnDisLike.isUserInteractionEnabled = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.btnUndo.isUserInteractionEnabled = true
        }
        let data = self.currentLoadedCardsArray.first?.json
        
        DispatchQueue.main.async {
            let isAds = data!.getBool(key: .isAds)
            if !isAds {
                self.actionDiscovered(opp: (data?.getString(key: .userid))!, type: .DISLIKE)
            }
        }
        removeObjectAndAddNewValues()
    }
    
    // action called when the card goes to the left.
    func cardGoesLeft(card: TinderCard) {
        
        //TODO: - YASH Changes
        //        removeObjectAndAddNewValues()
    }
    // action called when the card goes to the right.
    func cardGoesRight(card: TinderCard) {
        //TODO: - YASH Changes
        //        removeObjectAndAddNewValues()
    }
    
    func currentCardStatus(card: TinderCard, distance: CGFloat) {
        
        if distance == 0 {
            //            emojiView.rateValue =  2.5
        }else{
//            let value = Float(min(fabs(distance/100), 1.0) * 5)
//            let sorted = distance > 0  ? 2.5 + (value * 5) / 10  : 2.5 - (value * 5) / 10
            //            emojiView.rateValue =  sorted
        }
    }    
    
    func getDiscoverMyDistance(page: Int = 1, progress: Bool = true) {
        self.response.removeAll()
        self.allCardsArray.removeAll()
        self.btnUndo.isUserInteractionEnabled = false
        currentIndex = 0
        let location = jsonString(from: [AppDelegate.shared.longitude,AppDelegate.shared.lattitude])
        
        let param = ["userid": userDetails.getString(key: .userid),"page": page, "location": "\"" + location! + "\"", "temp": self.userList] as [String : Any]
        ApiManager.shared.MakePostAPI(name: DISCOVER_MY_DISTANCE, params: param, progress: progress, vc: self) { (result, error) in
            self.isLoaded = true
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                    let data = json.getArray(key: .data)
                    if data.count < 10 {
                        self.isDiscoverUser = false
                    }
                    
                    for json in data {
                        print(self.addedCardCount)
                        self.vwBotttomButton.isHidden = false
                        
                        self.addedCardCount += 1
                        var result = json
                        result["isAds"] = false
                        self.response.append(result)
                        
                        if self.addedCardCount == IM_AD_INSERTION_POSITION {
                            var result = JSON()
                            result["isAds"] = true
                            self.response.append(result)
                            self.addedCardCount = 0
                        }
                    }
                    print(self.response)
                    self.loadCardValues(json: self.response)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.getDiscoverMyDistance(page: page, progress: progress)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func actionDiscovered(opp: String, type: TYPE_ACTION) {
        self.userList.append(opp)
        var param = [String : Any]()
        if self.isSuperLike {
            param = ["userid": userDetails.getString(key: .userid),"opp": opp, "type": type.rawValue, "message": self.SuperLikeMessage] as [String : Any]
        }
        else {
            param = ["userid": userDetails.getString(key: .userid),"opp": opp, "type": type.rawValue] as [String : Any]
        }
        self.isSuperLike = false
        print(param)
        ApiManager.shared.MakePostAPI(name: ACTION_DESCOVERED, params: param, progress: false, vc: self) { (result, error) in
            if result != nil {
                
                for (index, user) in self.userList.enumerated() {
                    if user == opp {
                        if self.userList.count > index {
                            self.userList.remove(at: index)
                        }
                    }
                }
                
                let json = JSON(result!)
                print(json)
                let error = json.getString(key: .error)
                if error != YES {
                    let data = json.getDictionary(key: .data)
                    print(data)
                    let match = data.getBool(key: .match)
                    print(match)
                    if match {
                        let profileDetails = data.getDictionary(key: .profileDetails)
                        let username = profileDetails.getString(key: .username)
                        let profile = profileDetails.getArray(key: .profile)

                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ItsMatchVC") as! ItsMatchVC
                        obj.userName = username
                        obj.profile = profile.first?.string ?? ""
                        obj.delegate = self
                        obj.modalPresentationStyle = .overFullScreen
                        self.present(obj, animated: false, completion: nil)
                    }
         
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                for (index, user) in self.userList.enumerated() {
                    if user == opp {
                        if self.userList.count > index {
                            self.userList.remove(at: index)
                        }
                    }
                }
                self.actionDiscovered(opp: opp, type: type)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
 }

 
 extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
    
    func underline(range:NSRange) {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
            attributedText = attributedString
        }
    }
 }
