//
//  ItsMatchVC.swift
//  Berry
//
//  Created by Haresh Bhai on 10/05/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit


protocol ChangeIndexMatchDelegate: class {
    func ChangeIndexMatchFinish()
}
class ItsMatchVC: UIViewController {

    @IBOutlet weak var imgBackground: UIImageView!    
    @IBOutlet weak var lblUser: UILabel!
    
    @IBOutlet weak var btnKeepSwipe: UIButton!
    @IBOutlet weak var btngoToMatch: UIButton!
    
    var userName:String = ""
    var profile:String = ""

    weak var delegate:ChangeIndexMatchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgBackground.image = UIImage(named: "Lucas")
        self.lblUser.text = userName.capitalized + " likes you too!"
        btnKeepSwipe.layer.cornerRadius = btnKeepSwipe.bounds.height / 2
        btnKeepSwipe.addShadow(offset: CGSize(width: 0.1, height: 0.1))
        btngoToMatch.layer.cornerRadius = btngoToMatch.bounds.height / 2
        btngoToMatch.addShadow(offset: CGSize(width: 0.1, height: 0.1))        
        self.imgBackground.sd_setImage(with: URL(string: profile), completed: nil)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btngoToMatchClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.ChangeIndexMatchFinish()
    }
    
    @IBAction func btnKeepSwipingClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
