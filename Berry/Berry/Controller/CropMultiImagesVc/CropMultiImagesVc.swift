//
//  CropMultiImagesVc.swift
//  Berry
//
//  Created by YASH on 19/11/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import TOCropViewController
import SwiftyJSON


enum checkParentControllerForCropImagesVc {
    case forAddPhotoFromDevicePhotos
    case forProfileShowPhoto
    case forMatchCardShowPhotoHeader
    case forRegistration
    case forMatchCardShowInstagramPhotos
    case forAddPhotoFromSocialMedia
}

protocol CropImagesDelegate {
    func setCropImagesInCollection(arrayOfImages : [UIImage])
    func setCropImagesWithUrlInCollection(arrayOfImages : NSMutableArray)
}

class CropMultiImagesVc: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var collectionSelectedImages: UICollectionView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var pageControlBottom: UIPageControl!
    @IBOutlet weak var lblPhotosCount: UILabel!
    
    //MARK: - Variable
    var arrSelectedImageURLFromSocialMedia = [URL]() // Social media images URL
    var arrSocialMediaImages : [JSON] = []
    var arrSocialMediaImagesMutable = NSMutableArray()
    var delegateCropImage : CropImagesDelegate?
    var currentImageIndex = 0
    var selectedController = checkParentControllerForCropImagesVc.forAddPhotoFromDevicePhotos
    var userId : String = ""

    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSelectedImages.register(UINib(nibName: "cropMultiImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "cropMultiImageCollectionCell")
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupUI() {
        lblPhotosCount.textColor = UIColor.white
        lblPhotosCount.font = themeFont(size: 15, fontname: .medium)
        lblPhotosCount.isHidden = true
        
        if selectedController == .forAddPhotoFromDevicePhotos {
            [btnCrop,btnDelete,btnDone].forEach { (btn) in
                btn?.isHidden = false
            }
        }
        else if selectedController == .forProfileShowPhoto {
            [btnCrop,btnDone].forEach { (btn) in
                btn?.isHidden = true
            }
            btnDelete.isHidden = false
            
            if(arrSocialMediaImagesMutable.count > 0) {
                collectionSelectedImages.reloadData()
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
                
                pageControlBottom.currentPage = currentImageIndex
                collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        else if selectedController == .forMatchCardShowPhotoHeader {
            [btnDone,btnDelete,btnCrop].forEach { (btn) in
                btn?.isHidden = true
            }
            
            collectionSelectedImages.reloadData()
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
            
            pageControlBottom.currentPage = currentImageIndex
            collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
        else if selectedController == .forRegistration {
            [btnDelete,btnDone].forEach { (btn) in
                btn?.isHidden = false
            }
            btnCrop.isHidden = true
            if(arrSocialMediaImagesMutable.count > 0) {
                collectionSelectedImages.reloadData()
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
                pageControlBottom.currentPage = currentImageIndex
                collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        else if selectedController == .forMatchCardShowInstagramPhotos {
            [btnDone,btnDelete,btnCrop].forEach { (btn) in
                btn?.isHidden = true
            }
            
            lblPhotosCount.isHidden = false
            pageControlBottom.isHidden = true
            lblPhotosCount.text = "\(currentImageIndex + 1) / \(arrSocialMediaImagesMutable.count)"
            collectionSelectedImages.reloadData()
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
            collectionSelectedImages.scrollToItem(at: IndexPath(item: currentImageIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
        else if selectedController == .forAddPhotoFromSocialMedia {
            for i in 0..<self.arrSelectedImageURLFromSocialMedia.count {
                var dict = JSON()
                dict["imageURL"].url = self.arrSelectedImageURLFromSocialMedia[i]
                dict["isImage"] = "0"  // 0 for url and 1 for image
                self.arrSocialMediaImages.append(dict)
                
                let dictInner = (arrSocialMediaImages[i].dictionaryObject)! as NSDictionary
                let dictOuter = dictInner.mutableCopy()
                arrSocialMediaImagesMutable.add(dictOuter)
            }
            self.collectionSelectedImages.reloadData()
        }
    }
}

extension CropMultiImagesVc {
    //MARK : - @IBAction
    @IBAction func btnBackTappedAction(_ sender: Any) {
        
        if selectedController == .forAddPhotoFromDevicePhotos {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is PhotoPickerVc {
                    arrSocialMediaImagesMutable = []
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
        else if selectedController == .forProfileShowPhoto {
            arrSocialMediaImagesMutable = []
            self.navigationController?.popViewController(animated: true)
        }
        else if selectedController == .forMatchCardShowPhotoHeader {
            self.navigationController?.popViewController(animated: true)
        }
        else if selectedController == .forRegistration {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is SignUpVc {
                    arrSocialMediaImagesMutable = []
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
        else if selectedController == .forMatchCardShowInstagramPhotos {
            self.navigationController?.popViewController(animated: true)
        }
        else if selectedController == .forAddPhotoFromSocialMedia {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnCropTappedAction(_ sender: UIButton) {
        let arrCurrentIndex = collectionSelectedImages.indexPathsForVisibleItems
        let currentIndex = arrCurrentIndex[0]
        currentImageIndex = currentIndex.row
        
        var cropVC = TOCropViewController()
       
        let indexPath = IndexPath(item: currentImageIndex, section: 0)
        
        if let cell = collectionSelectedImages.cellForItem(at: indexPath) as? cropMultiImageCollectionCell {
            let dict = self.arrSocialMediaImagesMutable[currentImageIndex] as! NSDictionary
            
            cell.imgSelectedPhotos.sd_setShowActivityIndicatorView(true)
            cell.imgSelectedPhotos.sd_setIndicatorStyle(.gray)
            
            if dict["isImage"] as! String == "0" {
                let imgURL = URL(string: dict["imageURL"] as! String)
                cell.imgSelectedPhotos.sd_setImage(with:imgURL, placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
                cropVC = TOCropViewController(image: cell.imgSelectedPhotos.image!)
            }
            else {
                cropVC = TOCropViewController(image: cell.imgSelectedPhotos.image!)
            }
        }
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        cropVC.modalPresentationStyle = .overFullScreen
        self.present(cropVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnDeleteTapped(_ sender: UIButton) {
        let arrCurrentIndex = collectionSelectedImages.indexPathsForVisibleItems
        let currentIndex = arrCurrentIndex[0]
        currentImageIndex = currentIndex.row
        
        if selectedController == .forAddPhotoFromDevicePhotos {
            if arrSocialMediaImagesMutable.count == 1 {
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                for aViewController in viewControllers {
                    if aViewController is PhotoPickerVc {
                        self.navigationController!.popToViewController(aViewController, animated: false)
                    }
                }
            }
            else {
                arrSocialMediaImagesMutable.removeObject(at: currentImageIndex)
                collectionSelectedImages.reloadData()
            }
        }
        else if selectedController == .forProfileShowPhoto {
            if arrSocialMediaImagesMutable.count <= 3 {
                self.showToast(message: "Minimum 3 images are required")
                return
            }
            
            let imageNew = arrSocialMediaImagesMutable[currentImageIndex] as! NSDictionary
            let isImage = imageNew.value(forKey: "isNew") as? String
            if isImage == "0" {
                self.deleteProfile(index: currentImageIndex)
            }
            arrSocialMediaImagesMutable.removeObject(at: currentImageIndex)
            self.delegateCropImage?.setCropImagesWithUrlInCollection(arrayOfImages: arrSocialMediaImagesMutable)
            collectionSelectedImages.reloadData()
            //            self.navigationController?.popViewController(animated: false)
        }
        else if selectedController == .forMatchCardShowPhotoHeader {
            // No Option of Delete
        }
        else if selectedController == .forRegistration {
            if arrSocialMediaImagesMutable.count <= 3 {
                self.showToast(message: "Minimum 3 images are required")
                return
            }
            
            if arrSocialMediaImagesMutable.count == 1 {
                let imageNew = arrSocialMediaImagesMutable[currentImageIndex] as! NSDictionary
                let isImage = imageNew.value(forKey: "isNew") as? String
                if isImage == "0" {
                    self.deleteProfile(index: currentImageIndex, isSave: false)
                }
                
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                for aViewController in viewControllers {
                    if aViewController is SignUpVc {
                        arrSocialMediaImagesMutable.removeObject(at: currentImageIndex)
                        self.delegateCropImage?.setCropImagesWithUrlInCollection(arrayOfImages: arrSocialMediaImagesMutable)
                        self.navigationController!.popToViewController(aViewController, animated: false)
                    }
                }
            }
            else {
                let imageNew = arrSocialMediaImagesMutable[currentImageIndex] as! NSDictionary
                let isImage = imageNew.value(forKey: "isNew") as? String
                if isImage == "0" {
                    self.deleteProfile(index: currentImageIndex, isSave: false)
                }
                
                arrSocialMediaImagesMutable.removeObject(at: currentImageIndex)
                collectionSelectedImages.reloadData()
            }
        }
        else if selectedController == .forAddPhotoFromSocialMedia {
            if arrSocialMediaImagesMutable.count == 1 {
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                for aViewController in viewControllers {
                    if aViewController is PhotoPickerVc {
                        
                        self.navigationController!.popToViewController(aViewController, animated: false)
                    }
                }
            }
            else {
                arrSocialMediaImagesMutable.removeObject(at: currentImageIndex)
                collectionSelectedImages.reloadData()
            }
        }
    }
    
    @IBAction func btnDoneSelectionTappedAction(_ sender: UIButton) {
        if selectedController == .forAddPhotoFromSocialMedia {
            self.delegateCropImage?.setCropImagesWithUrlInCollection(arrayOfImages: arrSocialMediaImagesMutable)
            self.navigationController?.popViewController(animated: false)
        }
        else {
            self.delegateCropImage?.setCropImagesWithUrlInCollection(arrayOfImages: arrSocialMediaImagesMutable)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func deleteProfile(index: Int, isSave: Bool = true) {
        let param = ["userid": self.userId == "" ? userDetails.getString(key: .userid) : self.userId, "index": index] as [String : Any]
        ApiManager.shared.MakePostAPI(name: DELETE_PROIFLE, params: param, vc: self) { (response, error) in
            if response != nil {
                let json = JSON(response!)
                let error = json.getString(key: .error)
                if error != YES {
                    if isSave {
                        saveJSON(j: json, key: USER_DETAILS_KEY)
                        userDetails = json
                    }
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }                
            else {
                self.deleteProfile(index: index, isSave: isSave)
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
}

extension CropMultiImagesVc : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControlBottom.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        lblPhotosCount.text = "\(pageControlBottom.currentPage + 1) / \(arrSocialMediaImagesMutable.count)"
    }
}


extension CropMultiImagesVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedController == .forAddPhotoFromSocialMedia {
            self.pageControlBottom.numberOfPages = arrSocialMediaImagesMutable.count
            return arrSocialMediaImagesMutable.count
        }
        else {
            self.pageControlBottom.numberOfPages = arrSocialMediaImagesMutable.count
            return arrSocialMediaImagesMutable.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cropMultiImageCollectionCell", for: indexPath) as! cropMultiImageCollectionCell
        
        let dict = self.arrSocialMediaImagesMutable[indexPath.row] as! NSDictionary
        
        if dict["isImage"] as! String == "0" { // for URL
            cell.imgSelectedPhotos.sd_setShowActivityIndicatorView(true)
            cell.imgSelectedPhotos.sd_setIndicatorStyle(.gray)
            
            let imgURL = URL(string: dict["imageURL"] as! String)            
            cell.imgSelectedPhotos.sd_setImage(with: imgURL, placeholderImage: UIImage(named:"placeholder_image_listing_square"), options: .lowPriority, completed: nil)
        }
        else { // For images
            cell.imgSelectedPhotos.image = dict["imageURL"] as? UIImage
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        cell.imgSelectedPhotos.isUserInteractionEnabled = true
        cell.imgSelectedPhotos.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth : CGFloat = 0.0
        var cellHeight : CGFloat = 0.0
        
        cellWidth  = collectionView.frame.size.width
        cellHeight  = collectionView.frame.size.height
        
        return CGSize(width: cellWidth , height:CGFloat(cellHeight))
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //        self.navigationController?.popViewController(animated: false)
    }
    
}

extension CropMultiImagesVc : TOCropViewControllerDelegate {
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        let dict = ["imageURL":image, "isImage": "1"] as NSDictionary
        self.arrSocialMediaImagesMutable.replaceObject(at: currentImageIndex, with: dict)
        
        cropViewController.dismiss(animated: false, completion: nil)
        collectionSelectedImages.reloadData()
    }
}
