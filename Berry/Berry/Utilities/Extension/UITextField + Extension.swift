//
//  UITextField + Extension.swift
//  Berry
//
//  Created by YASH on 30/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension CustomTextField {
    
    func setupCustomTextField() {
        self.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.font = themeFont(size: 16, fontname: .regular)
        self.leftPaddingView = 10
        self.cornerRadius = 8
    }
}
