//
//  SocketService-Prifix.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

//Server URL
let SOCKET_SERVER_PATH = "http://ec2-13-59-127-40.us-east-2.compute.amazonaws.com:4001/" //http://ec2-3-14-146-40.us-east-2.compute.amazonaws.com:4001/" //"http://berry.oengines.com:4001/"

//API
//New
let REQ = "req"
let RES = "res"

/****
 * Socket Events Name
 */
let SocketInit: String = "init"
let SocketChatUsersList: String = "chatUsersList"
let SocketSingleChatDetails: String = "singleChatDetails"
let SocketAddQuestions = "addQuestions"
let SocketChatList = "chatList"
let SocketSendMsg = "sendMsg"
let SocketOnlineOffline = "onlineOffline"
let SocketBlockorunblock = "blockorunblock"
let SocketUnreadNoUniqMsg = "unreadNoUniqMsg"
let SocketRead = "read"
let SocketAddUserToChat = "addUserToChat"
let SocketReadNotification = "readNotification"
