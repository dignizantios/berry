//
//  DeleteAccountVC.swift
//  Berry
//
//  Created by Anil on 07/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
enum DeleteAccount:Int {
    case FoundOnBerry = 22
    case FoundSomeoneElse = 23
    case DonotLikeBerry = 24
    case Other = 25
    case None = 0
}

class cardCell: UICollectionViewCell {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnCheckClick: UIButton!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lblText1: UILabel!
    @IBOutlet weak var btnCheck1: UIButton!
    @IBOutlet weak var btnCheckClick1: UIButton!
    
    var handlerBtnClick:(DeleteAccount) -> Void = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view1.backgroundColor = UIColor.appThemeLightShadBackgroundColor
        view2.backgroundColor = UIColor.appThemeLightShadBackgroundColor
        
        lblText.textColor = UIColor.appThemeDarkGrayColor
        lblText.font = themeFont(size: 15, fontname: .medium)
        
        lblText1.textColor = UIColor.appThemeDarkGrayColor
        lblText1.font = themeFont(size: 15, fontname: .medium)
    }
    
    func setViewForIndex1(type:DeleteAccount) {
        img.image = UIImage.init(named: "ic_barry_delete_account_screen.png")
        lblText.text = getCommonString(key: "Found_someone_onBerry_key")
        btnCheck.isSelected = type == DeleteAccount.FoundOnBerry
        btnCheck.tag = DeleteAccount.FoundOnBerry.rawValue
        btnCheckClick.tag = DeleteAccount.FoundOnBerry.rawValue
        
        img1.image = UIImage.init(named: "ic_elsewhere_delete_account_screen.png")
        lblText1.text = getCommonString(key: "Found_someone_elsewhere_key")
        btnCheck1.isSelected = type == DeleteAccount.FoundSomeoneElse
        btnCheck1.tag = DeleteAccount.FoundSomeoneElse.rawValue
        btnCheckClick1.tag = DeleteAccount.FoundSomeoneElse.rawValue
    }
    
    func setViewForIndex2(type:DeleteAccount) {
        img.image = UIImage.init(named: "ic_Don't like Berry_delete_account_screen.png")
        lblText.text = getCommonString(key: "Dont_like_Berry_key")
        btnCheck.isSelected = type == DeleteAccount.DonotLikeBerry
        btnCheck.tag = DeleteAccount.DonotLikeBerry.rawValue
        btnCheckClick.tag = DeleteAccount.DonotLikeBerry.rawValue
        
        img1.image = UIImage.init(named: "ic_other_delete_account_sceen.png")
        lblText1.text = getCommonString(key: "Other_key")
        btnCheck1.isSelected = type == DeleteAccount.Other
        btnCheck1.tag = DeleteAccount.Other.rawValue
        btnCheckClick1.tag = DeleteAccount.Other.rawValue
        
    }
    
    @IBAction func onBtnChecked(_ sender:UIButton) {
        var account = DeleteAccount.None
        if sender.tag == DeleteAccount.FoundOnBerry.rawValue {
            account = DeleteAccount.FoundOnBerry
        } else if sender.tag == DeleteAccount.FoundSomeoneElse.rawValue {
            account = DeleteAccount.FoundSomeoneElse
        } else if sender.tag == DeleteAccount.DonotLikeBerry.rawValue {
            account = DeleteAccount.DonotLikeBerry
        } else if sender.tag == DeleteAccount.Other.rawValue {
            account = DeleteAccount.Other
        }
        
        handlerBtnClick(account)
    }
    
}


class DeleteAccountVC: UIViewController {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTellus: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnLogout: UIButton!
    
    var deleteAccountTypeChoose = DeleteAccount.None
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setupUI() {
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Delete_Account_key"))
       
        lblMsg.textColor = UIColor.appThemeDarkGrayColor
        lblMsg.font = themeFont(size: 20, fontname: .bold)
//        lblTellus.textColor = UIColor.appThemeLightGrayColor
//        lblTellus.font = themeFont(size: 20, fontname: .light)
        
        btnLogout.titleLabel?.font = themeFont(size: 20, fontname: .light)
        btnLogout.setTitle(getCommonString(key: "Delete_Account_key"), for: .normal)
        btnLogout.setTitleColor(UIColor.white, for: .normal)
        btnLogout.backgroundColor = UIColor.appThemeRedColor
        btnLogout.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = self.view.bounds.size.width - 60
        layout.itemSize = CGSize(width: width, height: 150)
        layout.scrollDirection = .vertical
        collectionView.isScrollEnabled = false
        collectionView.collectionViewLayout = layout
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func redirectToPopDeleteAccount() {
        let vc = PopupDeleteAccountVC.init(nibName: "PopupDeleteAccountVC", bundle: nil)
        vc.type = deleteAccountTypeChoose
        vc.handlerDeleteAccount = { [weak self] (isDelete) in
            let isDel = isDelete
            vc.dismiss(animated: true, completion: {
                if isDel {
                    self?.redirectToLoginScreen()
                } else {
//                    self?.navigationController?.popViewController(animated: false)
                    self?.redirectToPopConfirmationDeleteAccount()
                }
            })
            
        }
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirectToPopConfirmationDeleteAccount() {
        let vc = PopupConfirmationDeleteAccountVC.init(nibName: "PopupConfirmationDeleteAccountVC", bundle: nil)
        vc.type = deleteAccountTypeChoose
        vc.handlerDeleteAccount = { [weak self] (isDelete) in
            let isDel = isDelete
            vc.dismiss(animated: true, completion: {
                if isDel {
                    self?.redirectToLoginScreen()
                } else {
//                    self?.navigationController?.popViewController(animated: false)
                }
            })
            
        }
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirectToLoginScreen() {
        deletJSON(key: USER_DETAILS_KEY)
        Defaults.removeObject(forKey: "facebookID")
        Defaults.removeObject(forKey: "authoToken")
        Defaults.removeObject(forKey: "InstagramID")
        Defaults.synchronize()
        
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RootNav") as! UINavigationController
        AppDelegate.shared.window?.rootViewController = vc
    }
    
    // MARK:- IBAction
    @IBAction func btnLogoutClick(_ sender: Any) {
        
    }
    
}

extension DeleteAccountVC: UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cardCell", for: indexPath) as! cardCell
        if indexPath.row == 0 {
            cell.setViewForIndex1(type: deleteAccountTypeChoose)
        } else {
            cell.setViewForIndex2(type: deleteAccountTypeChoose)
        }
        cell.handlerBtnClick = { [weak self] (type) in
            self?.deleteAccountTypeChoose = type
//            self?.collectionView.reloadData()
            self?.redirectToPopDeleteAccount()
        }
        return cell
    }
    
}
