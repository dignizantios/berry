//
//  SuperLikePopupVc.swift
//  Berry
//
//  Created by Anil on 26/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SuperLikeDelegate: class {
    func SuperLikeFinish(userid: String, text: String)
}
class SuperLikePopupVc: UIViewController {
    
    
    //MARK: - @IBOutlet
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblEnterDescription: UILabel!
    @IBOutlet weak var txtVwDescription: CustomTextview!
    @IBOutlet weak var lblPlaceHolderEnterHere: UILabel!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var lblCount: UILabel!
    
    //MARK: - Variables
    var oppUserId : String = ""
    weak var delegate: SuperLikeDelegate?
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
    }
    
    func setupUI() {
        lblMessage.textColor = UIColor.appThemeLightGrayColor
        lblMessage.font = themeFont(size: 18, fontname: .medium)
        lblEnterDescription.text = "You get to send a direct message…Make it count!"
        [lblMessage,lblEnterDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        txtVwDescription.font = themeFont(size: 16, fontname: .regular)
        txtVwDescription.textColor = UIColor.appThemeDarkGrayColor
        txtVwDescription.delegate = self
        
        lblPlaceHolderEnterHere.font = themeFont(size: 15, fontname: .light)
        lblPlaceHolderEnterHere.textColor = UIColor.appThemeLightGrayColor
        
        btnSubmit.setupShadowThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "SUBMIT_key"), for: .normal)
        
    }
    
    func validation() {
        if self.txtVwDescription.text!.toTrim() == "" {
            AlertView(title: "Failed", message: "Please enter message")
        }
        else {
            dismiss(animated: true, completion: nil)
            self.delegate?.SuperLikeFinish(userid: self.oppUserId, text: self.txtVwDescription.text!.toTrim())
        }
    }
}

//MARK: - IBAction method
extension SuperLikePopupVc {
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.validation()
    }
}


extension SuperLikePopupVc : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            self.lblPlaceHolderEnterHere.isHidden = false
        }
        else {
            self.lblPlaceHolderEnterHere.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            txtVwDescription.resignFirstResponder()
            return false
        }
        
        let updatedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        if updatedText.count > 50 {
            return false
        }
        self.lblCount.text = "\(updatedText.count)" + " / 50"
        return true
    }
}


