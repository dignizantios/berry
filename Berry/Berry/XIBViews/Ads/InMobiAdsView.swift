//
//  InMobiAdsView.swift
//  Berry
//
//  Created by Haresh Bhai on 15/05/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import InMobiSDK
import SwiftyJSON
import SDWebImage
import GoogleMobileAds

class InMobiAdsView: UIView, IMNativeDelegate
{
    var superVC:SwipeCardVc?
    var InMobiNativeAd: IMNative?
    var appLink: String = ""
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblAds: TopAlignedLabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSponsored: UILabel!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var btnInstall: UIButton!
    @IBOutlet weak var starView: FloatRatingView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var bottomImage: UIImageView!
    
//    MARK:- Google Admob
    @IBOutlet weak var nativeAdPlaceholder: UIView!
    var heightConstraint : NSLayoutConstraint?
    var adLoader: GADAdLoader!
    var nativeAdView: GADUnifiedNativeAdView!
    
    @IBAction func btnInstallClicked(_ sender: Any)
    {
        guard let url = URL(string: appLink) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    func nativeDidFinishLoading(_ native: IMNative!) {
        NSLog("InMobi Native Did finished loading");
        
        if DeviceType.iPhone5orSE || DeviceType.iPhone4orLess {
            self.lblTitle.font = themeFont(size: 18, fontname: .bold)
            self.lblAds.font = themeFont(size: 16, fontname: .regular)
            self.lblSponsored.font = themeFont(size: 12, fontname: .regular)
            self.btnInstall.titleLabel?.font = themeFont(size: 16, fontname: .regular)
        }
        else {
            self.lblTitle.font = themeFont(size: 21, fontname: .bold)
            self.lblAds.font = themeFont(size: 17, fontname: .regular)
            self.lblSponsored.font = themeFont(size: 12, fontname: .regular)
            self.btnInstall.titleLabel?.font = themeFont(size: 17, fontname: .regular)
        }
        self.lblSponsored.layer.cornerRadius = self.lblSponsored.bounds.height / 2
        self.lblSponsored.layer.masksToBounds = true
        self.lblTitle.text = native.adTitle
        self.lblAds.text = native.adDescription
        self.lblAds.sizeToFit()
        self.btnInstall.setTitle(native.adCtaText, for: .normal)
        self.btnInstall.layer.cornerRadius = self.btnInstall.bounds.height / 2
        let view = native.primaryView(ofWidth: self.viewContent.frame.size.width)
        self.viewHeight.constant = (view?.bounds.height)!
        self.viewContent.addSubview(native.primaryView(ofWidth: self.viewContent.frame.size.width));
        let json = JSON(native.customAdContent)
        if !json.getBool(key: .isVideo) {
            let dict = convertToDictionary(text: native.customAdContent)
            let ss = dict!["screenshots"] as? NSDictionary
            let url = ss?.getString(key: .url)
            print(url)
            SDWebImageManager.shared().loadImage(with: URL(string: url!), options: SDWebImageOptions.highPriority, progress: { (image, finished, url) in
                
            }, completed: { (image, data, error, type, finished, imageUrl) in
                if(finished) {
                    if((image) != nil) {
                        
                        DispatchQueue.main.async {
                            self.imgMain.image = self.blur(image: image!)
                        }
//                        self.topImage.image = image
//                        self.topImage.transform = CGAffineTransform(scaleX: 1, y: -1)
//                        self.bottomImage.image = image
//                        self.bottomImage.transform = CGAffineTransform(scaleX: 1, y: -1)
//                        DispatchQueue.main.async {
//                            self.blurButtonTapped(self.topImage)
//                            self.blurButtonTapped(self.bottomImage)
//                        }
                    }
                }
            })
            
//            let image =  native.primaryView(ofWidth: self.viewContent.frame.size.width)?.asImage()
            
        }


        self.appLink = native.adLandingPageUrl.absoluteString
//        btnInstall.layer.backgroundColor = UIColor(red: 75/255, green: 41/255, blue: 109/255, alpha: 1).cgColor
    }
    
    func blur(image: UIImage) -> UIImage {
        let radius: CGFloat = 3;
        let context = CIContext(options: nil);
        let inputImage = CIImage(cgImage: image.cgImage!);
        let filter = CIFilter(name: "CIGaussianBlur");
        filter?.setValue(inputImage, forKey: kCIInputImageKey);
        filter?.setValue("\(radius)", forKey:kCIInputRadiusKey);
        let result = filter?.value(forKey: kCIOutputImageKey) as! CIImage;
        let rect = CGRect(x: radius * 2, y: radius * 2, width: image.size.width - radius * 4, height: image.size.height - radius * 4)
        let cgImage = context.createCGImage(result, from: rect);
        let returnImage = UIImage(cgImage: cgImage!);
        return returnImage;
    }
    
     func blurButtonTapped(_ bgImageView: UIImageView) {
        
        let inputImage = CIImage(cgImage: (bgImageView.image?.cgImage)!)
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(inputImage, forKey: "inputImage")
        filter?.setValue(10, forKey: "inputRadius")
        let blurred = filter?.outputImage
        
        var newImageSize: CGRect = (blurred?.extent)!
        newImageSize.origin.x += (newImageSize.size.width - (bgImageView.image?.size.width)!) / 2
        newImageSize.origin.y += (newImageSize.size.height - (bgImageView.image?.size.height)!) / 2
        newImageSize.size = (bgImageView.image?.size)!
        
        let resultImage: CIImage = filter?.value(forKey: "outputImage") as! CIImage
        let context: CIContext = CIContext.init(options: nil)
        let cgimg: CGImage = context.createCGImage(resultImage, from: newImageSize)!
        let blurredImage: UIImage = UIImage.init(cgImage: cgimg)
        bgImageView.image = blurredImage
    }
    
    func loadAds() {
        self.InMobiNativeAd = IMNative(placementId:1556067989851);
        self.InMobiNativeAd?.delegate = self;
        self.InMobiNativeAd?.load()
        self.InMobiNativeAd?.isReady()
    }

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func native(_ native: IMNative!, didFailToLoadWithError error: IMRequestStatus!)
    {
        nativeAdPlaceholder.isHidden = false
        self.setupGoogleAdsView()
    }
}

class TopAlignedLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let textRect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        super.drawText(in: textRect)
    }
}

extension InMobiAdsView
{
    func setupGoogleAdsView()
    {
        guard let nibObjects = Bundle.main.loadNibNamed("AdmobNativeAdsView", owner: nil, options: nil),
          let adView = nibObjects.first as? GADUnifiedNativeAdView else {
            assert(false, "Could not load nib file for adView")
            return
        }
    
        setAdView(adView)
        refreshAd(nil)
    }
    
    func setAdView(_ view: GADUnifiedNativeAdView) {
        // Remove the previous ad view.
        nativeAdView = view
        nativeAdPlaceholder.addSubview(nativeAdView)
        nativeAdView.translatesAutoresizingMaskIntoConstraints = false
    }
    func refreshAd(_ sender: AnyObject!)
    {
        adLoader = GADAdLoader(adUnitID: adUnitID_Admob, rootViewController: superVC, adTypes: [.unifiedNative], options: nil)
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }
    /// Returns a `UIImage` representing the number of stars from the given star rating; returns `nil`
    /// if the star rating is less than 3.5 stars.
    func imageOfStars(from starRating: NSDecimalNumber?) -> UIImage? {
        guard let rating = starRating?.doubleValue else {
        return nil
        }
        if rating >= 5 {
        return UIImage(named: "stars_5")
        } else if rating >= 4.5 {
        return UIImage(named: "stars_4_5")
        } else if rating >= 4 {
        return UIImage(named: "stars_4")
        } else if rating >= 3.5 {
        return UIImage(named: "stars_3_5")
        } else {
        return nil
        }
    }
}

extension InMobiAdsView:GADUnifiedNativeAdLoaderDelegate
{
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd)
    {
        nativeAdView.nativeAd = nativeAd
        nativeAd.delegate = self
        heightConstraint?.isActive = false
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent

        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
          // By acting as the delegate to the GADVideoController, this ViewController receives messages
          // about events in the video lifecycle.
          //mediaContent.videoController.delegate = self
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
          heightConstraint = NSLayoutConstraint(item: mediaView,
                                                attribute: .height,
                                                relatedBy: .equal,
                                                toItem: mediaView,
                                                attribute: .width,
                                                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                                                constant: 0)
          heightConstraint?.isActive = true
        }

        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil

        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil

        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil

        (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from:nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil

        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil

        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil

        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil

        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
    }
}

// MARK: - GADUnifiedNativeAdDelegate implementation
extension InMobiAdsView:GADUnifiedNativeAdDelegate {

  func nativeAdDidRecordClick(_ nativeAd: GADUnifiedNativeAd) {
    print("\(#function) called")
  }

  func nativeAdDidRecordImpression(_ nativeAd: GADUnifiedNativeAd) {
    print("\(#function) called")
  }

  func nativeAdWillPresentScreen(_ nativeAd: GADUnifiedNativeAd) {
    print("\(#function) called")
  }

  func nativeAdWillDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
    print("\(#function) called")
  }

  func nativeAdDidDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
    print("\(#function) called")
  }

  func nativeAdWillLeaveApplication(_ nativeAd: GADUnifiedNativeAd) {
    print("\(#function) called")
  }
}

extension InMobiAdsView : GADAdLoaderDelegate {
  func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
    print("\(adLoader) failed with error: \(error.localizedDescription)")
  }
}

extension UIView {
    
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.y, y: -origin.x,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return rotatedImage ?? self
        }
        
        return self
    }
}

struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height)
    static let maxWH = max(ScreenSize.width, ScreenSize.height)
}

struct DeviceType {
    static let iPhone4orLess = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH < 568.0
    static let iPhone5orSE   = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 568.0
    static let iPhone678     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 667.0
    static let iPhone678p    = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 736.0
    static let iPhoneX       = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 812.0
    static let iPhoneXRMax   = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.maxWH == 896.0
    static var hasNotch: Bool {
        return iPhoneX || iPhoneXRMax
    }
}
