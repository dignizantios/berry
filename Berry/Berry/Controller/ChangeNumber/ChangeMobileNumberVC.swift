//
//  ChangeMobileNumberVC.swift
//  Berry
//
//  Created by Haresh Bhai on 19/06/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol ChangeMobileNumberDelegate: class {
    func ChangeMobileNumberDidFinish(mobileNo: String, countryDialCode: String, countryName: String, CountryCode: String)
}

class ChangeMobileNumberVC: UIViewController, CountryCodeDelegate {

    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgCountryCode: UIImageView!
    @IBOutlet weak var viewCountry: UIView!
    
    
    //MARK: - Variable
    var countryDialCode: String = "+1"
    var countryName: String = "United States"
    var countryCode: String = "US"
    var mobileNumber: String = ""
    
    weak var delegate: ChangeMobileNumberDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtMobileNumber.text = self.mobileNumber
        self.setupUI()
        // Do any additional setup after loading the view.
    }

    func setupUI() {
        self.viewCountry.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.viewCountry.layer.cornerRadius = 8
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
        
        lblMobileNumber.textColor = UIColor.black
        lblMobileNumber.font = themeFont(size: 17, fontname: .bold)
        lblMobileNumber.text = getCommonString(key: "Mobile_number_key")
        txtMobileNumber.setupCustomTextField()
        
        btnSubmit.setTitle(getCommonString(key: "SUBMIT_key"), for: .normal)
        btnSubmit.setupShadowThemeButtonUI()
        
        addDoneButtonOnKeyboard(textfield: txtMobileNumber)
    }
    
    @IBAction func btnCountryTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.delegate = self
        obj.modalPresentationStyle = .overFullScreen
        self.present(obj, animated:  false, completion: nil)
    }
    
    func CountryCodeDidFinish(data: JSON) {
        self.countryDialCode = data["dial_code"].string ?? ""
        self.countryName = data["name"].string ?? ""
        self.countryCode = data["code"].string ?? ""
        
        self.imgCountryCode.image = UIImage(named : countryCode)
        self.lblCountryCode.text = countryDialCode
    }

    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.ChangeMobileNumberDidFinish(mobileNo: self.txtMobileNumber.text!.toTrim(), countryDialCode: self.countryDialCode, countryName: self.countryName, CountryCode: String(self.countryDialCode.dropFirst()))
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
