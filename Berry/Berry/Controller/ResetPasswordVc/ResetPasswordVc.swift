//
//  ResetPasswordVc.swift
//  Berry
//
//  Created by YASH on 29/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

enum checkParentControllerOfResetPassword {
    case fromOTP
    case fromChangePassword
}

class ResetPasswordVc: UIViewController {
    
    //MARK: - Outlet    
    
    @IBOutlet weak var viewOldPassBack: UIView!
    @IBOutlet weak var viewNewPassBack: UIView!
    @IBOutlet weak var viewConfirmPassBack: UIView!
    
    @IBOutlet weak var lblEnterNewPassword: UILabel!
    @IBOutlet weak var lblConfirmNewPassword: UILabel!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    @IBOutlet weak var txtConfirmNewPassword: CustomTextField!
    
    @IBOutlet weak var vwEnterOldPassword: UIView!
    @IBOutlet weak var lblEnterOldPassword: UILabel!
    @IBOutlet weak var txtEnterOldPassword: CustomTextField!
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var lblPasswordCharacter: UILabel!
    @IBOutlet weak var lblPasswordNumber: UILabel!
    @IBOutlet weak var lblPasswordNonAlphan: UILabel!
    @IBOutlet weak var lblPassReq: UILabel!
    
    //MARK: - Variable
    var selectedController = checkParentControllerOfResetPassword.fromOTP
    var mobileNo: String = ""
    var countryDialCode: String = ""

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .fromOTP {
            self.vwEnterOldPassword.isHidden = true
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Reset_Password_key"))
        }
        else {
            self.vwEnterOldPassword.isHidden = false
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Change_password_key"))
        }
    }
    
    func setupUI() {
        [lblEnterNewPassword,lblConfirmNewPassword,lblEnterOldPassword, lblPassReq].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 17, fontname: .bold)
        }
        
        self.viewOldPassBack.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.viewOldPassBack.layer.cornerRadius = 8
        
        self.viewNewPassBack.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.viewNewPassBack.layer.cornerRadius = 8
        
        self.viewConfirmPassBack.backgroundColor = UIColor.appThemetxtBackgroundColor
        self.viewConfirmPassBack.layer.cornerRadius = 8
        
        lblConfirmNewPassword.text = getCommonString(key: "Confirm_new_password_key")
        lblEnterNewPassword.text = getCommonString(key: "Enter_new_password_key")
        lblEnterOldPassword.text = getCommonString(key: "Enter_old_password_key")
        
        [txtNewPassword,txtConfirmNewPassword,txtEnterOldPassword].forEach { (txt) in
            txt?.setupCustomTextField()
            txt?.delegate = self
        }
        
        btnSubmit.setupShadowThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "SUBMIT_key"), for: .normal)
    }
    
    func validationPassword(){
        if selectedController == .fromChangePassword && self.txtEnterOldPassword.text! == "" {
            AlertView(title: "Failed", message: "Please enter old password")
        }
        else if self.txtNewPassword.text! == "" {
            AlertView(title: "Failed", message: "Please enter new password")
        }
       /* else if self.txtNewPassword.text!.count < 8 {
            self.showRedAlert()
            AlertView(title: "Failed", message: "Please enter minimum 8 digit password")
        }*/
        else if !isValidPassword(testStr: self.txtNewPassword.text!) {
            self.showRedAlert()
//            self.showToast(message: "Password should be at least one number, one non-alphanumeric character and 8 character long")
        }
        else if selectedController == .fromChangePassword && self.txtEnterOldPassword.text! == self.txtNewPassword.text {
            AlertView(title: "Failed", message: "Old and new password is same please use different password")
        }
        else if self.txtConfirmNewPassword.text! == "" {
            self.showRedAlert(isShow: false)
            AlertView(title: "Failed", message: "Please enter confirm password")
        }
        else if self.txtNewPassword.text! != self.txtConfirmNewPassword.text! {
            AlertView(title: "Failed", message: "New and confirm password does not match")
        }
        else {
            self.showRedAlert(isShow: false)
            if selectedController == .fromChangePassword {
                self.changePassword()
            }
            else {
                self.resetPassword()
            }
        }
    }
    
    func showRedAlert(isShow: Bool = true) {
        self.lblPasswordNumber.textColor = isShow ? UIColor.red : UIColor.lightGray
        self.lblPasswordCharacter.textColor = isShow ? UIColor.red : UIColor.lightGray
        self.lblPasswordNonAlphan.textColor = isShow ? UIColor.red : UIColor.lightGray
    }
    
    func changePassword() {
        let param = ["userid":userDetails.getString(key: .userid),"oldpass":self.txtEnterOldPassword.text!,"newpass":self.txtNewPassword.text!]
        ApiManager.shared.MakePostAPI(name: CHANGE_PASSWORD, params: param, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                    self.txtEnterOldPassword.text = ""
                    self.txtNewPassword.text = ""
                    self.txtConfirmNewPassword.text = ""
//                    let msg = json.getString(key: .msg)
//                    AlertView(title: "Success", message: msg)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.changePassword()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    func resetPassword() {
        let param = ["mobile_no": self.mobileNo, "password":self.txtNewPassword.text!, "co": self.countryDialCode.dropFirst()] as [String : Any]
        print(param)
        ApiManager.shared.MakePostAPI(name: FORGOT_PASSWORD, params: param, vc: self) { (result, error) in
            if result != nil {
                let json = JSON(result!)
                let error = json.getString(key: .error)
                if error != YES {
                    self.txtEnterOldPassword.text = ""
                    self.txtNewPassword.text = ""
                    self.txtConfirmNewPassword.text = ""
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Success", message: msg)
                    self.navigationController?.popToRootViewController(animated: true)
                }
                else {
                    let msg = json.getString(key: .msg)
                    AlertView(title: "Failed", message: msg)
                }
            }
            else {
                self.resetPassword()
//                AlertView(title: "Failed", message: error!)
            }
        }
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.validationPassword()
    }
    
    @IBAction func btnOldShowPasswordTapped(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "password_not_viewed") {
            UIView.performWithoutAnimation {
                self.txtEnterOldPassword.isSecureTextEntry = false
                sender.setImage(UIImage(named: "password_viewed"), for: .normal)
            }
        }
        else {
            UIView.performWithoutAnimation {
                self.txtEnterOldPassword.isSecureTextEntry = true
                sender.setImage(UIImage(named: "password_not_viewed"), for: .normal)
            }
        }
    }
    
    @IBAction func btnNewShowPasswordTapped(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "password_not_viewed") {
            UIView.performWithoutAnimation {
                self.txtNewPassword.isSecureTextEntry = false
                sender.setImage(UIImage(named: "password_viewed"), for: .normal)
            }
        }
        else {
            UIView.performWithoutAnimation {
                self.txtNewPassword.isSecureTextEntry = true
                sender.setImage(UIImage(named: "password_not_viewed"), for: .normal)
            }
        }
    }
    
    @IBAction func btnConfirmShowPasswordTapped(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "password_not_viewed") {
            UIView.performWithoutAnimation {
                self.txtConfirmNewPassword.isSecureTextEntry = false
                sender.setImage(UIImage(named: "password_viewed"), for: .normal)
            }
        }
        else {
            UIView.performWithoutAnimation {
                self.txtConfirmNewPassword.isSecureTextEntry = true
                sender.setImage(UIImage(named: "password_not_viewed"), for: .normal)
            }
        }
    }
}

extension ResetPasswordVc : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)

            if updatedText == " " {
                return false
            }
            
            if (string == " ") {
                return false
            }            
        }        
        return true
    }
}
